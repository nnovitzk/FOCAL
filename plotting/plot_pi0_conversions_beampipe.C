void plot_pi0_conversions_beampipe(void) {
  gStyle->SetOptStat(0);

  Float_t etaMin = 5.0; //4.5;//3.0; //4.0;
  Float_t etaMax = 5.5; //4.5;
  
  TFile *fin = new TFile("data_quark/analysis_pi0_pt0_10_albeflange_support_newpars_all.root");

  const Int_t drawAlFunc = 1;
  
  TTree *analysis = dynamic_cast<TTree*>(fin->Get("analysis"));

  Float_t thetaMin = 2*TMath::ATan(TMath::Exp(-etaMax));
  Float_t thetaMax = 2*TMath::ATan(TMath::Exp(-etaMin));
  
  TCut etaCut(Form("Theta[0]<%f&&Theta[0]>%f",thetaMax,thetaMin));

  TCanvas *c1 = new TCanvas("c1","c1: conversions 2D",700,600);
  analysis->Draw("TMath::Sqrt(cVx[1]*cVx[1]+cVy[1]*cVy[1]):cVz[1]","conv_flag[1]");
  analysis->SetMarkerColor(2);
  analysis->Draw("TMath::Sqrt(cVx[1]*cVx[1]+cVy[1]*cVy[1]):cVz[1]",etaCut&&TCut("conv_flag[1]"),"same");

  TLatex *ltx = new TLatex;
  ltx->SetNDC();
  ltx->SetTextFont(42);
  ltx->DrawLatex(0.15,0.85,Form("%.1f < #eta < %.1f",etaMin,etaMax));
  
  TCanvas *c2 = new TCanvas("c2","c2: conversions z",700,600);
  analysis->Draw("cVz[1]","conv_flag[1]");
  analysis->SetLineColor(2);
  analysis->Draw("cVz[1]",etaCut&&"conv_flag[1]","same");

  TCanvas *c2b = new TCanvas("c2b","c2b: conversions eta",700,600);
  analysis->SetLineColor(4);
  analysis->Draw("-TMath::Log(TMath::Tan(Theta[1]/2))>>hEta(60,3,6.0)","");
  analysis->SetLineColor(2);
  analysis->Draw("-TMath::Log(TMath::Tan(Theta[1]/2))>>hEtaConv","conv_flag[1]","same");

  TCanvas *c2c = new TCanvas("c2c","c2c: conversion prob",700,600);
  TH1F *hEta = dynamic_cast<TH1F*> (TDirectory::CurrentDirectory()->Get("hEta"));
  TH1F *hEtaConv = dynamic_cast<TH1F*> (TDirectory::CurrentDirectory()->Get("hEtaConv"));
  TH1F *hConvProb = new TH1F(*hEtaConv);
  hConvProb->SetName("hConvProb");
  hConvProb->Divide(hEta);
  hConvProb->GetXaxis()->SetTitle("#eta");
  hConvProb->GetYaxis()->SetTitle("conversion prob.");
  hConvProb->SetMinimum(0);
  hConvProb->SetMaximum(1.0);
  hConvProb->Draw();

   TF1 *f_conv = new TF1("f_conv","1-TMath::Exp(-[0]/TMath::Sin(TMath::ATan(TMath::Exp(-x))*2))",2.5,6.0);
  const Float_t d_beampipe = 0.08;  // 800 micron in cm
  const Float_t d_beampipe_Al = 0.1;  // 1 mm
  const Float_t X0_Be = 35.28; // cm (for Be);
  const Float_t X0_Al = 8.897; // cm (for Al);

  if (drawAlFunc) {
    f_conv->SetParameter(0,d_beampipe_Al/X0_Al);  // should need factor 7/9 for conversions?
    f_conv->DrawCopy("same");
    f_conv->SetLineStyle(2);
  }
  f_conv->SetParameter(0,d_beampipe/X0_Be);  // should need factor 7/9 for conversions?
  f_conv->DrawCopy("same");

  
  TCanvas *c3 = new TCanvas("c3","c3: nClust",700,600);
  analysis->SetLineColor(4);
  analysis->Draw("nClusters>>hnClust",etaCut);
  analysis->SetLineColor(2);
  analysis->Draw("nClusters>>hnClustConv",etaCut&&"(conv_flag[1]||conv_flag[2])","same");
  auto hnClust = dynamic_cast<TH1*>(TDirectory::CurrentDirectory()->Get("hnClust"));
  auto hnClustConv = dynamic_cast<TH1*>(TDirectory::CurrentDirectory()->Get("hnClustConv"));
  cout << " hnClust " << hnClust << endl;
  //hnClust->SetMaximum(2400);
  hnClust->SetTitle("Number of clusters");
  hnClust->SetXTitle("N_{clust}");
  hnClust->SetYTitle("N");

  Float_t convFrac = hnClustConv->GetEntries()/hnClust->GetEntries();
  cout << "conversions: " << hnClustConv->GetEntries() << " out of " << hnClust->GetEntries() << " total pi0s; fraction " << hnClustConv->GetEntries()/hnClust->GetEntries() << endl;
  analysis->SetLineStyle(2);
  analysis->SetLineStyle(1);

  ltx->DrawLatex(0.15,0.85,Form("%.1f < #eta < %.1f",etaMin,etaMax));
  TLegend *leg = new TLegend(0.5,0.6,0.90,0.8);
  leg->SetBorderSize(0);
  leg->AddEntry(hnClust,"all","l");
  leg->AddEntry(hnClustConv,">= 1 conversion","l");
  leg->Draw();

  ltx->SetTextSize(0.04);
  ltx->DrawLatex(0.5,0.5,Form("frac w/ conv: %.2f",convFrac));
  
  TCanvas *c4 = new TCanvas("c4","c4: invariant mass",700,600);
  analysis->SetLineColor(4);
  analysis->Draw("InvariantMass>>hImassNoConv",etaCut&&"InvariantMass>0&&conv_flag[1]==0&&conv_flag[2]==0");
  analysis->SetLineColor(2);
  analysis->Draw("InvariantMass>>hImassConv",etaCut&&"InvariantMass>0&&(conv_flag[1]||conv_flag[2])","same");

  auto hImassConv = dynamic_cast<TH1*>(TDirectory::CurrentDirectory()->Get("hImassConv"));
  auto hImassNoConv = dynamic_cast<TH1*>(TDirectory::CurrentDirectory()->Get("hImassNoConv"));
  hImassNoConv->SetTitle("Invariant mass");
  hImassNoConv->SetXTitle("m(#gamma,#gamma)");
  //hImassNoConv->SetMaximum(200);
  
  TLegend *leg2 = new TLegend(0.5,0.6,0.88,0.8);
  leg2->SetBorderSize(0);
  leg2->AddEntry(hImassNoConv,"no conversion","l");
  leg2->AddEntry(hImassConv,">= 1 conversion","l");
  leg2->Draw();
  ltx->DrawLatex(0.55,0.85,Form("%.1f < #eta < %.1f",etaMin,etaMax));
}
