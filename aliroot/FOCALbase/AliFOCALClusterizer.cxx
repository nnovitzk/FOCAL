/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 16   2010                            //
//                                                     //
//  Clusterizer for ALICE-FOCAL                        //
//                                                     //
//-----------------------------------------------------//

#include "TClonesArray.h"
#include "TTree.h"
#include <Riostream.h>

#include "AliFOCALClusterizer.h"
#include "AliLog.h"

ClassImp(AliFOCALClusterizer)

using namespace std;

//____________________________________________________________________________
AliFOCALClusterizer::AliFOCALClusterizer():
  TObject(),
  fout(0),
  branch(0),
  fBackgroundBranch(0),
  fInputArray(NULL),
  fTreeR(NULL),
  fCluster(NULL),
  fClusterItr(NULL),
  fClusterHCal(NULL),
  fPad(NULL),
  fCell(NULL)
{
  // 
}

//____________________________________________________________________________
AliFOCALClusterizer::AliFOCALClusterizer(const AliFOCALClusterizer &ali):
  TObject(ali),
  fout(ali.fout),
  branch(ali.branch),
  fBackgroundBranch(ali.fBackgroundBranch),
  fInputArray(ali.fInputArray),
  fTreeR(ali.fTreeR),
  fCluster(ali.fCluster),
  fClusterItr(ali.fClusterItr),
  fClusterHCal(ali.fClusterHCal),
  fPad(ali.fPad),
  fCell(ali.fCell)
{
  // Copy constructor
  //    NB: should probably use a deep copy for the TClonesArrays
}

AliFOCALClusterizer & AliFOCALClusterizer::operator = (const AliFOCALClusterizer &ali)
{
	if(this!=&ali)
	{
		fInputArray = ali.fInputArray;
		fTreeR = ali.fTreeR;
		fCluster = ali.fCluster;
		fClusterItr = ali.fClusterItr;
		fClusterHCal = ali.fClusterHCal;
		fPad = ali.fPad;
		fCell = ali.fCell;
		branch = ali.branch;
		fBackgroundBranch = ali.fBackgroundBranch;
		fout = ali.fout;
	}
	return *this;
}

//____________________________________________________________________________
AliFOCALClusterizer::~AliFOCALClusterizer()
{
  //
  // aaaa
  //
	if(fInputArray)
	{
		fInputArray->Delete();
		delete fInputArray;
	}
	if(fCluster)
	{
		fCluster->Delete();
		delete fCluster;
	}
	if(fClusterItr)
	{
		fClusterItr->Delete();
		delete fClusterItr;
	}
	if(fClusterHCal)
	{
		fClusterHCal->Delete();
		delete fClusterHCal;
	}
	if(fPad)
	{
		fPad->Delete();
	}
	if(fCell)
	{
		fCell->Delete();
	}
}


//____________________________________________________________________________
void AliFOCALClusterizer::SetInput(TTree *InputTree, const char *format)
{
	// Read the digits from the input tree
	if (strcmp(format,"DIGITS") == 0) {
	  branch = InputTree->GetBranch("Digits");
	  if(!branch)
	  {
		  AliError("can't get the branch with the Digits!");
		  return;
	  }
	} else {
	  branch = InputTree->GetBranch("FOCAL");
	  if(!branch)
	  {
		  AliError("can't get the branch with the FOCAL!");
		  return;
	  }
	}
}

//____________________________________________________________________________
void AliFOCALClusterizer::SetBackground(TTree *backgroundTree) {

  fBackgroundBranch = backgroundTree->GetBranch("FOCAL");
	if(!fBackgroundBranch)
	{
		AliError("fBackgroundBranch: can't get the branch with the FOCAL!");
		return;
	}
}

//____________________________________________________________________________
void AliFOCALClusterizer::SetOutput(const char *name)
{
	// Read the digits or hits from the input tree
	// fTreeR = clustersTree;
  fClusterItr = new TClonesArray("AliFOCALCluster",1000);
  fCluster = new TClonesArray("AliFOCALCluster",1000);
  fPad = new TClonesArray("AliFOCALPad",1000);
  fCell = new TClonesArray("AliFOCALCell",1000);
  
	fout = new TFile(name,"recreate");
	fTreeR = new TTree("fTreeR","fTreeR"); 
	AliDebug(9, "Making array for FOCAL clusters");

	
	Int_t bufsize = 32000;
	fTreeR->Branch("AliFOCALClusterItr", &fClusterItr, bufsize);
	
	fTreeR->Branch("AliFOCALCluster", &fCluster, bufsize);

	fTreeR->Branch("AliFOCALClusterHCal", &fClusterHCal, bufsize);
	
	fTreeR->Branch("AliFOCALPad", &fPad, bufsize);
	
	fTreeR->Branch("AliFOCALCell", &fCell, bufsize);
}

// Creates output file, does not create anything inside, returns pointer to it
TFile * AliFOCALClusterizer::CreateOutputFile(const char * fileName) {

  fout = new TFile(fileName,"recreate");
  return fout;
}

// Creates directory inside the output file, creates Trees, returns pointer to the created directory
TDirectory * AliFOCALClusterizer::SetOutputForEvent(const char * eventName) {
  
  cout << "AliFOCALClusterizer::SetOuputForEvent called" << endl;
  
  if (!fout) {
    cout << "AliFOCALClusterizer::SetOuputForEvent: No output file defined! Returning." << endl;
    return 0;
  }
  
  if (!fClusterItr)
    fClusterItr = new TClonesArray("AliFOCALCluster",1000);
  if (!fCluster)
    fCluster = new TClonesArray("AliFOCALCluster",1000);
  if (!fClusterHCal)
    fClusterHCal = new TClonesArray("AliFOCALCluster",1000);
  if (!fPad)
    fPad = new TClonesArray("AliFOCALPad",1000);
  if (!fCell)
    fCell = new TClonesArray("AliFOCALCell",1000);
  
  cout << Form("AliFOCALClusterizer::SetOutputForEvent: Creating new folder (%s) inside the output file",eventName) << endl;
  
  TDirectory* outDir = fout->mkdir(eventName);
  fout->cd(eventName);
 
  cout << Form("AliFOCALClusterizer::SetOutputForEvent: Creating new TTree inside %s.",eventName) << endl;
  
  if (fTreeR) {
    delete fTreeR;
    fTreeR = 0;
  }
    
  fTreeR = new TTree("fTreeR","fTreeR");
  
  Int_t bufsize = 32000;
  fTreeR->Branch("AliFOCALClusterItr", &fClusterItr, bufsize);	
  fTreeR->Branch("AliFOCALCluster", &fCluster, bufsize);	
  fTreeR->Branch("AliFOCALClusterHCal", &fClusterHCal, bufsize);	
  fTreeR->Branch("AliFOCALPad", &fPad, bufsize);	
  fTreeR->Branch("AliFOCALCell", &fCell, bufsize);

  // This does not work anymore?
  // cout << " pointer from fout->GetDi" << fout->GetDirectory(eventName) << endl;
  return outDir; ///fout->GetDirectory(eventName);
}

// Saves output ttree into given folder in output file (folder must exist)
Bool_t AliFOCALClusterizer::SaveEventOutput(const char * eventName) {
  
  cout << "AliFOCALClusterizer::SaveEventOutput called" << endl;
 
  if (!fTreeR) {
    cout << "AliFOCALClusterizer::SaveEventOutput: No output tree, returning false!" << endl;
    return false;
  }
 
  if (!fout) {
    cout << "AliFOCALClusterizer::SaveEventOutput: No output file, returning false!" << endl;
    return false;
  }
  
  if (fout->GetDirectory(eventName) == 0) {
    cout << "AliFOCALClusterizer::SaveEventOutput: Missing output directory in output file, returning false!" << endl;
    return false;
  }
  
  fout->cd(eventName);
  fTreeR->Write();
  return true;
}

// Saves and closes the output file
Bool_t AliFOCALClusterizer::CloseOutputFile() {
  
  cout << "AliFOCALClusterizer::CloseOutputFile called" << endl;
  
  if (!fout) {
    cout << "AliFOCALClusterizer::CloseOutputFile: No output file, returning false!" << endl;
    return false;
  }
  
  fout->Write();
  fout->Close();
  
  fTreeR = 0;
  
  return true;
}

//____________________________________________________________________________
void AliFOCALClusterizer::AnaEnd(void)
{
	fout->cd();
	fTreeR->Write();
	fout->Close();
}
