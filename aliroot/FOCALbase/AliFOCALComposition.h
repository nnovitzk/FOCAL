#ifndef ALIFOCALCOMPOSITION_H
#define ALIFOCALCOMPOSITION_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  30 2009                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "Rtypes.h"
#include "TObject.h"
#include <TString.h>


class TClonesArray;



class AliFOCALComposition: public TObject
{

 public:
  AliFOCALComposition();
  AliFOCALComposition(TString material, Int_t layer, Int_t stack, Int_t id,
		      Float_t cx, Float_t cy, Float_t cz,
		      Float_t dx, Float_t dy, Float_t dz);
		      
  AliFOCALComposition(AliFOCALComposition *fComposition);
  AliFOCALComposition (const AliFOCALComposition &fComposition);
  AliFOCALComposition &operator=(const AliFOCALComposition &fComposition);

  virtual ~AliFOCALComposition();

 public:
  virtual void SetCompositionParameters(TString material, Int_t layer, Int_t stack, Int_t id, 
					Float_t cx, Float_t cy, Float_t cz,
					Float_t dx, Float_t dy, Float_t dz){
    fMaterial = material;
    fLayer = layer;
    fStack  = stack;
    fId = id;
    fCenterX = cx;
    fCenterY = cy;
    fCenterZ = cz;
    fSizeX = dx;
    fSizeY = dy;
    fSizeZ = dz;
  };
  virtual void SetLayerNumber(Int_t layer){fLayer = layer;};
  virtual void SetId(Int_t id){fId = id;};
  virtual Float_t GetThickness(void) const { return fSizeZ ;};
  virtual void SetCenterZ(float val){ fCenterZ = val; };
  
  virtual TString Material() const {return fMaterial;}
  virtual Int_t Layer() const {return fLayer;}
  virtual Int_t Stack() const {return  fStack;}
  virtual Int_t Id() const {return  fId;}
  virtual Float_t CenterX() const {return fCenterX;}
  virtual Float_t CenterY() const {return fCenterY;}
  virtual Float_t CenterZ() const {return fCenterZ;}
  virtual Float_t SizeX() const {return fSizeX;}
  virtual Float_t SizeY() const {return fSizeY;}
  virtual Float_t SizeZ() const {return fSizeZ;}
  

 private:
  TString     fMaterial;
  Int_t       fLayer;
  Int_t       fStack;
  Int_t       fId;
  Float_t     fCenterX;
  Float_t     fCenterY;
  Float_t     fCenterZ;
  Float_t     fSizeX;
  Float_t     fSizeY;
  Float_t     fSizeZ;
  
  ClassDef(AliFOCALComposition,5) // Utility class for the detector set:FOCAL
};

#endif
