#ifndef ALIFOCALGEOMETRY_H
#define ALIFOCALGEOMETRY_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: AliPHOSGeometry.h 29570 2008-10-28 08:12:56Z hristov $ */

//_________________________________________________________________________
// Geometry class  for FOCAL : singleton
//  
//
//-- Author: Taku Gunji


#include <list>
#include "Rtypes.h"
#include "TObject.h"
#include "AliLog.h"
#include "AliFOCALComposition.h"
#include "AliFOCALdigit.h"

class TVector3;
class TClonesArray;
class TObjArray;
class AliFOCALComposition;
class AliFOCALVirtSegment;

using std::list;

class AliFOCALGeometry
{
	public:
		AliFOCALGeometry() ;
		AliFOCALGeometry(AliFOCALGeometry *fGeometry);
		AliFOCALGeometry (const AliFOCALGeometry &fGeometry);
		AliFOCALGeometry& operator= (const AliFOCALGeometry &fGeometry);
		virtual ~AliFOCALGeometry(void) ; 
		
		static AliFOCALGeometry *GetInstance();
		static AliFOCALGeometry *GetInstance(const char *geometryfile);
		
		virtual void  Init(const char *geometryfile);
		virtual void  Init();
		virtual void  BuildComposition();
		virtual void  SetParameters(const char *geometryfile);
		virtual void  SetParameters();
		//  Bool_t  IsInitialized(void) const { return fgInit ; }
				
		///// functions to get the geometry 
		void GetGeoPadCenter(Int_t Tower, Int_t Layer, Int_t Stack, Int_t Row, Int_t Col, Double_t &x, Double_t &y, Double_t &z);
		void GetGeoPadCenterLocal(Int_t Row, Int_t Col, Double_t &x, Double_t &y);
		void GetGeoPixelCenter(Int_t pixel_id, Int_t Tower, Int_t Layer, Int_t Stack, Int_t Row, Int_t Col, Double_t &x, Double_t &y, Double_t &z);
		void GetGeoCompositionCenter(Int_t Tower, Int_t Layer, Int_t Stack, Double_t &x, Double_t &y, Double_t &z);
		void GetPadPositionId2RowColStackLayer(Int_t id, Int_t &row, Int_t &col, Int_t &stack, Int_t &lay, Int_t &seg);
		/*
		void GetStripPosition(Int_t id, Int_t tower, Int_t istrip, Int_t &layerid, Double_t &x, Double_t &y, Double_t &z); /// Still using??
		void GetStripPositionInWafer(Int_t id, Int_t x_y, Double_t &valx, Double_t &valy); /// Still using??
		
		Int_t GetStripNumber(Int_t vol0, Int_t vol1, Int_t vol2, Double_t x, Double_t y, Double_t z); /// Still using??
		*/
		Int_t GetPixelNumber(Int_t vol0, Int_t vol1, Int_t vol2, Double_t x, Double_t y, Double_t z); 
		Int_t GetNumberOfPads(void){ return fGlobal_NX_NY_Pads*fGlobal_NX_NY_Pads; }
		Int_t GetNumberOfTowersInX(void) { return fGlobal_NX_Tower; }
		Int_t GetNumberOfTowersInY(void) { return fGlobal_NY_Tower; }
		Float_t GetHCALTowerSize(void) { return fGlobal_HCAL_Tower_Size; }
		// Function to get number of segments;
		Int_t GetNumberOfSegments() { return fNumberOfSegments; }
		Int_t GetNumberOfPadLayers() { return fNPadLayers; }
		Int_t GetNumberOfHCalLayers() { return fNHCalLayers; }
		Int_t GetNumberOfLayerSeg() {return fLayerSeg; }
		
		Double_t GetFOCALSizeX(void);
		Double_t GetFOCALSizeY(void);
		Double_t GetTowerSize(void);
		Double_t GetFOCALSizeZ(void);
		Double_t GetECALSizeZ();
		Double_t GetECALCenterZ();
		Double_t GetHCALSizeZ();
		Double_t GetHCALCenterZ();
		Double_t GetFOCALSegmentZ(int seg);
		Double_t GetFOCALZ0(void) {return fGlobal_FOCAL_Z0; }
		Double_t  GetTowerGapSize(void) { return fGlobal_TOWER_TOL; }
		Double_t  GetGlobalPixelSize(void) {return fGlobal_Pixel_Size;}
		Double_t  GetGlobalPadSize(void) {return fGlobal_Pad_Size;}
		
		Bool_t GetGeoTowerCenter(Int_t Tower, Double_t &x, Double_t &y, Double_t &z, Int_t segment=-1);
		Bool_t DisabledTower(int Tower);
		Bool_t isPadOrPixel(Int_t Id); /// Still using?? // pad -> true, strip->false;
		
		virtual TObjArray *GetFOCALMicroModule(int fLayer);
		virtual AliFOCALComposition *GetFOCALComposition(int fLayer, int stack);
		TString GetTowerGapMaterial(void) { return fGlobal_Gap_Material; }
		
		bool GetVirtualInfo(Float_t x, Float_t y, Float_t z, Int_t & col, Int_t & row, Int_t & layer, Int_t & segment);
		bool GetXYZFromColRowSeg(Int_t col, Int_t row, Int_t segment, Float_t & x, Float_t & y, Float_t & z);
		bool GetVirtualNColRow(Int_t segment, Int_t & nCol, Int_t & nRow);
		Int_t GetVirtualNSegments();
		Bool_t GetVirtualLayerSegment(Float_t z, Int_t &layer, Int_t &segment);
		Bool_t GetVirtualSegmentFromLayer(Int_t layer, Int_t &segment);
		Int_t GetVirtualSegment(Float_t z);
		Float_t GetVirtualPadSize(Int_t segment);
		Float_t GetVirtualRelativeSensitiveThickness(Int_t segment);
		Float_t GetVirtualPixelTreshold(Int_t segment);
		Float_t GetVirtualSegmentSizeZ(Int_t segment);
		Float_t GetVirtualSegmentZ(Int_t segment);
		bool GetVirtualIsPixel(Int_t segment);
		bool GetVirtualIsHCal(Int_t segment);
		
		void SetUpLayerSegmentMap();
		void SetUpTowerWaferSize();
	
		bool GetUseHCALSandwich() { return fUseSandwichHCAL;}
		
	private:
		TObjArray *fGeometryComposition;
		TObjArray *fGeomObj ; 
		TObjArray *fFrontMatterCompositionBase;
		TObjArray *fPadCompositionBase;
		TObjArray *fPixelCompositionBase;
		TObjArray *fHCalCompositionBase;
		//AliFOCALComposition *fComp[1000];
		//AliFOCALComposition *fPadCompositionBase[200];
		//AliFOCALComposition *fStripCompositionBase[200];
		static AliFOCALGeometry * fGeom;   
		static  Bool_t  fgInit;  
		
		
		Int_t   fGlobal_NX_NY_Pads;
		Int_t   fGlobal_NX_Tower;
		Int_t   fGlobal_NY_Tower;
		Float_t fGlobal_HCAL_Tower_Size;
		Float_t fGlobal_FOCAL_Z0;
		Float_t fGlobal_Pad_Size;
		Float_t fGlobal_PPTOL;
		Float_t fGlobal_SKIN;
		Float_t fGlobal_TOWER_TOL;
		Bool_t  fGlobal_Pixel_Readout; 
		Float_t fGlobal_Pixel_Size; 
		TString fGlobal_Gap_Material;
		Bool_t  fUseSandwichHCAL;
		
		Int_t fLayerSeg;
		Int_t fNPadLayers; // total number of pad layers 
		Int_t fNHCalLayers; // number of HCalLayers
		Int_t fNPixelLayers; // number of pixel layers
		Int_t fPixelLayerLocations[20]; // location of the pixel layers
		Int_t fComposition[100]; //  0 --> pad layer , -1 == strip layers
		Int_t fSegments[100]; //  which layer belongs which segments 
		Int_t fNumberOfLayersInSegments[100];  // nymber of layers in each segment 
		Int_t fNumberOfSegments;  // number of long. segements  
		Int_t fNFrontMatterCompositionBase;
		Int_t fNPadCompositionBase;
		Int_t fNPixelCompositionBase;
		Int_t fNHCalCompositionBase;
		Float_t fLocalLayerZ[100]; //// layer location in z 
		Float_t fLocalSegmentsZ[100]; ///segment location in z 
		Float_t fFrontMatterLayerThickness;
		Float_t fPadLayerThickness;
		Float_t fPixelLayerThickness;
		Float_t fHCalLayerThickness;
		Float_t fLayerThickness[100]; //thickenss of the layers
		Float_t fTowerSize; 
		Float_t fWaferSize; 
		list<int> fDisableTowers;
		
		AliFOCALComposition *fCompositionRet;

		Int_t fVirtualNSegments;
		Int_t fVirtualSegmentsCreated;
		AliFOCALVirtSegment * * fVirtualSegmentComposition;
		
		
	//ClassDef(AliFOCALGeometry,5) ; //Hits manager for set:FOCAL
	ClassDef(AliFOCALGeometry,7) ; //Hits manager for set:FOCAL

};

class AliFOCALVirtSegment {

 public:
 
  AliFOCALVirtSegment():
    fMinLayer(-1),
    fMaxLayer(-1),
    fPadSize(-1),
    fRelativeSensitiveThickness(-1),
    fIsPixel(0),
    fPixelTreshold(-1)
  {}
  
  AliFOCALVirtSegment& operator=(const AliFOCALVirtSegment &fSegment)
  {
    if(this!=&fSegment){
      fMinLayer = fSegment.fMinLayer;
      fMaxLayer = fSegment.fMaxLayer;
      fPadSize = fSegment.fPadSize;
      fRelativeSensitiveThickness = fSegment.fRelativeSensitiveThickness;
      fIsPixel = fSegment.fIsPixel;
      fPixelTreshold = fSegment.fPixelTreshold;
    } 
    return *this;
  }
  
  ~AliFOCALVirtSegment()
  {}
    
  Int_t   fMinLayer;
  Int_t   fMaxLayer;
  Float_t fPadSize;
  Float_t fRelativeSensitiveThickness;
  Int_t   fIsPixel;  // 0: pad or strip; 1: pixel; 2: HCAL
  Float_t fPixelTreshold;
  
};
#endif 


