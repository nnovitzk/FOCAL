/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

//_________________________________________________________________________
// Geometry class  for FOCAL : singleton  
/// T, Gunji 

// --- Standard library ---
#include <fstream>

// --- ROOT system ---

#include "TVector3.h"
#include "TRotation.h" 
#include "TParticle.h"
#include <TGeoManager.h>
#include <TGeoMatrix.h>
#include <Riostream.h>
#include "Rtypes.h"
#include "TObjArray.h"
#include "TObjString.h"

// --- AliRoot header files ---
#include "AliLog.h"
#include "AliFOCALGeometry.h"

ClassImp(AliFOCALGeometry)

Bool_t    AliFOCALGeometry::fgInit      = kFALSE;
AliFOCALGeometry  *AliFOCALGeometry::fGeom = 0;

using namespace std;

//_________________________________________________________________________
AliFOCALGeometry::AliFOCALGeometry():
  fGeometryComposition(0),
  fGeomObj(0),
  fFrontMatterCompositionBase(0),
  fPadCompositionBase(0),
  fPixelCompositionBase(0),
  fHCalCompositionBase(0),
  fGlobal_NX_NY_Pads(0),
  fGlobal_NX_Tower(0),
  fGlobal_NY_Tower(0),
  fGlobal_HCAL_Tower_Size(0),
  fGlobal_FOCAL_Z0(0),
  fGlobal_Pad_Size(0),
  fGlobal_PPTOL(0),
  fGlobal_SKIN(0),
  fGlobal_TOWER_TOL(0),
  fGlobal_Pixel_Readout(kFALSE),
  fGlobal_Pixel_Size(0),
  fGlobal_Gap_Material(0),
  fUseSandwichHCAL(kTRUE),
  fNPadLayers(0),
  fNHCalLayers(0),
  fNPixelLayers(0),
  fNumberOfSegments(0),
  fNFrontMatterCompositionBase(0),
  fNPadCompositionBase(0),
  fNPixelCompositionBase(0),
  fNHCalCompositionBase(0),
  fFrontMatterLayerThickness(0),
  fPadLayerThickness(0),
  fPixelLayerThickness(0),
  fHCalLayerThickness(0),
  fTowerSize(0),
  fWaferSize(0),
  fDisableTowers(0),
  fCompositionRet(0),
  fVirtualNSegments(0),
  fVirtualSegmentsCreated(0),
  fVirtualSegmentComposition(0)
{
  
  // 
  for(int i=0;i<20;i++){
    fPixelLayerLocations[i]=-1;
  }
  for(int i=0;i<100;i++){
    fSegments[i]=-100;
    fComposition[i]=0;
    fNumberOfLayersInSegments[i]=-1;
  }
  for(int i=0;i<100;i++){
    fLocalLayerZ[i]=0;
    fLocalSegmentsZ[i]=0;
    fLayerThickness[i]=0;
  }
  
  fCompositionRet = new AliFOCALComposition();

}  

//_________________________________________________________________________
AliFOCALGeometry::AliFOCALGeometry(AliFOCALGeometry *fGeometry):
  fGeometryComposition(0),
  fGeomObj(0),
  fFrontMatterCompositionBase(0),
  fPadCompositionBase(0),
  fPixelCompositionBase(0),
  fHCalCompositionBase(0),
  fGlobal_NX_NY_Pads(0),
  fGlobal_NX_Tower(0),
  fGlobal_NY_Tower(0),
  fGlobal_HCAL_Tower_Size(0),
  fGlobal_FOCAL_Z0(0),
  fGlobal_Pad_Size(0),
  fGlobal_PPTOL(0),
  fGlobal_SKIN(0),
  fGlobal_TOWER_TOL(0),
  fGlobal_Pixel_Readout(kFALSE),
  fGlobal_Pixel_Size(0),
  fGlobal_Gap_Material(0),
  fUseSandwichHCAL(kTRUE),
  fNPadLayers(0),
  fNPixelLayers(0),
  fNumberOfSegments(0),
  fNFrontMatterCompositionBase(0),
  fNPadCompositionBase(0),
  fNPixelCompositionBase(0),
  fNHCalCompositionBase(0),
  fFrontMatterLayerThickness(0),
  fPadLayerThickness(0),
  fPixelLayerThickness(0),
  fHCalLayerThickness(0),
  fTowerSize(0),
  fWaferSize(0),
  fDisableTowers(0),
  fCompositionRet(0),
  fVirtualNSegments(0),
  fVirtualSegmentsCreated(0),
  fVirtualSegmentComposition(0)
{
  // 
  for(int i=0;i<20;i++){
    fPixelLayerLocations[i]=-1;
  }
  for(int i=0;i<100;i++){
    fSegments[i]=-100;
    fComposition[i]=0;
    fNumberOfLayersInSegments[i]=-1;
  }
  for(int i=0;i<100;i++){
    fLocalLayerZ[i]=0;
    fLocalSegmentsZ[i]=0;
    fLayerThickness[i]=0;
  }
//  fCompositionRet = new AliFOCALComposition();
  *this = fGeometry;
}

//_________________________________________________________________________
AliFOCALGeometry::AliFOCALGeometry (const AliFOCALGeometry &fGeometry):
  fGeometryComposition(fGeometry.fGeometryComposition),
  fGeomObj(fGeometry.fGeomObj),
  fFrontMatterCompositionBase(fGeometry.fFrontMatterCompositionBase),
  fPadCompositionBase(fGeometry.fPadCompositionBase),
  fPixelCompositionBase(fGeometry.fPixelCompositionBase),
  fHCalCompositionBase(fGeometry.fHCalCompositionBase),
  fGlobal_NX_NY_Pads(fGeometry.fGlobal_NX_NY_Pads),
  fGlobal_NX_Tower(fGeometry.fGlobal_NX_Tower),
  fGlobal_NY_Tower(fGeometry.fGlobal_NY_Tower),
  fGlobal_HCAL_Tower_Size(fGeometry.fGlobal_HCAL_Tower_Size),
  fGlobal_FOCAL_Z0(fGeometry.fGlobal_FOCAL_Z0),
  fGlobal_Pad_Size(fGeometry.fGlobal_Pad_Size),
  fGlobal_PPTOL(fGeometry.fGlobal_PPTOL),
  fGlobal_SKIN(fGeometry.fGlobal_SKIN),
  fGlobal_TOWER_TOL(fGeometry.fGlobal_TOWER_TOL),
  fGlobal_Pixel_Readout(fGeometry.fGlobal_Pixel_Readout),
  fGlobal_Pixel_Size(fGeometry.fGlobal_Pixel_Size),
  fGlobal_Gap_Material(fGeometry.fGlobal_Gap_Material),
  fUseSandwichHCAL(fGeometry.fUseSandwichHCAL),
  fNPadLayers(fGeometry.fNPadLayers),
  fNHCalLayers(fGeometry.fNHCalLayers),
  fNPixelLayers(fGeometry.fNPixelLayers),
  fNumberOfSegments(fGeometry.fNumberOfSegments),
  fNFrontMatterCompositionBase(fGeometry.fNFrontMatterCompositionBase),
  fNPadCompositionBase(fGeometry.fNPadCompositionBase),
  fNPixelCompositionBase(fGeometry.fNPixelCompositionBase),
  fNHCalCompositionBase(fGeometry.fNHCalCompositionBase),
  fFrontMatterLayerThickness(fGeometry.fFrontMatterLayerThickness),
  fPadLayerThickness(fGeometry.fPadLayerThickness),
  fPixelLayerThickness(fGeometry.fPixelLayerThickness),
  fHCalLayerThickness(fGeometry.fHCalLayerThickness),
  fTowerSize(fGeometry.fTowerSize),
  fWaferSize(fGeometry.fWaferSize),
  fDisableTowers(fGeometry.fDisableTowers),
  fCompositionRet(fGeometry.fCompositionRet),
  fVirtualNSegments(fGeometry.fVirtualNSegments),
  fVirtualSegmentsCreated(fGeometry.fVirtualSegmentsCreated),
  fVirtualSegmentComposition(fGeometry.fVirtualSegmentComposition)
{
  cout << "Copy constructor for AliFOCALGeometry (NB no deep copy of virt deg)" << endl;
  for(int i=0;i<20;i++){
    fPixelLayerLocations[i]=fGeometry.fPixelLayerLocations[i];
  }
  for(int i=0;i<100;i++){
    fSegments[i]=fGeometry.fSegments[i];
    fComposition[i]=fGeometry.fComposition[i];
    fNumberOfLayersInSegments[i]=fGeometry.fNumberOfLayersInSegments[i];
  }
  for(int i=0;i<100;i++){
    fLocalLayerZ[i]=fGeometry.fLocalLayerZ[i];
    fLocalSegmentsZ[i]=fGeometry.fLocalSegmentsZ[i];
    fLayerThickness[i]=fGeometry.fLayerThickness[i];
  }
}

//_________________________________________________________________________
AliFOCALGeometry& AliFOCALGeometry::operator=(const AliFOCALGeometry &fGeometry)
{
  cout << "Assignment for AliFOCALGeometry (NB no deep copy of virt deg)" << endl;
  if(this!=&fGeometry){
    fGeometryComposition = fGeometry.fGeometryComposition ; 
    fGeomObj = fGeometry.fGeomObj ; 
    fFrontMatterCompositionBase = fGeometry.fFrontMatterCompositionBase ; 
    fPadCompositionBase = fGeometry.fPadCompositionBase ; 
    fPixelCompositionBase = fGeometry.fPixelCompositionBase ;
    fHCalCompositionBase = fGeometry.fHCalCompositionBase ;  
    fGlobal_FOCAL_Z0 = fGeometry.fGlobal_FOCAL_Z0 ; 
    fGlobal_Pad_Size = fGeometry.fGlobal_Pad_Size ; 
    fGlobal_NX_NY_Pads = fGeometry.fGlobal_NX_NY_Pads ; 
    fGlobal_PPTOL = fGeometry.fGlobal_PPTOL ; 
    fGlobal_SKIN = fGeometry.fGlobal_SKIN ; 
    fGlobal_TOWER_TOL = fGeometry.fGlobal_TOWER_TOL ; 
    fGlobal_NX_Tower = fGeometry.fGlobal_NX_Tower ;
    fGlobal_NY_Tower = fGeometry.fGlobal_NY_Tower ; 
    fGlobal_HCAL_Tower_Size = fGeometry.fGlobal_HCAL_Tower_Size ; 
    fGlobal_Pixel_Readout = fGeometry.fGlobal_Pixel_Readout ; 
    fGlobal_Pixel_Size = fGeometry.fGlobal_Pixel_Size ; 
    fGlobal_Gap_Material = fGeometry.fGlobal_Gap_Material ; 
    fUseSandwichHCAL = fGeometry.fUseSandwichHCAL ; 
    fNPadLayers = fGeometry.fNPadLayers ; 
    fNHCalLayers = fGeometry.fNHCalLayers ; 
    fNPixelLayers = fGeometry.fNPixelLayers ; 
    fNumberOfSegments = fGeometry.fNumberOfSegments ; 
    fNFrontMatterCompositionBase = fGeometry.fNFrontMatterCompositionBase ; 
    fNPadCompositionBase = fGeometry.fNPadCompositionBase ; 
    fNPixelCompositionBase = fGeometry.fNPixelCompositionBase ; 
    fNHCalCompositionBase = fGeometry.fNHCalCompositionBase ; 
    fFrontMatterLayerThickness = fGeometry.fFrontMatterLayerThickness ; 
    fPadLayerThickness = fGeometry.fPadLayerThickness ; 
    fPixelLayerThickness = fGeometry.fPixelLayerThickness ;
    fHCalLayerThickness = fGeometry.fHCalLayerThickness ;  
    fTowerSize = fGeometry.fTowerSize ; 
    fWaferSize = fGeometry.fWaferSize ; 
    fDisableTowers = fGeometry.fDisableTowers ;    
    fCompositionRet = fGeometry.fCompositionRet;
    fVirtualNSegments = fGeometry.fVirtualNSegments;
    fVirtualSegmentsCreated = fGeometry.fVirtualSegmentsCreated;
    fVirtualSegmentComposition = fGeometry.fVirtualSegmentComposition;
    {
      for(int i=0;i<20;i++){
	fPixelLayerLocations[i]=fGeometry.fPixelLayerLocations[i];
      }
      for(int i=0;i<100;i++){
	fSegments[i]=fGeometry.fSegments[i];
	fComposition[i]=fGeometry.fComposition[i];
	fNumberOfLayersInSegments[i]=fGeometry.fNumberOfLayersInSegments[i];
      }
      for(int i=0;i<100;i++){
	fLocalLayerZ[i]=fGeometry.fLocalLayerZ[i];
	fLocalSegmentsZ[i]=fGeometry.fLocalSegmentsZ[i];
	fLayerThickness[i]=fGeometry.fLayerThickness[i];
      }
    }
  } 
  return *this;
}

//_________________________________________________________________________
AliFOCALGeometry *AliFOCALGeometry::GetInstance(){
  // Returns the pointer of the unique instance
  //cout << "AliFOCALGeometry::GetInstance()" << endl;
  if(fGeom==0x0){
    cout<<"Create instance : Using default geometry"<<endl;
    fGeom = new AliFOCALGeometry();
    fGeom->Init();
    cout<<"Done !! "<<fgInit<<endl;
  }else{
    if(fgInit==kFALSE){
      fGeom = new AliFOCALGeometry();
      fGeom->Init();
    }
    cout<<" Instance is already there "<<fGeom->GetFOCALZ0()<<" "<<fgInit<<endl;
  }
  //cout << "return " <<  fGeom << endl;
  return static_cast<AliFOCALGeometry *>( fGeom );

}
//_________________________________________________________________________
AliFOCALGeometry *AliFOCALGeometry::GetInstance(const char *geometryfile){
  // Returns the pointer of the unique instance
  if(fGeom==0x0){
    cout<<"Create instance : Using geometry configured by "<<geometryfile<<endl;
    fGeom = new AliFOCALGeometry();
    fGeom->Init(geometryfile);
    cout<<"Done !! "<<fgInit<<endl;
  }else{
    if(fgInit==kFALSE){
      fGeom = new AliFOCALGeometry();
      fGeom->Init(geometryfile);
    }
    cout<<" Instance is already there "<<fGeom->GetFOCALZ0()<<" "<<fgInit<<endl;
  }
  return static_cast<AliFOCALGeometry *>( fGeom );
}


//_________________________________________________________________________
AliFOCALGeometry::~AliFOCALGeometry(void)
{
  // tor
  cout <<"Destructor for AliFOCALGeometry" << endl;
  /*
  cout << " Calling Delete for arrays" << endl;
  fGeometryComposition->Clear();
//  fGeometryComposition->Delete();
//  fGeomObj->Clear();
  fGeomObj->Delete();
//  fPadCompositionBase->Clear();
  fPadCompositionBase->Delete();
//  fPixelCompositionBase->Clear();
  fPixelCompositionBase->Delete();
//  fFrontMatterCompositionBase->Clear();
  fFrontMatterCompositionBase->Delete();
  fHCalCompositionBase->Delete();
  */

  delete fGeometryComposition;
  delete fGeomObj;
  delete fPadCompositionBase;
  delete fHCalCompositionBase;
  delete fPixelCompositionBase;
  delete fFrontMatterCompositionBase;

  fGeometryComposition = 0;
  fGeomObj = 0;
  fPadCompositionBase = 0;
  fHCalCompositionBase = 0;
  fPixelCompositionBase = 0;
  fFrontMatterCompositionBase = 0;

//  fCompositionRet->Clear(); 
//  fCompositionRet->Delete();
//  delete fCompositionRet;
//  fCompositionRet = 0;
  
  // MvL: this causes a double delete when segments are also generated in sims. Not sure why, but commenting out for now (Memory leak!)
  /*
  for( Int_t seg = 0; seg < fVirtualSegmentsCreated; seg++) {
    cout << "delete fVirtualSegmentComposition["<<seg<<"]"<<endl;
    delete fVirtualSegmentComposition[seg];
    fVirtualSegmentComposition[seg]=0;
  }
  
  delete [] fVirtualSegmentComposition;
  fVirtualSegmentComposition = 0;
  fVirtualSegmentsCreated = 0;
  */

  fgInit=kFALSE;
  cout << "dtor done" << endl;
}

//_________________________________________________________________________
void AliFOCALGeometry::Init(const char *geometryfile)
{
  cout<<" Geometry init for FoCAL from "<<geometryfile<<endl;
  if((strcmp(geometryfile,"default"))){
    SetParameters(geometryfile);
  }else{
    SetParameters();
  }
  cout <<" Geometry init done." << endl;
  BuildComposition();
  fgInit = kTRUE;

}
//_________________________________________________________________________
void AliFOCALGeometry::Init()
{
  cout<<" Geometry init for FoCAL "<<endl;
  SetParameters();
  BuildComposition();
  fgInit = kTRUE;
}

//_________________________________________________________________________
void AliFOCALGeometry::BuildComposition(void)
{
  cout<<" AliFOCALGeometry::BuildComposition "<<endl;
  fGeometryComposition = new TObjArray(10000);
  fGeomObj = new TObjArray(10000);
  

  int nlayers=fNPadLayers + fNPixelLayers + fNHCalLayers;

  ////// Si pad micro module  for the first Pad layer
  int NumberOfObjects = 0; 
  for(int i=0;i<nlayers; i++){
    if (i < fNPadLayers + fNPixelLayers) {
      // Check whether it is a pixel layer
      Int_t isPixel = 0;
      for (Int_t iPix = 0; iPix < fNPixelLayers && !isPixel; iPix++) {
	cout << "Check pixel layer idx " << iPix << " loc " << fPixelLayerLocations[iPix] << " i= " << i << endl;
	if (i == fPixelLayerLocations[iPix])
	  isPixel = 1;
      }
      if (isPixel) {
	cout << "Adding pixel layer at layer " << i << endl;
	for(int j=0;j<fPixelCompositionBase->GetEntriesFast();j++){
	  AliFOCALComposition *ftmp = (AliFOCALComposition*)fPixelCompositionBase->UncheckedAt(j);
	  AliFOCALComposition *fComp = (AliFOCALComposition*)ftmp->Clone();
	  fComp -> SetLayerNumber(i);
	  fGeomObj->AddLast(fComp);		       
	  fComp->Clear();  
	  ftmp->Clear();
	  fComp = 0;
	  ftmp = 0;
	  NumberOfObjects++;   
	}
	fLayerThickness[i] = fPixelLayerThickness;
      }
      else {
	for(int j=0;j<fPadCompositionBase->GetEntriesFast();j++){
	  AliFOCALComposition *ftmp = (AliFOCALComposition*)fPadCompositionBase->UncheckedAt(j);
	  AliFOCALComposition *fComp = (AliFOCALComposition*)ftmp->Clone();
	  //      cout<<" **** "<<fComp->Material()<<" "<<fComp->Layer()<<" "<<fComp->Stack()<<" "<<fComp->CenterZ()<<" "<<fComp->SizeZ()<<" "<<endl;
	  fComp->SetLayerNumber(i);
	  fGeomObj->AddLast(fComp);
	  fComp->Clear();
	  ftmp->Clear();
	  fComp = 0;
	  ftmp = 0;
	  NumberOfObjects++;
	}
	fLayerThickness[i] = fPadLayerThickness;
      }
    }
    else {
      for(int j=0;j<fHCalCompositionBase->GetEntriesFast();j++){
	AliFOCALComposition *ftmp = (AliFOCALComposition*)fHCalCompositionBase->UncheckedAt(j);
	AliFOCALComposition *fComp = (AliFOCALComposition*)ftmp->Clone();
	//      cout<<" **** "<<fComp->Material()<<" "<<fComp->Layer()<<" "<<fComp->Stack()<<" "<<fComp->CenterZ()<<" "<<fComp->SizeZ()<<" "<<endl;
	fComp->SetLayerNumber(i);
	fGeomObj->AddLast(fComp);
	fComp->Clear(); // MvL: Not needed?
	ftmp->Clear(); // MvL: Not needed?
	fComp = 0; // MvL: Not needed?
	ftmp = 0; // MvL: Not needed?
	NumberOfObjects++;
      }
      fLayerThickness[i] = fHCalLayerThickness;
    }
  }

  for(int i=0;i<nlayers;i++){
    fLocalLayerZ[i] = 0;
    for(int j=0;j<i;j++){
      fLocalLayerZ[i] += fLayerThickness[j];
    }
  }
  for(int i=0;i<nlayers;i++){
    fLocalSegmentsZ[fSegments[i]] += fLayerThickness[i];
  }

  fLocalLayerZ[nlayers]=-1;

  ////// add the front matter to the tower
  for(int j=0;j<fFrontMatterCompositionBase->GetEntriesFast();j++){
    AliFOCALComposition *ftmp = (AliFOCALComposition*)fFrontMatterCompositionBase->UncheckedAt(j);
    AliFOCALComposition *fComp = (AliFOCALComposition*)ftmp->Clone();
    fComp -> SetLayerNumber(-1);
    fGeomObj->AddLast(fComp);
    fComp->Clear();
    ftmp->Clear();
    fComp = 0;
    ftmp = 0;
    NumberOfObjects++;
  }

  //// re-iterate to set the longitudinal positions 
  AliFOCALComposition *ftmp;// = new AliFOCALComposition();
  for(int i=0;i<fGeomObj->GetEntriesFast(); i++){
    ftmp = (AliFOCALComposition*) fGeomObj->UncheckedAt(i);
    if(ftmp->Layer()>=0){
      ftmp->SetCenterZ(fFrontMatterLayerThickness+fLocalLayerZ[ftmp->Layer()]+ftmp->CenterZ()+ftmp->SizeZ()/2); /// this is pad/strip layer
    }else{
      ftmp->SetCenterZ(ftmp->CenterZ()+ftmp->SizeZ()/2); /// this is frontmatter 
    }
    //cout<<ftmp->Material()<<" "<<ftmp->Layer()<<" "<<ftmp->Stack()<<" "<<ftmp->CenterZ()<<" "<<ftmp->SizeZ()<<endl;
    fGeometryComposition->AddLast(ftmp);
  }



  ///// so far, we defined the tower composition 
  ///// finally let's fill the material between towers.
  ///// idea is to implement 4 gap material associated with one tower
  ///// -x --> fGlobal_TOWER_TOL/2 x TowerSizeY()+fGlobal_TOWER_TOL x FOCALSizeZ
  /////  x --> fGlobal_TOWER_TOL/2 x TowerSizeY()+fGlobal_TOWER_TOL x FOCALSizeZ
  ///// -y --> TowerSizeX() x fGlobal_TOWER_TOL/2  x FOCALSizeZ
  /////  y --> TowerSizeX() x fGlobal_TOWER_TOL/2  x FOCALSizeZ

//  AliFOCALComposition *fGap = new AliFOCALComposition();
//  for(int i=0;i<4;i++){
//    float cx=0;
//    float cy=0;
//    float cz=0;
//    float dx=0;
//    float dy=0;
//    float dz=0;
//    if(i==0){ //-x 
//      cx  = -1*0.5*GetTowerSize()-fGlobal_TOWER_TOL/4;
//      cy  = 0;
//      cz  = GetFOCALZ0();
//      dx  = fGlobal_TOWER_TOL/4;
//      dy  = GetTowerSize()+fGlobal_TOWER_TOL;
//      dz  = GetFOCALSizeZ(); 
//    }else if(i==1){ // +x 
//      cx  = 0.5*GetTowerSize()+fGlobal_TOWER_TOL/4;
//      cy  = 0;
//      cz  = GetFOCALZ0();
//      dx  = fGlobal_TOWER_TOL/4;
//      dy  = GetTowerSize()+fGlobal_TOWER_TOL;
//      dz  = GetFOCALSizeZ(); 
//    }else if(i==2){ // -y 
//      cx  = 0; 
//      cy  = -0.5*GetTowerSize()-fGlobal_TOWER_TOL/4;
//      cz  = GetFOCALZ0();
//      dx  = GetTowerSize();
//      dy  = fGlobal_TOWER_TOL/4;
//      dz  = GetFOCALSizeZ(); 
//    }else if(i==3){ // +y 
//      cx  = 0; 
//      cy  = 0.5*GetTowerSize()+fGlobal_TOWER_TOL/4;
//      cz  = GetFOCALZ0();
//      dx  = GetTowerSize();
//      dy  = fGlobal_TOWER_TOL/4;
//      dz  = GetFOCALSizeZ(); 
//    }

//    fGap->SetCompositionParameters(
//				   fGlobal_Gap_Material, 0, 0, i,
//				   cx,
//				   cy, 
//				   cz,
//				   dx,
//				   dy,
//				   dz
//				   );
//    
//    fGeometryComposition->AddLast(fGap);
//  }

}


//_________________________________________________________________________
void AliFOCALGeometry::SetParameters(){

  cout<<" set default parameters "<<endl;

  /////// this is default setting for the global parameters
  fGlobal_FOCAL_Z0 = 500;
  fGlobal_NX_NY_Pads = 8;
  fGlobal_Pad_Size = 1.13-0.011;
  fGlobal_PPTOL = 0.011;
  fGlobal_SKIN = 0.1355;
  fGlobal_NX_Tower = 20;
  fGlobal_NY_Tower = 20;
  fGlobal_HCAL_Tower_Size = 2.5;
  //fGlobal_TOWER_TOL = 0.15;
  fGlobal_TOWER_TOL = 0.0;
  fGlobal_Gap_Material = "Air";

  fNPadLayers=21;
  fNumberOfSegments = 3;
  fNumberOfLayersInSegments[0]=7;
  fNumberOfLayersInSegments[1]=7;
  fNumberOfLayersInSegments[2]=7;


  fNPixelLayers=4;
  fPixelLayerLocations[0]=2;
  fPixelLayerLocations[1]=4;
  fPixelLayerLocations[2]=6;
  fPixelLayerLocations[3]=8;

  fGlobal_Pixel_Readout = kFALSE;
  //fGlobal_Pixel_Readout = kTRUE;
  fGlobal_Pixel_Size = 0.0055;


  fNPadCompositionBase=0;
  fNPixelCompositionBase=0;
  fNFrontMatterCompositionBase=0;


  SetUpLayerSegmentMap();
  SetUpTowerWaferSize();


  float wafer_size = GetTowerSize();
  float center_z = 0;
  //// define base pad micro module compotents


  fFrontMatterCompositionBase = new TObjArray(10000);
  fPadCompositionBase = new TObjArray(10000);
  fPixelCompositionBase = new TObjArray(10000);

  AliFOCALComposition *fCompBaseFrontMatter[500];
  AliFOCALComposition *fCompBase[500];
  AliFOCALComposition *fCompBasePixel[500];
  for(int i=0;i<500;i++){
    fCompBaseFrontMatter[i] = new AliFOCALComposition();
    fCompBase[i] = new AliFOCALComposition();
    fCompBasePixel[i] = new AliFOCALComposition();
  }

  //fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
  fCompBase[fNPadCompositionBase]->SetCompositionParameters("Alloy",0,0,0, 0,0,center_z, wafer_size,wafer_size,0.35);
  //fCompBase[fNPadCompositionBase]->SetCompositionParameters("PureW",0,0,0, 0,0,center_z, wafer_size,wafer_size,0.35);
  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
  center_z+= fCompBase[fNPadCompositionBase]->GetThickness();
  fCompBase[fNPadCompositionBase]->Clear();
  fCompBase[fNPadCompositionBase] = 0;
  fNPadCompositionBase++;

  //  fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
  fCompBase[fNPadCompositionBase]->SetCompositionParameters("G10",0,1, 1, 0,0,center_z, wafer_size,wafer_size, 0.01);
  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
  center_z+= fCompBase[fNPadCompositionBase]->GetThickness();
  fCompBase[fNPadCompositionBase]->Clear();
  fCompBase[fNPadCompositionBase] = 0;  
  fNPadCompositionBase++;


  for(int ix=0;ix<fGlobal_NX_NY_Pads;ix++){
    for(int iy=0;iy<fGlobal_NX_NY_Pads;iy++){
      double x, y;
      GetGeoPadCenterLocal(iy, ix, x, y);
      //fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
      fCompBase[fNPadCompositionBase]->SetCompositionParameters("SiPad",0,2, ix+iy*fGlobal_NX_NY_Pads, 
								x,y,center_z, fGlobal_Pad_Size, fGlobal_Pad_Size, 0.0535);
      fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
      fCompBase[fNPadCompositionBase]->Clear();
      fCompBase[fNPadCompositionBase] = 0;
      fNPadCompositionBase++;
    }
  }
  center_z+= 0.0535;
  //fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
  fCompBase[fNPadCompositionBase]->SetCompositionParameters("G10",0,3, 3, 0,0,center_z, wafer_size,wafer_size,0.01);
  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
  center_z+= fCompBase[fNPadCompositionBase]->GetThickness();
  fCompBase[fNPadCompositionBase]->Clear();
  fCompBase[fNPadCompositionBase] = 0;
  fNPadCompositionBase++;

  //fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
  fCompBase[fNPadCompositionBase]->SetCompositionParameters("Ceramic",0,4, 4, 0,0,center_z, wafer_size,wafer_size,0.08);
  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
  center_z+= fCompBase[fNPadCompositionBase]->GetThickness();
  fCompBase[fNPadCompositionBase]->Clear();
  fCompBase[fNPadCompositionBase] = 0;
  fNPadCompositionBase++;

  //  fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
  fCompBase[fNPadCompositionBase]->SetCompositionParameters("Air",0,5, 5, 0,0,center_z, wafer_size,wafer_size,0.1);
  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
  center_z+= fCompBase[fNPadCompositionBase]->GetThickness();
  fCompBase[fNPadCompositionBase]->Clear();
  fCompBase[fNPadCompositionBase] = 0;
  fNPadCompositionBase++;

  fPadLayerThickness = center_z;
  

  //// define base strip micro module compotents


  center_z = 0;
  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("G10",fPixelLayerLocations[0],0,0,
				      0,0,center_z, 
				      wafer_size,wafer_size,0.01);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("SiPixelX", fPixelLayerLocations[0],1, 1,  
				      0,0,center_z, 
				      wafer_size,wafer_size,0.0535);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("G10", fPixelLayerLocations[0],2, 2, 
				      0,0,center_z, 
				      wafer_size,wafer_size,0.05);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("Air", fPixelLayerLocations[0],3, 3, 
						   0,0,center_z, 
						   wafer_size,wafer_size,0.01);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;  
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("G10", fPixelLayerLocations[0],4, 4, 
						   0,0,center_z, 
						   wafer_size,wafer_size,0.01);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("SiPixelY", fPixelLayerLocations[0],5, 5,  
						   0,0,center_z, 
						   wafer_size,wafer_size,0.0535);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;  
  fNPixelCompositionBase++;

  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("G10", fPixelLayerLocations[0],6, 6, 
						   0,0,center_z, 
						   wafer_size,wafer_size,0.05);
  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
  center_z += fCompBasePixel[fNPixelCompositionBase]->GetThickness();
  fCompBasePixel[fNPixelCompositionBase]->Clear();
  fCompBasePixel[fNPixelCompositionBase] = 0;
  fNPixelCompositionBase++;


  fPixelLayerThickness = center_z;


  ///FrontMatter
  center_z = 0;
  //  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("G10",0,0,0,
										    0,0,center_z, 
										    wafer_size,wafer_size,0.01);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("SiPixelX", 
									       0,1, 1,  
				      0,0,center_z, 
				      wafer_size,wafer_size,0.0535);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("G10", 0,2, 2, 
									       0,0,center_z, 
									       wafer_size,wafer_size,0.05);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("Air", 0,3, 3, 
									       0,0,center_z,
									       wafer_size,wafer_size,0.01);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;  
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("G10", 0,4, 4, 
						   0,0,center_z, 
						   wafer_size,wafer_size,0.01);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("SiPixelY", 0,5, 5,  
						   0,0,center_z, 
						   wafer_size,wafer_size,0.0535);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;  
  fNFrontMatterCompositionBase++;

  //  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->SetCompositionParameters("G10", 0,6, 6, 
						   0,0,center_z, 
						   wafer_size,wafer_size,0.05);
  fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  center_z += fCompBaseFrontMatter[fNFrontMatterCompositionBase]->GetThickness();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  fCompBaseFrontMatter[fNFrontMatterCompositionBase] = 0;
  fNFrontMatterCompositionBase++;


  fFrontMatterLayerThickness = center_z;


  //  delete [] fCompBase;
  //  delete [] fCompBasePixel;

  for(int i=0;i<500;i++){
    if (fCompBaseFrontMatter[i])
      delete fCompBaseFrontMatter[i];
    if (fCompBase[i])
      delete fCompBase[i];
    if (fCompBasePixel[i])
      delete fCompBasePixel[i];
  }
  
}

//_________________________________________________________________________
void AliFOCALGeometry::SetParameters(const char *geometryfile)
{
  /////// this is default setting for the global parameters
  fGlobal_FOCAL_Z0 = 500;
  fGlobal_NX_NY_Pads = 8;
  fGlobal_Pad_Size = 1.13-0.011;
  fGlobal_PPTOL = 0.011;
  fGlobal_SKIN = 0.1355;
  fGlobal_NX_Tower = 20;
  fGlobal_NY_Tower = 20;
  fGlobal_HCAL_Tower_Size = 2.5;
  fGlobal_TOWER_TOL = 0.15;
  fGlobal_Gap_Material = "Air";
	
  /// Danilo's Change!!!!!!!!!!!!
  /*fNLayers=21;
    fNumberOfSegments = 3;
    fNumberOfLayersInSegments[0]=7;
    fNumberOfLayersInSegments[1]=7;
    fNumberOfLayersInSegments[2]=7;
    fNLayers=21;*/
	
  fNPixelLayers=4;
  fPixelLayerLocations[0]=2;
  fPixelLayerLocations[1]=4;
  fPixelLayerLocations[2]=6;
  fPixelLayerLocations[3]=8;
	
  fGlobal_Pixel_Readout = kFALSE;
  fGlobal_Pixel_Size = 0.0055;
	
  fTowerSize = 0;
  fWaferSize = 0;
	
  fNPadCompositionBase=0;
  fNPixelCompositionBase=0;
	
  ifstream fin(geometryfile);
  if(fin.fail())
    {
      AliLog::Message(AliLog::kError,"No geometry file for FoCAL. Use default ones", "AliFOCALGeometry.cxx", "AliFOCALGeometry.cxx", "UpdateDefaults()", "AliFOCALGeometry.cxx", __LINE__);
      SetParameters();
      return ;
    }
  else
    {
      cout<<" Use Geometryfile "<<geometryfile<<endl;
    }
	
  AliFOCALComposition *fPadCompDummy[50];
  AliFOCALComposition *fHCalCompDummy[50];
  AliFOCALComposition *fPixelCompDummy[50];
  AliFOCALComposition *fFrontMatterCompDummy[50];
  Int_t NCPad = 0; 
  Int_t NCHCal = 0; 
  Int_t NCPixel = 0; 
  Int_t NCFrontMatter = 0; 
	
  int npadlayers=0;
  int npixlayers=0;
  int pixl[10];
	
  string input;
  const char *pbrk;
  TString delim(' ');
	
  cout<<" start loading geometry file "<<endl;
  while(getline(fin, input))
    {
      cout<<input.c_str()<<" "<<endl;
      pbrk=strpbrk("#",input.c_str());
      if(pbrk!=NULL)
	{
	  cout << "Skipping comment" << endl;
	  continue;
	}
		
      TString str(input.c_str());
      TObjArray *obj = str.Tokenize(delim);
		
      TObjString *a0 = (TObjString*)obj->UncheckedAt(0);
      TString command(a0->GetString());
      if(command.Contains("COMPOSITION")!=0)  /// definition of the composition 
	{
	  int stack;
	  char *cdata = (char*)command.Data();
	  TString material;
	  float cx, cy, cz;
	  float dx, dy, dz;
			
	  TObjString *objstr[10];
			
	  objstr[1] = (TObjString*)obj->UncheckedAt(1);
	  objstr[2] = (TObjString*)obj->UncheckedAt(2);
	  objstr[3] = (TObjString*)obj->UncheckedAt(3);
	  objstr[4] = (TObjString*)obj->UncheckedAt(4);
	  objstr[5] = (TObjString*)obj->UncheckedAt(5);
	  objstr[6] = (TObjString*)obj->UncheckedAt(6);
			
	  material = objstr[1]->GetString();
	  cx = objstr[2]->GetString().Atof();
	  cy = objstr[3]->GetString().Atof();
	  dx = objstr[4]->GetString().Atof();
	  dy = objstr[5]->GetString().Atof();
	  dz = objstr[6]->GetString().Atof();
	  cz = 0;
			
	  if(command.Contains("PAD")!=0)
	    {
	      sscanf(cdata, "COMPOSITION_PAD_S%d",&stack);
	      fPadCompDummy[NCPad] = new AliFOCALComposition();
	      fPadCompDummy[NCPad]->SetCompositionParameters(material, 0, stack, stack, 
							     cx,cy,cz,dx,dy,dz);
	      NCPad++;
	      cout<<cdata<<endl;
	    }
	  if(command.Contains("HCAL")!=0)
	    {
	      sscanf(cdata, "COMPOSITION_HCAL_S%d",&stack);
	      fHCalCompDummy[NCHCal] = new AliFOCALComposition();
	      fHCalCompDummy[NCHCal]->SetCompositionParameters(material, 0, stack, stack, 
							       cx,cy,cz,dx,dy,dz);
	      NCHCal++;
	      cout<<cdata<<endl;
	    }
	  else if(command.Contains("PIX")!=0)
	    {
	      sscanf(cdata, "COMPOSITION_PIX_S%d",&stack);
	      fPixelCompDummy[NCPixel] = new AliFOCALComposition();
	      fPixelCompDummy[NCPixel]->SetCompositionParameters(material, 0, stack, stack, 
								 cx,cy,cz,dx,dy,dz);
	      NCPixel++;
	    }
	  else if(command.Contains("FM")!=0)
	    {
	      sscanf(cdata, "COMPOSITION_FM_S%d",&stack);
	      fFrontMatterCompDummy[NCFrontMatter] = new AliFOCALComposition();
	      fFrontMatterCompDummy[NCFrontMatter]->SetCompositionParameters(material, 0, stack, stack, cx, cy, cz, dx, dy, dz);
	      cout<<cdata<<" "<<material<<" "<<stack<<" "<<endl;
	      NCFrontMatter++;
	    }
	}
		
      if(command.Contains("GLOBAL")!=0)
	{
	  TObjString *objstr[10];
	  if(command.Contains("PAD_SIZE")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_Pad_Size =  objstr[1]->GetString().Atof();
	      cout<<" Pad Size is set to : "<<fGlobal_Pad_Size<<endl;
	    }
			
	  if(command.Contains("PAD_NX_NY")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_NX_NY_Pads =  objstr[1]->GetString().Atoi();
	      cout<<" N * N Pads is defined as : "<<fGlobal_NX_NY_Pads<<endl;
	    }
			
	  if(command.Contains("PAD_PPTOL")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_PPTOL =  objstr[1]->GetString().Atof();
	      cout<<" Pad-Pad distance is set to : "<<fGlobal_PPTOL<<endl;
	    }
			
	  if(command.Contains("PAD_SKIN")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_SKIN =  objstr[1]->GetString().Atof();
	      cout<<" Pad-Edge distance is set to : "<<fGlobal_SKIN<<endl;
	    }
			
	  if(command.Contains("FOCAL_Z")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_FOCAL_Z0 =  objstr[1]->GetString().Atof();
	      cout<<" Z-Location of the FoCAL is set to : "<<fGlobal_FOCAL_Z0<<endl;
	    }
			
	  if(command.Contains("NX_TOWER_FOCAL")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_NX_Tower =  objstr[1]->GetString().Atoi();
	      cout<<" Number of Towers of FOCAL in x direction is set to : "<<fGlobal_NX_Tower<<endl;
	    }
			
	  if(command.Contains("NY_TOWER_FOCAL")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_NY_Tower =  objstr[1]->GetString().Atoi();
	      cout<<" Number of Towers of FOCALin y direction is set to : "<<fGlobal_NY_Tower<<endl;
	    }
			
	  if(command.Contains("HCAL_TOWER_SIZE")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_HCAL_Tower_Size =  objstr[1]->GetString().Atof();
	      cout<<" The size of the HCAL readout tower will be : "<<fGlobal_HCAL_Tower_Size<<endl;
	    }
									
	  if(command.Contains("GLOBAL_TOWER_TOL")!=0)
	    {
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_TOWER_TOL =  objstr[1]->GetString().Atof();
	      objstr[2] = (TObjString*)obj->UncheckedAt(2);
	      fGlobal_Gap_Material = objstr[2]->GetString();
	      cout<<" Tower-Tower Gap is set to : "<<fGlobal_TOWER_TOL<<" Material : "<<fGlobal_Gap_Material<<endl;
	    }
	}
		
		
      if(command.Contains("COMMAND")!=0)
	{
	  char *com = (char*)command.Data();
	  TObjString *objstr[10];
	  objstr[1] = (TObjString*)obj->UncheckedAt(1);
	  if(command.Contains("NUMBER_OF_PAD_LAYERS")!=0)
	    {
	      //sscanf(com, "COMMAND_NUMBER_OF_PAD_LAYERS_%d",&npadlayers);
	      npadlayers =  objstr[1]->GetString().Atoi();
	      cout<<" number of pad layers "<<npadlayers<<endl;
	    }
	  /// Danilo's Change
	  ///*********************************************************
	  else if(command.Contains("NUMBER_OF_HCAL_LAYERS")!=0)
	    {
	      fNHCalLayers = objstr[1]->GetString().Atoi();
	      cout << " Number of HCAL layers " << fNHCalLayers <<endl;
        if(fNHCalLayers==1) fUseSandwichHCAL = kFALSE;
	    }
	  else if(command.Contains("NUMBER_OF_SEGMENTS")!=0)
	    {
	      //sscanf(com, "COMMAND_NUMBER_OF_SEGMENTS_%d",&fNumberOfSegments);
	      fNumberOfSegments = objstr[1]->GetString().Atoi();
	      cout << " Number of Segments " << fNumberOfSegments <<endl;
	    }
	  ///*********************************************************
	  else if(command.Contains("INSERT_PIX")!=0)
	    {
	      sscanf(com, "COMMAND_INSERT_PIX_AT_L%d",&pixl[npixlayers]);
	      cout<<" number of pixel layer "<<npixlayers<<" : location "<<pixl[npixlayers]<<endl;
	      npixlayers++;
	    }
	  else if(command.Contains("COMMAND_PIXEL_READOUT_ON")!=0)
	    {
	      fGlobal_Pixel_Readout = kTRUE;
	      objstr[1] = (TObjString*)obj->UncheckedAt(1);
	      fGlobal_Pixel_Size =  objstr[1]->GetString().Atof();	
	      cout<<" Pixel readout on (for MASPS): pixel size is set to : "<<fGlobal_Pixel_Size<<endl;
	    }	
	}
		
		
      if(command.Contains("VIRTUAL")!=0) {
		 
	Int_t segment, minlayer, maxLayer, isPixel;
	Float_t padSize, sensitiveThickness, pixelTreshold;

	char *cdata = (char*)command.Data();
		
	TObjString *objstr[10];
		
	if(command.Contains("N_SEGMENTS")!=0)
	  {
	    objstr[1] = (TObjString*)obj->UncheckedAt(1);
	    fVirtualNSegments =  objstr[1]->GetString().Atoi();
	    cout<<" Number of Virtual Segments is set to : "<< fVirtualNSegments <<endl;
	  }
		  
	if(command.Contains("SEGMENT_LAYOUT")!=0)
	  {
	    objstr[1] = (TObjString*)obj->UncheckedAt(1);
	    objstr[2] = (TObjString*)obj->UncheckedAt(2);
	    objstr[3] = (TObjString*)obj->UncheckedAt(3);
	    objstr[4] = (TObjString*)obj->UncheckedAt(4);
	    objstr[5] = (TObjString*)obj->UncheckedAt(5);
	    objstr[6] = (TObjString*)obj->UncheckedAt(6);
	  		
			
	    minlayer = objstr[1]->GetString().Atoi();
	    maxLayer = objstr[2]->GetString().Atoi();
	    padSize = objstr[3]->GetString().Atof();
	    sensitiveThickness = objstr[4]->GetString().Atof();
	    isPixel = objstr[5]->GetString().Atoi();
	    pixelTreshold = objstr[6]->GetString().Atof();
			
	    if (!fVirtualSegmentComposition) {
	      if (fVirtualNSegments <= 0) {
		cout << "Making 20 segments" << endl;
		fVirtualSegmentComposition = new AliFOCALVirtSegment* [20];
		for (Int_t seg = 0; seg < 20; seg++)
		  fVirtualSegmentComposition[seg] = new AliFOCALVirtSegment();
		fVirtualNSegments = 20;
		fVirtualSegmentsCreated = 20;
	      }
	      else {
		cout << "Making " << fVirtualNSegments << " segments" << endl;
		fVirtualSegmentComposition = new AliFOCALVirtSegment* [fVirtualNSegments];
		for (Int_t seg = 0; seg < fVirtualNSegments; seg++)
		  fVirtualSegmentComposition[seg] = new AliFOCALVirtSegment();
		fVirtualSegmentsCreated = fVirtualNSegments;
	      }
	    }
			
	    sscanf(cdata, "VIRTUAL_SEGMENT_LAYOUT_N%d",&segment);
	    if (segment > fVirtualNSegments) {
	      continue;
	    }
	    fVirtualSegmentComposition[segment]->fMinLayer = minlayer;
	    fVirtualSegmentComposition[segment]->fMaxLayer = maxLayer;
	    fVirtualSegmentComposition[segment]->fPadSize = padSize;
	    fVirtualSegmentComposition[segment]->fRelativeSensitiveThickness = sensitiveThickness;
	    fVirtualSegmentComposition[segment]->fPixelTreshold = pixelTreshold;
	    fVirtualSegmentComposition[segment]->fIsPixel = isPixel;
				
	    cout<<"Segment number " << segment << " defined with (minLayer, maxLayer, padSize, isPixel): (" 
		<< minlayer<< ", " << maxLayer << ", " <<  padSize << ", " << isPixel << ")" <<endl;
	  }
		  
      }
		
    }
	
	
  /////// re-arrange the longitudinal components
  cout<<" ********************************** "<<endl;
  fNPixelLayers = npixlayers;
  for(int i=0;i<npixlayers;i++)
    {
      fPixelLayerLocations[i] = pixl[i];
    }
	
  fNPadLayers = npadlayers;
	
  /// Danilo's Change
  ///*********************************************************
  fLayerSeg = (fNPadLayers+fNPixelLayers+fNHCalLayers)/fNumberOfSegments;
  cout << "layer/segment: " << fLayerSeg << endl;
  if(fNumberOfSegments >= 100)
    {
      cout << "You reach the segments limits!\nSetting Number of segments to: 100\n";
      fNumberOfSegments = 99;
      cout << "New number of segments: " << fNumberOfSegments << "\n";
      fLayerSeg = (fNPadLayers+fNPixelLayers+fNHCalLayers)/fNumberOfSegments;
    }
  if((fNPadLayers+fNPixelLayers+fNHCalLayers)%fNumberOfSegments)
    {
      fNumberOfSegments++;  
      for(int i = 0; i < fNumberOfSegments; i++)
	{
	  fNumberOfLayersInSegments[i] = fLayerSeg;
	}
      cout << "Number of segments: " << fNumberOfSegments << "\n";
    }
  else
    {
      for(int i = 0; i < fNumberOfSegments; i++)
	{
	  fNumberOfLayersInSegments[i] = fLayerSeg;
	}
    }
  ///*********************************************************

  SetUpLayerSegmentMap();
	
  float center_z = 0; 
  cout<<" ********************************** "<<endl;
	
  AliFOCALComposition *fCompBase[500];
  AliFOCALComposition *fCompBasePixel[500];
  //AliFOCALComposition *fCompBaseFrontMatter[500];
	
	
  fPadCompositionBase = new TObjArray(10000);
  fHCalCompositionBase = new TObjArray(10000);
  fPixelCompositionBase = new TObjArray(10000);
  fFrontMatterCompositionBase = new TObjArray(10000);
	
  for(int i=0;i<NCPad;i++)
    {
      if(fPadCompDummy[i]->Material()!="SiPad")
	{
	  fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
	  fCompBase[fNPadCompositionBase] -> SetCompositionParameters(fPadCompDummy[i]->Material(),
								      fPadCompDummy[i]->Layer(),
								      fPadCompDummy[i]->Stack(),
								      fPadCompDummy[i]->Id(),
								      fPadCompDummy[i]->CenterX(),
								      fPadCompDummy[i]->CenterY(),
								      center_z,
								      fPadCompDummy[i]->SizeX(),
								      fPadCompDummy[i]->SizeY(),
								      fPadCompDummy[i]->SizeZ());
	  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
	  fCompBase[fNPadCompositionBase]->Clear();
	  fNPadCompositionBase++;
			
	  if(fTowerSize<fPadCompDummy[i]->SizeX())
	    {
	      fTowerSize = fPadCompDummy[i]->SizeX();
	    }
	  if(fTowerSize<fPadCompDummy[i]->SizeY())
	    {
	      fTowerSize = fPadCompDummy[i]->SizeY();
	    }
	}
      else
	{
	  for(int ix=0;ix<fGlobal_NX_NY_Pads;ix++)
	    {
	      for(int iy=0;iy<fGlobal_NX_NY_Pads;iy++)
		{
		  double x, y;
		  GetGeoPadCenterLocal(iy, ix, x, y);
		  fCompBase[fNPadCompositionBase] = new AliFOCALComposition();
		  fCompBase[fNPadCompositionBase]->SetCompositionParameters("SiPad",
									    fPadCompDummy[i]->Layer(),
									    fPadCompDummy[i]->Stack(),
									    ix+iy*fGlobal_NX_NY_Pads, 
									    x,y,center_z, 
									    fGlobal_Pad_Size, 
									    fGlobal_Pad_Size, 
									    fPadCompDummy[i]->SizeZ());
		  fPadCompositionBase->AddLast(fCompBase[fNPadCompositionBase]);
		  fCompBase[fNPadCompositionBase]->Clear();
		  fNPadCompositionBase++;
					
		  if(fTowerSize<fGlobal_Pad_Size)
		    {
		      fTowerSize = fGlobal_Pad_Size;
		    }
		}
	    }      
	}
      center_z += fPadCompDummy[i]->GetThickness();
    }
	
  fPadLayerThickness = center_z;

  cout<<" ********************************** "<<endl;

  center_z=0;
  for(int i=0;i<NCPixel;i++)
    {
      if(fPixelCompDummy[i]->Material()!="SiPix")
	{	  
	  fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
	  fCompBasePixel[fNPixelCompositionBase] -> SetCompositionParameters(fPixelCompDummy[i]->Material(), 
									     fPixelLayerLocations[0],
									     fPixelCompDummy[i]->Stack(),
									     fPixelCompDummy[i]->Id(),
									     fPixelCompDummy[i]->CenterX(),
									     fPixelCompDummy[i]->CenterY(),
									     center_z,
									     fPixelCompDummy[i]->SizeX(),
									     fPixelCompDummy[i]->SizeY(),
									     fPixelCompDummy[i]->SizeZ());
	  fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
	  fCompBasePixel[fNPixelCompositionBase]->Clear();
	  fNPixelCompositionBase++;
	}
      else {
	for(int ix=0;ix<fGlobal_NX_NY_Pads;ix++)
	  {
	    for(int iy=0;iy<fGlobal_NX_NY_Pads;iy++)
	      {
		double x, y;
		GetGeoPadCenterLocal(iy, ix, x, y);
		fCompBasePixel[fNPixelCompositionBase] = new AliFOCALComposition();
		fCompBasePixel[fNPixelCompositionBase]->SetCompositionParameters("SiPix",
										 fPixelCompDummy[i]->Layer(),
										 fPixelCompDummy[i]->Stack(),
										 ix+iy*fGlobal_NX_NY_Pads, 
										 x,y,center_z, 
										 fGlobal_Pad_Size, 
										 fGlobal_Pad_Size, 
										 fPixelCompDummy[i]->SizeZ());
		fPixelCompositionBase->AddLast(fCompBasePixel[fNPixelCompositionBase]);
		fCompBasePixel[fNPixelCompositionBase]->Clear();
		fNPixelCompositionBase++;
	      }
	  }
      }
      center_z += fPixelCompDummy[i]->GetThickness();
    }
  fPixelLayerThickness = center_z;


  // Add HCal Layers
  // MvL: not sure why this copying step is needed; it's used in pads and pixels to generate 2x2 pads per tower...
  center_z=0;
  for(int i=0;i<NCHCal;i++)
    {
      AliFOCALComposition *comp = new AliFOCALComposition();
      comp -> SetCompositionParameters(fHCalCompDummy[i]->Material(), 
				       fHCalCompDummy[i]->Layer(),
				       fHCalCompDummy[i]->Stack(),
				       fHCalCompDummy[i]->Id(),
				       fHCalCompDummy[i]->CenterX(),
				       fHCalCompDummy[i]->CenterY(),
                       fNHCalLayers==1 ? 0. : center_z, //if we decided to use the spagetti HCAL it will be only one layer with two compositions
				       fHCalCompDummy[i]->SizeX(),
				       fHCalCompDummy[i]->SizeY(),
				       fHCalCompDummy[i]->SizeZ());
      fHCalCompositionBase->AddLast(comp);
      comp->Clear();  // This is also done in the other segments; not sure why
      if(fNHCalLayers==1){
        center_z = fHCalCompDummy[i]->GetThickness();
      }else{
        center_z += fHCalCompDummy[i]->GetThickness();
      }
      fNHCalCompositionBase++;
    }
  fHCalLayerThickness = center_z;
  center_z = 0;

  //  for(int i=0;i<NCFrontMatter;i++){
  //    fCompBaseFrontMatter[fNFrontMatterCompositionBase] = new AliFOCALComposition();
  //    fCompBaseFrontMatter[fNFrontMatterCompositionBase] -> SetCompositionParameters(fFrontMatterCompDummy[i]->Material(), 
  //										   -1,
  //										   fFrontMatterCompDummy[i]->Stack(),
  //										   fFrontMatterCompDummy[i]->Id(),
  //										   fFrontMatterCompDummy[i]->CenterX(),
  //										   fFrontMatterCompDummy[i]->CenterY(),
  //										   center_z,
  //										   fFrontMatterCompDummy[i]->SizeX(),
  //										   fFrontMatterCompDummy[i]->SizeY(),
  //										   fFrontMatterCompDummy[i]->SizeZ());
  //    fFrontMatterCompositionBase->AddLast(fCompBaseFrontMatter[fNFrontMatterCompositionBase]);
  //    fCompBaseFrontMatter[fNFrontMatterCompositionBase]->Clear();
  //    center_z += fFrontMatterCompDummy[i]->GetThickness();
  //    fNFrontMatterCompositionBase++;
  //  }
	
  fFrontMatterLayerThickness = center_z;
  SetUpTowerWaferSize();
  cout<<" end of SetParameters "<<endl;
}


//_________________________________________________________________________
TObjArray *AliFOCALGeometry::GetFOCALMicroModule(int layer){

  TObjArray *fLayerComposition = new TObjArray(1000);


  if(layer==-1){
    *fLayerComposition = *fGeometryComposition;
  }else{
    AliFOCALComposition *fComp1 = new AliFOCALComposition();
    for(int i=0;i<fGeometryComposition->GetEntriesFast(); i++){
      fComp1 = (AliFOCALComposition*)fGeometryComposition->UncheckedAt(i);
      if(fComp1->Layer()==layer){
	fLayerComposition->AddLast(fComp1);
      }
    }
  }
  return fLayerComposition;

}

//_________________________________________________________________________
AliFOCALComposition *AliFOCALGeometry::GetFOCALComposition(int layer, int stack){
  

  TIter iter(fGeometryComposition);
  //AliFOCALComposition *fComp1 = new AliFOCALComposition();
  //fCompositionRet = new AliFOCALComposition();
  while( (fCompositionRet = dynamic_cast<AliFOCALComposition*>(iter.Next())) ){
    if(fCompositionRet->Layer()==layer && fCompositionRet->Stack()==stack){
      break;
    }
  }
  AliFOCALComposition *ftmp = (AliFOCALComposition*)fCompositionRet->Clone();
  //delete iter;
  //fCompositionRet->Clear();
  return ftmp;


}

//_________________________________________________________________________
/// this gives global position of the center of Tower
Bool_t AliFOCALGeometry::GetGeoTowerCenter(Int_t Tower, Double_t &x, Double_t &y, Double_t &z, Int_t segment)
{

  int id = Tower;
  int ix = id%GetNumberOfTowersInX();
  int iy = id/GetNumberOfTowersInX();

  
  float dw = GetTowerSize()+GetTowerGapSize();
  
  x = ix*dw+0.5*dw-0.5*GetFOCALSizeX();
  y = iy*dw+0.5*dw-0.5*GetFOCALSizeY();

  if(!fUseSandwichHCAL && GetVirtualIsHCal(segment))
  {
    Int_t nCols, nRows;
    GetVirtualNColRow(segment, nCols, nRows);
    ix = id%nCols;
    iy = id/nRows;

    if(fUseSandwichHCAL)
    {
      float padSize = fVirtualSegmentComposition[segment]->fPadSize;
      x = ix * padSize + 0.5 * padSize -0.5*GetFOCALSizeX();
      y = iy * padSize + 0.5 * padSize -0.5*GetFOCALSizeY();
    }
    else
    {
      Float_t RowDistance = fVirtualSegmentComposition[segment]->fPadSize * TMath::Sin(TMath::Pi() / 3);
      Float_t Radius = fVirtualSegmentComposition[segment]->fPadSize / 2;
      Float_t Diameter = fVirtualSegmentComposition[segment]->fPadSize;

      Bool_t rowIsOdd = iy % 2 == 1;

      if (rowIsOdd)
        x = ix * Diameter + (Radius - 0.5*GetFOCALSizeX()) + Radius;
      else// No: Calculate normally
        x = ix * Diameter + Radius - 0.5*GetFOCALSizeX();

      y = iy * RowDistance + Radius - 0.5*GetFOCALSizeY();
    }
  }

  z = GetFOCALZ0();
  
  //// remove beam pipe area
  //  define beam pipe radius, calculate half of the tower diagonal in XY 
  //  and remove every tower which center is closer than the sum of the two...
  Double_t BeamPipeRadius = 3.6; // in cm
  Double_t TowerHalfDiag = TMath::Sqrt2()*0.5*GetTowerSize(); // tower half diagonal
  Double_t MinRadius = BeamPipeRadius+TowerHalfDiag;
  //
  if((x*x+y*y) < (MinRadius*MinRadius)){  // comparing the tower center position with the minimum distance in second powers.
    fDisableTowers.push_back(Tower+1);
    return false;
  }

  return true;

}

//_________________________________________________________________________
void AliFOCALGeometry::GetGeoCompositionCenter(Int_t Tower, Int_t Layer, Int_t Stack,
					       Double_t &x, Double_t &y, Double_t &z)
{
  Int_t segment = 0;
  GetVirtualSegmentFromLayer(Layer, segment);

  GetGeoTowerCenter(Tower, x, y, z, segment);

  AliFOCALComposition *fComp1 = (AliFOCALComposition*)GetFOCALComposition(Layer, Stack);
  if(fComp1==0x0 ){
    z = z + fLocalLayerZ[Layer]-GetFOCALSizeZ()/2;
  }else{
    z = fComp1->CenterZ()-GetFOCALSizeZ()/2+GetFOCALZ0();
  }

  //z = z + fLocalLayerZ[Layer]-GetFOCALSizeZ()/2;

//  fComp1->Clear();
//  fComp1->Delete();
//  fComp1 = 0;
  delete fComp1;


  //  fComp1 = 0;
  
}
//_________________________________________________________________________
/// this gives global position of the pad
void AliFOCALGeometry::GetGeoPadCenter(Int_t Tower, Int_t Layer, Int_t Stack, 
				       Int_t Row, Int_t Col,
				       Double_t &x, Double_t &y, Double_t &z)
{
  
  Double_t x1, y1, z1;
  GetGeoCompositionCenter(Tower, Layer, Stack, x1, y1, z1);


  Double_t x2, y2;
  GetGeoPadCenterLocal(Row, Col, x2, y2);

  z = z1;
  x = x1 + x2;
  y = y1 + y2;

} 

//_________________________________________________________________________
/// this gives local position of the pad with respect to the wafer
void AliFOCALGeometry::GetGeoPadCenterLocal(Int_t Row, Int_t Col, Double_t &x, Double_t &y)
{


  /// startting to count from upper-left
  /*
    (0,0)    
     ___________________
    |  __   __
    | |__| |__|
    |  __   __
    | |__| |__|
    |  __   __
    | |__| |__|
    |
  */

  float wafer_size = GetTowerSize();
  
  x = fGlobal_SKIN+Col*(fGlobal_Pad_Size+fGlobal_PPTOL)+0.5*fGlobal_Pad_Size;
  y = -fGlobal_SKIN-Row*(fGlobal_Pad_Size+fGlobal_PPTOL)-0.5*fGlobal_Pad_Size;
  
  x = x - 0.5*wafer_size;
  y = y + 0.5*wafer_size;
}



//_________________________________________________________________________
Double_t AliFOCALGeometry::GetTowerSize(void)
{

  return fTowerSize; 
  //  return fGlobal_NX_NY_Pads*(fGlobal_Pad_Size+fGlobal_PPTOL)-fGlobal_PPTOL+2*fGlobal_SKIN;
}

//_________________________________________________________________________
Double_t AliFOCALGeometry::GetFOCALSizeX(void)
{
  //  return fGlobal_NX_Tower*(GetTowerSize()+fGlobal_TOWER_TOL)-fGlobal_TOWER_TOL;
  return fGlobal_NX_Tower*(GetTowerSize()+fGlobal_TOWER_TOL);
}

//_________________________________________________________________________
Double_t AliFOCALGeometry::GetFOCALSizeY(void)
{
  //  return fGlobal_NY_Tower*(GetTowerSize()+fGlobal_TOWER_TOL)-fGlobal_TOWER_TOL;
  return fGlobal_NY_Tower*(GetTowerSize()+fGlobal_TOWER_TOL);
}

//_________________________________________________________________________
Double_t AliFOCALGeometry::GetFOCALSizeZ(void){

  Double_t ret = 0;

  for(int i=0;i<fNPadLayers+fNPixelLayers+fNHCalLayers;i++){
    ret += fLayerThickness[i];
  }
  ret = ret + fFrontMatterLayerThickness;
  return ret;
}
//_________________________________________________________________________
Double_t AliFOCALGeometry::GetECALSizeZ(){

  Double_t ret = 0;

  for(int i=0;i<fNPadLayers+fNPixelLayers;i++){
    ret += fLayerThickness[i];
  }
  ret = ret + fFrontMatterLayerThickness;
  return ret;
}
//_________________________________________________________________________
Double_t AliFOCALGeometry::GetECALCenterZ(){

  // Determines the ECAL z center of mass with respect to the FOCAL
  Double_t centerZ = fFrontMatterLayerThickness + fLocalLayerZ[0] + GetECALSizeZ()/2;

  return centerZ;
}

//_________________________________________________________________________
Double_t AliFOCALGeometry::GetHCALSizeZ(){

  Double_t ret = 0;

  for(int i=fNPadLayers+fNPixelLayers;i<fNPadLayers+fNPixelLayers+fNHCalLayers;i++){
    ret += fLayerThickness[i];
  }
  return ret;
}

//_________________________________________________________________________
Double_t AliFOCALGeometry::GetHCALCenterZ(){

  Double_t centerZ = fFrontMatterLayerThickness + fLocalLayerZ[fNPadLayers+fNPixelLayers] + GetHCALSizeZ()/2;

  return centerZ;
}


//_________________________________________________________________________
//. this returns following quantities for the pad position location
/// layer depth 
//  pad row and col in the wafer 
//  wafer id in the brick, where the pad belongs to  
void AliFOCALGeometry::GetPadPositionId2RowColStackLayer(Int_t id, 
							 Int_t &row, Int_t &col, 
							 Int_t &stack, Int_t &lay, 
							 Int_t &seg){

  //cout<<"AliFOCALGeometry::GetPadPositionId2RowColWaferLayer "<<id<<" "<<wafer<<" "<<GetNumberOfPads()<<" "<<fNumberOfLayersInSegemnets<< endl;
  ////  id contains loction of pads in the tower, pad stack, pad layer
  /////  (fComp->Id()) + (fComp->Stack() << 12) + (fComp->Layer() << 13) +1 ;
  ///// 
  Int_t number = id -1; 
  Int_t padid = (number & 0xfff);
  stack = (number>>12)&0x000f;
  lay = (number>>16)&0x00ff;
  
  seg = fSegments[lay];

  col = padid%fGlobal_NX_NY_Pads;
  row = padid/fGlobal_NX_NY_Pads;

  if(GetVirtualIsHCal(seg))
  {
    Int_t nCols, nRows;
    GetVirtualNColRow(seg, nCols, nRows);
    col = id%nCols;
    row = id/nRows;
  }
}

//_________________________________________________________________________ 
//// this gives longitudinal position of the segment 
Double_t AliFOCALGeometry::GetFOCALSegmentZ(Int_t seg){
  Double_t ret=0;

  if(seg<0 || seg>fNumberOfSegments){
    ret = GetFOCALZ0();
  }else{
    for(int i=0;i<seg;i++){
      ret += fLocalSegmentsZ[i];
    }
  }
  
  ret = ret + fLocalSegmentsZ[seg]/2 + GetFOCALZ0() - GetFOCALSizeZ()/2;
  
  return ret;
}

//_________________________________________________________________________ 
/// this function defines 
/// layer is strip or pad?
/// which segment this layer delongs to?
void AliFOCALGeometry::SetUpLayerSegmentMap(void){
  ///// define the longitudinal elements 
  ////  fSegements = -1 -> strip layer
  ////  fSegements = 0  --> pad 0th segement
  ////  fSegements = 1  --> pad 1th segement
  ////  fSegements = 2  --> pad 2th segement

  for(int i=0;i<fNPixelLayers;i++){
    fComposition[fPixelLayerLocations[i]]=-1;
  }
  
  int low=0;
  int start=0;
  int high=0;
  for(int i=0;i<fNumberOfSegments;i++){
    high += fNumberOfLayersInSegments[i] ;
    for(int j=start;j<fNPixelLayers+fNPadLayers+fNHCalLayers;j++){
      if(fComposition[j]==-1) {
	fSegments[j]=i;
	start++;
      }else{
	fSegments[j]=i;
	low ++;
	start++;
      }
      if(low>=high){
	break;
      }
    }
  }
  /*
  for(int i=0;i<fNLayers+fNPixelLayers;i++){
    cout<<i<<" "<<fComposition[i]<<" "<<fSegments[i]<<endl;
  }
  */
}

//_________________________________________________________________________ 
/// this is the pixel number to be stored in the Hits.root file
/// this is used for the study with fine pixel readout
/// the pad is divided into the pixels with the size of fGlobal_ Pixel_Size 
Int_t AliFOCALGeometry::GetPixelNumber(Int_t vol0, Int_t vol1, Int_t /*vol2*/ , Double_t x, Double_t y, Double_t /* z */ ){

  Int_t ret = 0;

  if(fGlobal_Pixel_Readout == kFALSE){
    ret = -1;
    return ret; 
  }

  int id = vol0;
  int tower = vol1;
  //int brick = vol2;  /// meaning 0 in the current design
  
  int row, col, stack, layer, segment;
  GetPadPositionId2RowColStackLayer(id, row, col, stack, layer, segment);
  Double_t x0, y0, z0;
  GetGeoPadCenter(tower, layer, stack, row, col, x0, y0, z0);
  
//  cout << "PadLocation1: " << x0 << ", " << y0 << endl;
  

  double x_loc = x-x0;
  double y_loc = y-y0;



  double pixel_nbr_x = ((x_loc+0.5*fGlobal_Pad_Size)/(fGlobal_Pixel_Size));
  double pixel_nbr_y = ((y_loc+0.5*fGlobal_Pad_Size)/(fGlobal_Pixel_Size));
  
  int pixel_number_x;
  pixel_number_x = static_cast<int>(pixel_nbr_x);
//  if(pixel_number_x-pixel_nbr_x>0.5){
//    pixel_number_x = pixel_number_x+1;
//  }

  int pixel_number_y;
  pixel_number_y = static_cast<int>(pixel_nbr_y);
//  if(pixel_number_y-pixel_nbr_y>0.5){
//    pixel_number_y = pixel_number_y+1;
//  }

  ret = (pixel_number_x <<8) | pixel_number_y;
  //cout<<x<<" "<<y<<" "<<x0<<" "<<y0<<" "<<x_loc<<" "<<y_loc<<" "<<pixel_number_x<<" "<<pixel_number_y<<" "<<ret<<endl;
  return ret;
}

//_________________________________________________________________________ 
void AliFOCALGeometry::SetUpTowerWaferSize(){

  fWaferSize=fGlobal_NX_NY_Pads*(fGlobal_Pad_Size+fGlobal_PPTOL)-fGlobal_PPTOL+2*fGlobal_SKIN;
  
  if(fTowerSize<fWaferSize){
    fTowerSize = fWaferSize;
  }
  cout<<" tower size is set to : "<<fTowerSize<<" : wafer size = "<<fWaferSize<<endl;

}

//_________________________________________________________________________ 
Bool_t AliFOCALGeometry::DisabledTower(int Tower){

  list<int>::iterator it =  fDisableTowers.begin(); 
  while(it != fDisableTowers.end()){
    int ch = *it;
    if(ch==Tower){
      return true;
    }
    ++it;
  }
  return false;
}
    

//_________________________________________________________________________
/// this gives global position of the pixel
void AliFOCALGeometry::GetGeoPixelCenter(Int_t pixel, Int_t Tower, Int_t Layer, Int_t Stack, 
					 Int_t Row, Int_t Col,
					 Double_t &x, Double_t &y, Double_t &z)
{
  double x0, y0, z0;
  GetGeoPadCenter(Tower, Layer, Stack, Row, Col, x0, y0, z0);
  
  int pixel_y = pixel & 0xff;
  int pixel_x = (pixel>>8) & 0xff;
  
  double x1, y1;
  x1 = pixel_x*fGlobal_Pixel_Size+0.5*fGlobal_Pixel_Size-0.5*fGlobal_Pad_Size;
  y1 = pixel_y*fGlobal_Pixel_Size+0.5*fGlobal_Pixel_Size-0.5*fGlobal_Pad_Size;

  x = x1+x0;
  y = y1+y0;
  z = z0;
} 

//_________________________________________________________________________
//// this returns pad or strip 
//// if pad, this function returns true
//// if strip, this function returns false
Bool_t AliFOCALGeometry::isPadOrPixel(Int_t id){
  
  Bool_t b_pad;

  int offset = (1<<24);

  if(id<offset){
    b_pad = true;
  }else{
    b_pad = false;
  }
  
  return b_pad;
}

bool AliFOCALGeometry::GetVirtualInfo(Float_t x, Float_t y, Float_t z, Int_t & col, 
        Int_t & row, Int_t & layer, Int_t & segment) {
  //
  // Calculate col, row, layer, (virtual) segment from x,y,z
  // returns false if outside volume
  //

  if (!GetVirtualLayerSegment(z, layer, segment))
    return false;
 
  if (segment == -1) {
    //cout << "bad segment" << endl;
    return false;
  }
    
  if (TMath::Abs(x) > GetFOCALSizeX()/2)
    return false;
  if (TMath::Abs(y) > GetFOCALSizeY()/2)
    return false;
 
  col = (Int_t)((x + GetFOCALSizeX()/2)/fVirtualSegmentComposition[segment]->fPadSize);  
  row = (Int_t)((y + GetFOCALSizeY()/2)/fVirtualSegmentComposition[segment]->fPadSize);

  if(!fUseSandwichHCAL && GetVirtualIsHCal(segment)){

    // Find the row and column of the box that the point falls in.
    Float_t RowDistance = fVirtualSegmentComposition[segment]->fPadSize * TMath::Sin(TMath::Pi() / 3);
    Float_t Radius = fVirtualSegmentComposition[segment]->fPadSize / 2;
    Float_t Diameter = fVirtualSegmentComposition[segment]->fPadSize;

    row = (Int_t)((y + GetFOCALSizeY()/2 - Radius)/RowDistance);

    Bool_t rowIsOdd = row % 2 == 1;

    // Is the row an odd number?
    if (rowIsOdd)// Yes: Offset x to match the indent of the row
        col = (Int_t) (((x + GetFOCALSizeX()/2 - Radius) - Radius) / Diameter);
    else// No: Calculate normally
        col = (Int_t)((x + GetFOCALSizeX()/2 - Radius)/Diameter);  

    // Work out the position of the point relative to the box it is in
    double relY = y - (row * RowDistance);
    double relX;

    if (rowIsOdd)
        relX = (x - (col * Diameter)) - Radius;
    else
        relX = x - (col * Diameter);

    // Work out if the point is above either of the hexagon's top edges
    if( (TMath::Power(relX - Radius,2) + TMath::Power(relY - Radius,2)) > TMath::Power(Radius,2) )
    {

      if(relX < Radius)
      {
        row--;
        if (!rowIsOdd)
            col--;
      }
      else
      {
        row--;
        if (rowIsOdd)
          col++;
      }
    }
  }

  return true;
}

//_______________________________________________________________________
bool AliFOCALGeometry::GetXYZFromColRowSeg(Int_t col, Int_t row, Int_t segment, Float_t & x, Float_t & y, Float_t & z) {
  
  if (segment > fVirtualNSegments)
    return false;
  
  if(!fUseSandwichHCAL && GetVirtualIsHCal(segment))
  {
    Float_t RowDistance = fVirtualSegmentComposition[segment]->fPadSize * TMath::Sin(TMath::Pi() / 3);
    Float_t Radius = fVirtualSegmentComposition[segment]->fPadSize / 2;
    Float_t Diameter = fVirtualSegmentComposition[segment]->fPadSize;

    Bool_t rowIsOdd = row % 2 == 1;

    if (rowIsOdd)
      x = (-1*GetFOCALSizeX()/2 + Radius) + Radius + ((Float_t)col+0.5)*Diameter;
    else// No: Calculate normally
      x = -1*GetFOCALSizeX()/2 + Radius + ((Float_t)col+0.5)*Diameter;

    if ( ( x < -GetFOCALSizeX()/2 ) || ( x > GetFOCALSizeX()/2 ) )
      return false;

    y = -1*GetFOCALSizeY()/2 + Radius + ((Float_t)row+0.5)*RowDistance;
    
    if ( ( y < -GetFOCALSizeY()/2 ) || ( y > GetFOCALSizeY()/2 ) )
      return false;
  }
  else
  {
    x = -1*GetFOCALSizeX()/2 + ((Float_t)col+0.5)*fVirtualSegmentComposition[segment]->fPadSize;
    
    if ( ( x < -GetFOCALSizeX()/2 ) || ( x > GetFOCALSizeX()/2 ) )
      return false;
      
    y = -1*GetFOCALSizeY()/2 + ((Float_t)row+0.5)*fVirtualSegmentComposition[segment]->fPadSize;
    
    if ( ( y < -GetFOCALSizeY()/2 ) || ( y > GetFOCALSizeY()/2 ) )
      return false;
  }
  
  z = GetVirtualSegmentZ(segment);
  
  return true;
}

//_______________________________________________________________________
bool AliFOCALGeometry::GetVirtualNColRow(Int_t segment, Int_t & nCol, Int_t & nRow) {

  if (!fVirtualSegmentComposition)
    return false;

  if ( (segment < 0) || ( segment >= fVirtualNSegments) ) 
    return false;
  
  nCol = (Int_t)(GetFOCALSizeX()/fVirtualSegmentComposition[segment]->fPadSize + 0.001);
  nRow = (Int_t)(GetFOCALSizeY()/fVirtualSegmentComposition[segment]->fPadSize + 0.001);

  if(!fUseSandwichHCAL && GetVirtualIsHCal(segment))
  {
    Float_t RowDistance = fVirtualSegmentComposition[segment]->fPadSize * TMath::Sin(TMath::Pi() / 3);
    nRow = (Int_t)(GetFOCALSizeY()/RowDistance + 0.001);
  }

  return true;
}

//_______________________________________________________________________
Int_t AliFOCALGeometry::GetVirtualNSegments() {

  return fVirtualNSegments;
}

//_______________________________________________________________________
Bool_t AliFOCALGeometry::GetVirtualLayerSegment(Float_t z, Int_t &layer, Int_t &segment) {
  
  layer = -1;
  segment = -1;

  z = z - GetFOCALZ0() + GetFOCALSizeZ()/2;  // z from front face (excluding fron matter)
  Float_t EMLayersZ = fNPadLayers * fPadLayerThickness + fNPixelLayers * fPixelLayerThickness;  // Pixel layers replace pad layers
  if (z < EMLayersZ) {
    layer = fNPadLayers + fNPixelLayers - 1;
    while (layer >=0 && z < fLocalLayerZ[layer])
      layer--;
    //cout << "EM layer, z : " << z << " layer " << layer << " localLayZ " << fLocalLayerZ[layer] << endl;
  }
  else {
    z = z - EMLayersZ;
    layer = Int_t(z / fHCalLayerThickness) + fNPadLayers;
  }
  
  if ( (layer < 0) || ( layer >= (fNPadLayers + fNPixelLayers + fNHCalLayers)) ) {
    return false;
  }
  
  //cout << "layer: " << layer << endl;
  
  segment = -1;
  for (Int_t nSeg = 0 ; nSeg < fVirtualNSegments ; nSeg++) {
    //cout << "Segment boundaries " << nSeg << " : " << fVirtualSegmentComposition[nSeg]->fMinLayer << " " << fVirtualSegmentComposition[nSeg]->fMaxLayer << endl;
    if ( (layer >= fVirtualSegmentComposition[nSeg]->fMinLayer) && (layer <= fVirtualSegmentComposition[nSeg]->fMaxLayer) ) {
      segment = nSeg;
      break;
    } 
  }

  if (segment == fVirtualNSegments)
    return false;
 
  return true;
}

//_______________________________________________________________________
Bool_t AliFOCALGeometry::GetVirtualSegmentFromLayer(Int_t layer, Int_t &segment) {

  segment = -1;
  for (Int_t nSeg = 0 ; nSeg < fVirtualNSegments ; nSeg++) {
    //cout << "Segment boundaries " << nSeg << " : " << fVirtualSegmentComposition[nSeg]->fMinLayer << " " << fVirtualSegmentComposition[nSeg]->fMaxLayer << endl;
    if ( (layer >= fVirtualSegmentComposition[nSeg]->fMinLayer) && (layer <= fVirtualSegmentComposition[nSeg]->fMaxLayer) ) {
      segment = nSeg;
      break;
    } 
  }

  if (segment == fVirtualNSegments)
    return false;
 
  return true;
}

//_______________________________________________________________________
Int_t AliFOCALGeometry::GetVirtualSegment(Float_t z) {
  Int_t layer, segment;
  GetVirtualLayerSegment(z, layer, segment);
  return segment;
}
//_______________________________________________________________________
Float_t AliFOCALGeometry::AliFOCALGeometry::GetVirtualPadSize(Int_t segment) {
  if (!fVirtualSegmentComposition)
    return -1;
    
  return fVirtualSegmentComposition[segment]->fPadSize;
}

//_______________________________________________________________________
Float_t AliFOCALGeometry::GetVirtualRelativeSensitiveThickness(Int_t segment) {
  
  if (!fVirtualSegmentComposition)
    return -1;
    
  return fVirtualSegmentComposition[segment]->fRelativeSensitiveThickness;
}

//_______________________________________________________________________
Float_t AliFOCALGeometry::GetVirtualPixelTreshold(Int_t segment) {
  
  if (!fVirtualSegmentComposition)
    return -1;
    
  return fVirtualSegmentComposition[segment]->fPixelTreshold;
}

//________________________________________________________________________
Float_t AliFOCALGeometry::GetVirtualSegmentSizeZ(Int_t segment){
  
  if (!fVirtualSegmentComposition)
    return -1;
    
  Float_t size = 0;
  
  for (Int_t nLay = fVirtualSegmentComposition[segment]->fMinLayer; 
       nLay <= fVirtualSegmentComposition[segment]->fMaxLayer; nLay++) 
    size += fLayerThickness[nLay];
  
  return size;
}

//________________________________________________________________________
Float_t AliFOCALGeometry::GetVirtualSegmentZ(Int_t segment){
  
  if (!fVirtualSegmentComposition)
    return -1;
    
  Float_t before = 0;
  Float_t thickness  = 0;
  
  for (Int_t nLay = 0; 
       nLay < fVirtualSegmentComposition[segment]->fMinLayer; nLay++) 
    before += fLayerThickness[nLay];
    
  for (Int_t nLay = fVirtualSegmentComposition[segment]->fMinLayer; 
       nLay <= fVirtualSegmentComposition[segment]->fMaxLayer; nLay++) 
    thickness += fLayerThickness[nLay];
  
  return GetFOCALZ0() - GetFOCALSizeZ()/2 + before + thickness/2;
}

//________________________________________________________________________
bool AliFOCALGeometry::GetVirtualIsPixel(Int_t segment){
  
  if (!fVirtualSegmentComposition)
    return false;
  
  if ((segment < 0) || (segment >= fVirtualNSegments ))
    return false; 
    
  return (fVirtualSegmentComposition[segment]->fIsPixel == 1);
}

//________________________________________________________________________
bool AliFOCALGeometry::GetVirtualIsHCal(Int_t segment){
  
  if (!fVirtualSegmentComposition)
    return false;
  
  if ((segment < 0) || (segment >= fVirtualNSegments ))
    return false; 
    
  return (fVirtualSegmentComposition[segment]->fIsPixel == 2);
}

