#ifndef ALIFOCALPEDESTAL_H
#define ALIFOCALPEDESTAL_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */


class TNamed;
class AliCDBEntry;
class AliFOCAL;

class AliFOCALPedestal: public TNamed
{
 public:
  AliFOCALPedestal();
  AliFOCALPedestal(const char* name);
  AliFOCALPedestal(const AliFOCALPedestal &pedestal);
  AliFOCALPedestal& operator= (const AliFOCALPedestal &pedestal);
  virtual ~AliFOCALPedestal();
  void  Reset();
  void  SetPedMeanRms(Int_t sec, Int_t seg, Int_t row, Int_t col, Int_t cell, 
		      Float_t pedmean, Float_t pedrms);
  Int_t GetPedMeanRms(Int_t sec, Int_t seg, Int_t row, Int_t col, Int_t cell) const;
  
 protected:

  enum
      {
	kSec    = 12,
	kSeg    = 3,  // Number of modules per plane
	kRow    = 7,  // Row
	kCol    = 2,   // Column
	kCell    = 16   // Column
      };

  Int_t fPedMeanRms[kSec][kSeg][kRow][kCol][kCell];

  ClassDef(AliFOCALPedestal,1) // Pedestal class
};
#endif
