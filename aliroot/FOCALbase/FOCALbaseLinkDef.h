#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  AliFOCALCalibData+;
#pragma link C++ class  AliFOCALCell+;
#pragma link C++ class  AliFOCALCellMap+;
#pragma link C++ class  AliFOCALCluster+;
#pragma link C++ class  AliFOCALClusterizer+;
//#pragma link C++ class  AliFOCALClusterizerv0+;
#pragma link C++ class  AliFOCALClusterizerv1+;
#pragma link C++ class  AliFOCALClusterizerv2+;
#pragma link C++ class  AliFOCALClusterizerv3+;
#pragma link C++ class  AliFOCALComposition+;
#pragma link C++ class  AliFOCAL+;
#pragma link C++ class  AliFOCALdigit+;
#pragma link C++ class  AliFOCALDigitizer+;
#pragma link C++ class  AliFOCALSegmentMap+;
#pragma link C++ class  AliFOCALGeometry+;
#pragma link C++ class  AliFOCALKinematics+;
#pragma link C++ class  AliFOCALVirtSegment+;
#pragma link C++ class  AliFOCALhit+;
#pragma link C++ class  AliFOCALLoader+;
#pragma link C++ class  AliFOCALPad+;
#pragma link C++ class  AliFOCALRinger+;
#pragma link C++ class  AliFOCALPedestal+;
#pragma link C++ class  AliFOCALsdigit+;
#pragma link C++ class  ObjectMap+;

#endif
