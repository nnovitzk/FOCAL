/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//  Date   : August 05 2003                            //
//  used to store the info into TreeS                  //
//                                                     //
//-----------------------------------------------------//
#include "Riostream.h"
#include "Rtypes.h"
#include "AliFOCALsdigit.h"
#include <stdio.h>

ClassImp(AliFOCALsdigit)

AliFOCALsdigit::AliFOCALsdigit():
  fSec(0),
  fSeg(0),
  fLay(0),
  fRow(0),
  fColumn(0),
  fEdep(0.),
  fTime(0.)

{
  // Default Constructor
}

AliFOCALsdigit::AliFOCALsdigit(Int_t sec, Int_t seg, Int_t lay, 
			       Int_t row, Int_t col, 
			       Float_t edep, Float_t time):
  fSec(sec),
  fSeg(seg),
  fLay(lay),
  fRow(row),
  fColumn(col),
  fEdep(edep),
  fTime(time)
{
  // Constructor
}

AliFOCALsdigit::AliFOCALsdigit(AliFOCALsdigit *focalsdigit):
  fSec(0),
  fSeg(0),
  fLay(0),
  fRow(0),
  fColumn(0),
  fEdep(0.),
  fTime(0.)
{
  *this = *focalsdigit;
}

AliFOCALsdigit::AliFOCALsdigit(const AliFOCALsdigit& focalsdigit):
  TObject(focalsdigit),
  fSec(focalsdigit.fSec),
  fSeg(focalsdigit.fSeg),
  fLay(focalsdigit.fLay),
  fRow(focalsdigit.fRow),
  fColumn(focalsdigit.fColumn),
  fEdep(focalsdigit.fEdep),
  fTime(focalsdigit.fTime)
{
  //Copy Constructor 
}
AliFOCALsdigit & AliFOCALsdigit::operator=(const AliFOCALsdigit& focalsdigit)
{
  //Assignment operator 
  if(this != &focalsdigit)
    {
      fSec   = focalsdigit.fSec;
      fSeg   = focalsdigit.fSeg;
      fRow        = focalsdigit.fRow;
      fLay        = focalsdigit.fLay;
      fColumn     = focalsdigit.fColumn;
      fEdep       = focalsdigit.fEdep;
      fTime       = focalsdigit.fTime;
    }
  return *this;
}


AliFOCALsdigit::~AliFOCALsdigit()
{
  // Default Destructor
}
Int_t AliFOCALsdigit::GetSector() const
{
  return fSec;
}
Int_t AliFOCALsdigit::GetSegment() const
{
  return fSeg;
}
Int_t AliFOCALsdigit::GetLayer() const
{
  return fLay;
}

Int_t AliFOCALsdigit::GetRow() const
{
  return fRow;
}
Int_t AliFOCALsdigit::GetColumn() const
{
  return fColumn;
}
Float_t AliFOCALsdigit::GetCellEdep() const
{
  return fEdep;
}
Float_t AliFOCALsdigit::GetCellTime() const
{
  return fTime;
}

