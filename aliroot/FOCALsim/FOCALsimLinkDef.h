#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: FOCALsimLinkDef.h by Taku Gunji $ */
 
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  AliFOCALv0+;
#pragma link C++ class  AliFOCALv1+;
#pragma link C++ class  AliHALLv4+;
#pragma link C++ class  AliPIPEFOCAL+;

#endif
