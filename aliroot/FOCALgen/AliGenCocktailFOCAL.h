#ifndef ALIGENCOCKTAILFOCAL_H
#define ALIGENCOCKTAILFOCAL_H

// Derived class from AliGenCocktail with option to check for a particle in the FOCAL acceptance

#include "AliGenCocktail.h"

class AliGenCocktailFOCAL: public AliGenCocktail {

 public:
  AliGenCocktailFOCAL();
  ~AliGenCocktailFOCAL() {;}
  void Generate();
  void SetPrint(Bool_t b) {fPrint=b;}
  void SetTriggerKinematics(Float_t pt, Float_t mineta, Float_t maxeta) {fMinPt=pt; fMinEta=mineta; fMaxEta=maxeta;}
  void SetTriggerPart(Int_t id=22) {fPartId=22;}
 protected: 
  Int_t   fPartId;   // trigger on final-state particle with given pdg code (-1==no trigger, 0==any particle)
  Float_t fMinPt;    // minimum pt of trigger particle
  Float_t fMinEta;   // minimum eta of trigger particle
  Float_t fMaxEta;   // maximum eta of trigger particle
  Bool_t  fPrint;    // if 1 print trigger particle when triggered (for debugging)  
  ClassDef(AliGenCocktailFOCAL,0)
};
#endif
