#include "AliGenPythiaFOCAL.h"

#include "TParticle.h"
#include "AliLog.h"

ClassImp(AliGenPythiaFOCAL)

AliGenPythiaFOCAL::AliGenPythiaFOCAL():
  fCheckFOCAL(kFALSE),
  fFOCALMinEta(3.5),
  fFOCALMaxEta(5.8) 
{
  ;
}

AliGenPythiaFOCAL::AliGenPythiaFOCAL(Int_t npart): AliGenPythia(npart),
  fCheckFOCAL(kFALSE),
  fFOCALMinEta(3.5),
  fFOCALMaxEta(5.8) 
{
  ;
}


Bool_t AliGenPythiaFOCAL::IsInFOCAL(Float_t eta) const
{
  // Is particle in FOCAL acceptance? 

  if(eta > fFOCALMinEta  && eta < fFOCALMaxEta )
    return kTRUE;
  else
    return kFALSE;
}


Bool_t AliGenPythiaFOCAL::CheckDetectorAcceptance(Float_t phi, Float_t eta, Int_t iparticle)
{
  // check the eta/phi correspond to the detectors acceptance
  // iparticle is the index of the particle to be checked, for PHOS rotation case
  if     (fCheckPHOS   && IsInPHOS  (phi,TMath::Abs(eta),iparticle)) return kTRUE;
  else if(fCheckFOCAL  && IsInFOCAL (eta))     return kTRUE;
  else if((fCheckEMCAL && !fCheckFOCAL)  && IsInEMCAL (phi,TMath::Abs(eta))) return kTRUE;  // EMCAL is only checked if fCheckFOCAL is not set; we need to set fCheckEMCAL to get called by the base class
  else if(fCheckBarrel && IsInBarrel(    TMath::Abs(eta))) return kTRUE;
  else                                         return kFALSE;
}


Bool_t AliGenPythiaFOCAL::TriggerOnSelectedParticles(Int_t np)
{
  // Copied from AliGenPythia; added FOCAL in one place
  //
  // Select events with fragmentation photon, decay photon, pi0, eta or other hadrons going to PHOS or EMCAL or central barrel,
  // implemented primaryly for kPyJets, but extended to any kind of process.

  //printf("Check: frag photon %d, pi0 %d, eta %d, electron %d, hadron %d, decay %d, PHOS %d, EMCAL %d, Barrel %d \n",
  //       fFragPhotonInCalo,fPi0InCalo, fEtaInCalo,fEleInCalo,fHadronInCalo,fDecayPhotonInCalo,fCheckPHOS,fCheckEMCAL, fCheckBarrel); 

  Bool_t ok = kFALSE;
  for (Int_t i=0; i< np; i++) {

    TParticle* iparticle = (TParticle *) fParticles.At(i);

    Int_t pdg          = iparticle->GetPdgCode();
    Int_t status       = iparticle->GetStatusCode();
    Int_t imother      = iparticle->GetFirstMother() - 1;

    TParticle* pmother = 0x0;
    Int_t momStatus    = -1;
    Int_t momPdg       = -1;
    if(imother > 0 ){
      pmother = (TParticle *) fParticles.At(imother);
      momStatus    = pmother->GetStatusCode();
      momPdg       = pmother->GetPdgCode();
    }

    ok = kFALSE;

    //
    // Check the particle type: hadron (not pi0 or eta), electron, decay photon (from pi0 or eta or any), pi0 or eta
    //
    // Hadron
    if (fHadronInCalo && status == 1)
    {
      if(TMath::Abs(pdg) > 23 && pdg !=221 && pdg != 111) // avoid photons, electrons, muons, neutrinos and eta or pi0 
        // (in case neutral mesons were declared stable)
        ok = kTRUE;
    }
    //Electron
    else if (fEleInCalo && status == 1 && TMath::Abs(pdg) == 11)
    {
        ok = kTRUE;
    }
    //Fragmentation photon
    else if (fFragPhotonInCalo && pdg == 22 && status == 1)
    {
      if(momStatus != 11) ok = kTRUE ;  // No photon from hadron decay
    }
    // Decay photon
    else if (fDecayPhotonInCalo && !fForceNeutralMeson2PhotonDecay && pdg == 22) // pi0 status can be 1 or 11 depending on decay settings, work only for 11
    {
      if( momStatus == 11)
      {
        //if(iparticle->Pt() > fTriggerParticleMinPt) printf("Decay photon! pdg %d, status %d, pt %2.2f, mom: pdg %d, pt %2.2f\n",
        //                                                   pdg,status,iparticle->Pt(),momPdg,pmother->Pt());
        ok = kTRUE ;  // photon from hadron decay

        //In case only decays from pi0 or eta requested
        if(fPi0InCalo && momPdg!=111) ok = kFALSE;
        if(fEtaInCalo && momPdg!=221) ok = kFALSE;
      }

    }
    // Pi0 or Eta particle
    else if ((fPi0InCalo || fEtaInCalo))
    {
      if(fDecayPhotonInCalo && !fForceNeutralMeson2PhotonDecay ) continue ;

      if (fPi0InCalo && pdg == 111) // pi0 status can be 1 or 11 depending on decay settings
      {
        //if(iparticle->Pt() > fTriggerParticleMinPt) printf("Pi0! pdg %d, status %d, pt %2.2f\n",pdg,status,iparticle->Pt());
        ok = kTRUE;
      }
      else if (fEtaInCalo && pdg == 221)
      {
        //if(iparticle->Pt() > fTriggerParticleMinPt) printf("Eta! pdg %d, status %d, pt %2.2f\n",pdg,status,iparticle->Pt());
        ok = kTRUE;
      }

    }// pi0 or eta

    //
    // Check that the selected particle is in the calorimeter acceptance
    //
    if(ok && iparticle->Pt() > fTriggerParticleMinPt)
    {
      //Just check if the selected particle falls in the acceptance
      if(!fForceNeutralMeson2PhotonDecay )
      {
        //printf("\t Check acceptance! \n");
        Float_t phi = iparticle->Phi()*180./TMath::Pi(); //Convert to degrees
        Float_t eta = iparticle->Eta(); //in calos      

        if(CheckDetectorAcceptance(phi,eta,i))
        {
          ok =kTRUE;
          AliDebug(1,Form("Selected trigger pdg %d, status %d, pt %2.2f, eta %2.2f, phi %2.2f\n",pdg,status,iparticle->Pt(), eta, phi));
          //printf("\t Accept \n");
          break;
        }
        else ok = kFALSE;
      }
      //Mesons have several decay modes, select only those decaying into 2 photons
      else if(fForceNeutralMeson2PhotonDecay && (fPi0InCalo || fEtaInCalo))
      {
        // In case we want the pi0/eta trigger, 
        // check the decay mode (2 photons)

        //printf("\t Force decay 2 gamma\n");          

        Int_t ndaughters = iparticle->GetNDaughters();
        if(ndaughters != 2){
         ok=kFALSE;
          continue;
        }

        TParticle*d1 = (TParticle *) fParticles.At(iparticle->GetDaughter(0)-1);
        TParticle*d2 = (TParticle *) fParticles.At(iparticle->GetDaughter(1)-1);
        if(!d1 || !d2) {
          ok=kFALSE;
          continue;
        }

        //iparticle->Print();
        //d1->Print();
        //d2->Print();

        Int_t pdgD1 = d1->GetPdgCode();
        Int_t pdgD2 = d2->GetPdgCode();
        //printf("\t \t 2 daughters pdg = %d - %d\n",pdgD1,pdgD2);
        //printf("mother %d - %d\n",d1->GetFirstMother(),d2->GetFirstMother());

        if(pdgD1 != 22  || pdgD2 != 22){
          ok = kFALSE;
          continue;
        }

        //printf("\t accept decay\n");

        //Trigger on the meson, not on the daughter
        if(!fDecayPhotonInCalo){

          Float_t phi = iparticle->Phi()*180./TMath::Pi(); //Convert to degrees
          Float_t eta =iparticle->Eta(); //in calos 

          if(CheckDetectorAcceptance(phi,eta,i))
          {
            //printf("\t Accept meson pdg %d\n",pdg);
            ok =kTRUE;
            AliDebug(1,Form("Selected trigger pdg %d (decay), status %d, pt %2.2f, eta %2.2f, phi %2.2f\n",pdg,status,iparticle->Pt(), eta, phi));
            break;
          } else {
            ok=kFALSE;
            continue;
          }
        }
        //printf("Check daughters acceptance\n");

        //Trigger on the meson daughters
        //Photon 1
        Float_t phi = d1->Phi()*180./TMath::Pi(); //Convert to degrees
        Float_t eta = d1->Eta(); //in calos 
        if(d1->Pt() > fTriggerParticleMinPt)
        {
          //printf("\t Check acceptance photon 1! \n");
          if(CheckDetectorAcceptance(phi,eta,i))
          {
            //printf("\t Accept Photon 1\n");
            ok =kTRUE;
            AliDebug(1,Form("Selected trigger pdg %d (decay), status %d, pt %2.2f, eta %2.2f, phi %2.2f\n",pdg,status,iparticle->Pt(), eta, phi));
            break;
          }
          else ok = kFALSE;
        } // pt cut
        else  ok = kFALSE;

        //Photon 2
        phi = d2->Phi()*180./TMath::Pi(); //Convert to degrees
        eta = d2->Eta(); //in calos 

        if(d2->Pt() > fTriggerParticleMinPt)
        {
          //printf("\t Check acceptance photon 2! \n");
          if(CheckDetectorAcceptance(phi,eta,i))
          {
            //printf("\t Accept Photon 2\n");
            ok =kTRUE;
            AliDebug(1,Form("Selected trigger pdg %d (decay), status %d, pt %2.2f, eta %2.2f, phi %2.2f\n",pdg,status,iparticle->Pt(), eta, phi));
            break;
          }
          else ok = kFALSE;
        } // pt cut
        else ok = kFALSE;
      } // force 2 photon daughters in pi0/eta decays
      else ok = kFALSE;
    } else ok = kFALSE; // check acceptance
  } // primary loop
  //
  // If requested, rotate the particles event in phi to enhance/speed PHOS selection
  // A particle passing all trigger conditions except phi position in PHOS, is used as reference
  //
  if(fCheckPHOSeta)
  {
    RotatePhi(ok);
  }

  return ok;
}

