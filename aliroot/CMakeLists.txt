# **************************************************************************
# * Copyright(c) 1998-2014, ALICE Experiment at CERN, All rights reserved. *
# *                                                                        *
# * Author: The ALICE Off-line Project.                                    *
# * Contributors are mentioned in the code where appropriate.              *
# *                                                                        *
# * Permission to use, copy, modify and distribute this software and its   *
# * documentation strictly for non-commercial purposes is hereby granted   *
# * without fee, provided that the above copyright notice appears in all   *
# * copies and that both the copyright notice and this permission notice   *
# * appear in the supporting documentation. The authors make no claims     *
# * about the suitability of this software for any purpose. It is          *
# * provided "as is" without express or implied warranty.                  *
# **************************************************************************

cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)

project(FOCAL CXX C)
enable_testing()

foreach(p CMP0005  # Properly escape preprocessor definitions (v2.6, v3.0.2)
          CMP0025  # Compiler id for Apple Clang is now AppleClang (v3.0)
          CMP0042  # MACOSX_RPATH is enabled by default (v3.0)
          CMP0053  # Simplify variable reference and escape sequence evaluation (v3.1.3)
          CMP0068  # RPATH settings on macOS do not affect install_name (v3.9 and newer)
       )
  if(POLICY ${p})
    cmake_policy(SET ${p} NEW)
  endif()
endforeach()

message(STATUS "CMake platform: ${CMAKE_SYSTEM}")
message(STATUS "Build folder: ${FOCAL_BINARY_DIR}")
message(STATUS "Source folder: ${FOCAL_SOURCE_DIR}")
message(STATUS "Installation folder: ${CMAKE_INSTALL_PREFIX}")

# CMake supports different build types by default. We want the DEBUG build type
# to have "-g -O0" flags: by default it only has "-g"
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")

# You can change the build type using
# cmake -DCMAKE_BUILD_TYPE=DEBUG | RELEASE | RELWITHDEBINFO | MINSIZEREL ...
if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE RELWITHDEBINFO)
endif(NOT CMAKE_BUILD_TYPE)
string(TOUPPER ${CMAKE_BUILD_TYPE} CMAKE_BUILD_TYPE)
set(CMAKE_ALLOWED_BUILD_TYPES DEBUG RELEASE RELWITHDEBINFO COVERAGE CUSTOM_EXTERNAL)
list(FIND CMAKE_ALLOWED_BUILD_TYPES "${CMAKE_BUILD_TYPE}" VALID_BUILD_TYPE)
if(${VALID_BUILD_TYPE} EQUAL -1)
  string(REPLACE ";" ", " CMAKE_ALLOWED_BUILD_TYPES_FLAT "${CMAKE_ALLOWED_BUILD_TYPES}")
  message(FATAL_ERROR "Invalid build type ${CMAKE_BUILD_TYPE}. Use one of: ${CMAKE_ALLOWED_BUILD_TYPES_FLAT}. Build type is case-sensitive.")
endif()
message(STATUS "Build type: ${CMAKE_BUILD_TYPE} (${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}})")

# Path to additonal modules
set(CMAKE_MODULE_PATH "${FOCAL_SOURCE_DIR}/cmake")

# Retrieve Git version and revision
# FOCAL_VERSION
# FOCAL_REVISION
# FOCAL_SERIAL
include(CheckGitVersion)

include(CheckCXXCompilerFlag)

#       - CLANG_MAJOR.CLANG_MINOR or
#       - GCC_MAJOR.GCC_MINOR.GCC_PATCH
include(CheckCompiler)

# Utility to generate PARfiles
include(cmake/GenParFile.cmake)
# Shared library suffix
if (NOT CMAKE_SYSTEM_NAME STREQUAL Windows)
  set(CMAKE_SHARED_LIBRARY_SUFFIX .so)
endif (NOT CMAKE_SYSTEM_NAME STREQUAL Windows)

# Be sure about where libraries and binaries are put
set(LIBRARY_OUTPUT_PATH "${CMAKE_BINARY_DIR}/lib")
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}/bin")

# Build targets with install rpath on Mac to dramatically speed up installation
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
  if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    set(CMAKE_INSTALL_RPATH "@loader_path/../lib")
  endif()
endif()
if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
endif()
unset(isSystemDir)

# AliRoot Core mandatory
find_package(AliRoot REQUIRED)

# ROOT configuration mandatory
if(ROOTSYS)
  find_package(ROOT REQUIRED)

  # ROOT must be built with XML2 support
  if(NOT ROOT_HASXML)
    message(FATAL_ERROR "ROOT was not built with xml2 support. Please reinstall or rebuild ROOT with xml2 support")
  endif(NOT ROOT_HASXML)

  # Issue a warning if ROOT does not have AliEn support (this is wrong in most cases)
  if(NOT ROOT_HASALIEN)
    message(WARNING "ROOT has been built without AliEn support: some features might be unavailable!")
  endif(NOT ROOT_HASALIEN)

else()
  message(FATAL_ERROR "ROOT installation not found!\nPlease point to the ROOT installation using -DROOTSYS=ROOT_INSTALL_DIR")
endif(ROOTSYS)

# Enable C++11 by default if found in ROOT
if(ROOT_HAS_CXX11 AND NOT DISABLE_CXX11)
  message(STATUS "Enabling C++11")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()

# Enable C++14 by default if found in ROOT
if(ROOT_HAS_CXX14 AND NOT DISABLE_CXX14)
  message(STATUS "Enabling C++14")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
endif()

# Enable C++17 by default if found in ROOT
if(ROOT_HAS_CXX17 AND NOT DISABLE_CXX17)
  message(STATUS "Enabling C++17")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
endif()

# Turn some common warnings into errors
check_cxx_compiler_flag(-Werror=mismatched-new-delete CXX_COMPILER_HAS_MISMATCHED_NEW_DELETE)
if(CXX_COMPILER_HAS_MISMATCHED_NEW_DELETE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=mismatched-new-delete")
endif()
check_cxx_compiler_flag(-Werror=delete-non-virtual-dtor CXX_COMPILER_DELETE_NON_VIRTUAL_DTOR)
if(CXX_COMPILER_HAS_MISMATCHED_NEW_DELETE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=delete-non-virtual-dtor")
endif()

# Turn OFF some -Werrors
check_cxx_compiler_flag(-Wno-error=strict-aliasing CXX_COMPILER_STRICT_ALIASING)
if(CXX_COMPILER_STRICT_ALIASING)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-error=strict-aliasing")
endif()

# Turn on "all" warnings, but skip missing braces warning
check_cxx_compiler_flag(-Wall CXX_COMPILER_HAS_WALL)
if(CXX_COMPILER_HAS_WALL)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
endif()
check_cxx_compiler_flag(-Wno-missing-braces CXX_COMPILER_HAS_WNO_MISSING_BRACES)
if(CXX_COMPILER_HAS_WNO_MISSING_BRACES)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-missing-braces")
endif()

# Enable -Werror if requested
if(WARNINGS_AS_ERRORS)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
  message(WARNING "Treating all C++ compile warnings as errors!")
endif()

# Issue a warning if AliRoot Core does not have AliEn support (this is wrong in most cases)
if(NOT AliRoot_HASALIEN)
  message(WARNING "AliRoot Core has been built without with AliEn support: some features might be unavailable!")
endif()

# ROOT dictionaries and maps
include(CMakeALICE)

# General flags -> Should be moved into a configuration file
  set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)
  # Avoid problems with -fPIE (set automatically by the previous line).
  set(CMAKE_CXX_COMPILE_OPTIONS_PIE "")
  set(CMAKE_C_COMPILE_OPTIONS_PIE "")
  set(CMAKE_Fortran_COMPILE_OPTIONS_PIE "")
  set(CMAKE_NO_SYSTEM_FROM_IMPORTED TRUE)


# EMCAL libraries
add_subdirectory(FOCALbase)
add_subdirectory(FOCALgen)
add_subdirectory(FOCALsim)

# Install the macros, docs
#install(DIRECTORY macros DESTINATION EMCAL)
#install(DIRECTORY mapping DESTINATION EMCAL)
#if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/doc)
#    install(DIRECTORY doc DESTINATION EMCAL)
#elseif(FATAL_IF_MISSING)
#    message(FATAL_ERROR "doc is missing")
#endif()

message(STATUS "FOCAL enabled")
