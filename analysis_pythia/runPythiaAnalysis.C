void runPythiaAnalysis(Int_t startFolder, Int_t endFolder, Int_t startEvent, Int_t endEvent) {
  gROOT->LoadMacro("$HOME/FOCAL/FOCAL/LoadFOCAL.C");
  LoadFOCAL("/home/focal/FOCAL/FOCAL");

  gSystem->AddIncludePath("-I$ALICE_ROOT/include -I$HOME/users/mvl/FOCAL/FOCAL");
  gROOT->LoadMacro("analyzePythiaAlgo.cxx+g");
  
  char *dataSampleTag="pythia_MBtrig_13tev_loi2014";
  char* simFolder="/home/focal/storage/sims/pythia/MBtrig_13tev";
  //char *dataSampleTag="pythia_MBEMtrig_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/MBEMtrig_13tev";
  //char *dataSampleTag="pythia_MBEMdecaytrig_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/MBEMdecaytrig_13tev";
  //char *dataSampleTag="pythia_dirgamma_13tev_loi2014";
  //char* simFolder="/home/focal/storage/sims/pythia/dirgamma_13tev";

  char *outputdir = "results";
  char *clustersFolder="../clustering/results";

  analyzePythiaAlgo(startFolder, endFolder, startEvent, endEvent, simFolder, clustersFolder, dataSampleTag, outputdir);
}
