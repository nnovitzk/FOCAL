#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TProfile.h"
#include "TH2.h"
#include "TH3.h"
#include "TParticle.h"
#include "TSystem.h"
#include "Riostream.h"

#include "AliRunLoader.h"
#include "AliStack.h"
#include "AliFOCALCluster.h"

using std::cout;
using std::endl;

Float_t eta(TParticle* ); // declaration, implementation at end of file
Float_t phi(TParticle* );

void analyzePythiaAlgo(Int_t startFolder, Int_t endFolder, Int_t startEvent, Int_t endEvent, const Char_t* simFolder, const Char_t *clustersFolder, const Char_t* dataSampleTag, const Char_t *outputdir) {

  //const Float_t Ecut = 5; //[GeV] cut on cluster energy for pi0 candidate pairs

  // Old value for 4m position
  // const Float_t Ecut = 0.5; //[GeV] cut on cluster energy for pi0 candidate pairs
  // New value 7m (20150324)
  const Float_t Ecut = 2.0; //[GeV] cut on cluster energy for pi0 candidate pairs
  const Float_t kPi0Mass = 0.135;


  //LoadFOCAL_LOI("../../../FOCALlibs");

  for (Int_t nfolder = startFolder; nfolder <= endFolder; nfolder++) {
    char filename[200];
    sprintf(filename,"%s/clusters_%s_%i.root",clustersFolder,dataSampleTag,nfolder);

    TFile *clusterFile = new TFile(filename);
    cout << "Clusters from: " << clusterFile->GetName() << endl;
    
    TFile *fout = new TFile(Form("%s/analysis_%s_%d.root",outputdir,dataSampleTag,nfolder),"RECREATE");
    fout->cd();
    
    Float_t E_match, pt_match, phi_match, eta_match;
    Float_t pt_parent_match;
    Float_t E_match_ge, pt_match_ge;
    Float_t vx_match, vy_match, vz_match;
    Float_t dist_match, dist_match_ge;
    Int_t pdg_match, pdg_parent_match;
    Int_t n_match_02, n_match_05;
    Float_t Eclust, Xclust, Yclust, Zclust, ptclust, etaclust;
    Float_t W1Cclust, W2Cclust, ERclust;
    Float_t W1Fclust, W2Fclust;
    Float_t imass_pi0, pi0_Eclust2; // for pi0 rejection
    Float_t ptisoR2, ptisoR4; // pt sums for isolation cut
    Float_t EHCAL; // pt sums for isolation cut
    Float_t ptisoR2HCAL, ptisoR4HCAL; // pt sums for isolation cut
    Float_t ptisoR2Had, ptisoR2EM, ptisoR4Had, ptisoR4EM; // pt sums for isolation cut
    Int_t ievt, iClust;

    TTree * results = new TTree("clustTree","cluster Info Tree");
    results->Branch("nfolder",&nfolder,"nfolder/I");
    results->Branch("ievt",&ievt,"ievt/I");
    results->Branch("iclust",&iClust,"clust/I");
    results->Branch("Eclust",&Eclust,"Eclust/F");
    results->Branch("Xclust",&Xclust,"Xclust/F");
    results->Branch("Yclust",&Yclust,"Yclust/F");
    results->Branch("Zclust",&Zclust,"Zclust/F");
    // shower shape Coarse layers
    results->Branch("W1Cclust",&W1Cclust,"W1Cclust/F"); 
    results->Branch("W2Cclust",&W2Cclust,"W2Cclust/F");
    // shower shape Fine layers
    results->Branch("W1Fclust",&W1Fclust,"W1Fclust/F");
    results->Branch("W2Fclust",&W2Fclust,"W2Fclust/F");

    results->Branch("ERclust",&ERclust,"ERclust/F"); // long en ratio: E5/(E2+E4)
    results->Branch("ptclust",&ptclust,"ptclust/F");
    results->Branch("etaclust",&etaclust,"etaclust/F");
    results->Branch("imass_pi0",&imass_pi0,"imass_pi0/F");
    results->Branch("pi0_Eclust2",&pi0_Eclust2,"pi0_Eclust2/F");

    results->Branch("EHCAL",&EHCAL,"EHCAL/F");  // HCAL energy
    results->Branch("ptisoR2",&ptisoR2,"ptisoR2/F");  // sumpt for isolation from clusters
    results->Branch("ptisoR4",&ptisoR4,"ptisoR4/F"); // sumpt for isolation from clusters
    results->Branch("ptisoR2HCAL",&ptisoR2HCAL,"ptisoR2HCAL/F");  // sumpt for isolation from clusters
    results->Branch("ptisoR4HCAL",&ptisoR4HCAL,"ptisoR4HCAL/F"); // sumpt for isolation from clusters

    // particle level isolation information

    results->Branch("ptisoR2HadMC",&ptisoR2Had,"ptisoR2HadMC/F");  
    results->Branch("ptisoR2EMMC",&ptisoR2EM,"ptisoR2EMMC/F");  
    results->Branch("ptisoR4HadMC",&ptisoR4Had,"ptisoR4HadMC/F");
    results->Branch("ptisoR4EMMC",&ptisoR4EM,"ptisoR4EMMC/F");

    // matching information
    results->Branch("dist_match",&dist_match,"dist_match/F");
    results->Branch("n_match_02",&n_match_02,"n_match_02/I");
    results->Branch("n_match_05",&n_match_05,"n_match_05/I");
    results->Branch("pdg_match",&pdg_match,"pdg_match/I");
    results->Branch("pdg_parent_match",&pdg_parent_match,"pdg_parent_match/I");
    results->Branch("E_match",&E_match,"E_match/F");
    results->Branch("pt_match",&pt_match,"pt_match/F");
    results->Branch("phi_match",&phi_match,"phi_match/F");
    results->Branch("eta_match",&eta_match,"eta_match/F");
    results->Branch("vx_match",&vx_match,"vx_match/F");
    results->Branch("vy_match",&vy_match,"vy_match/F");
    results->Branch("vz_match",&vz_match,"vz_match/F");

    results->Branch("pt_parent_match",&pt_parent_match,"pt_parent_match/F");

    // also store nearest gamma, electron
    results->Branch("dist_match_ge",&dist_match_ge,"dist_match_ge/F");
    results->Branch("E_match_ge",&E_match_ge,"E_match_ge/F");
    results->Branch("pt_match_ge",&pt_match_ge,"pt_match_ge/F");

    TProfile *xsec_h = new TProfile("xsec_h","Cross section",1,0,1);
    TH1D *trials_h = new TH1D("trials_h","Trials",1,0,1);
    TH1D *nevt_h = new TH1D("nevt_h","Number of Events",1,0,1);

  
    TH2F *eta_pt_pi0 = new TH2F("eta_pt_pi0","pt, eta #pi^{0};#eta;p_{T} (GeV/c)",40,2,6,20,0,20);
    TH2F *eta_pt_gammadir = new TH2F("eta_pt_gammadir","pt, eta #gamma_{dir};#eta;p_{T} (GeV/c)",40,2,6,20,0,20);
    TH2F *eta_pt_gammadec = new TH2F("eta_pt_gammadec","pt, eta #gamma_{dec};#eta;p_{T} (GeV/c)",40,2,6,20,0,20);
    TH2F *eta_pt_clust = new TH2F("eta_pt_clust","pt, eta clusters;#eta;p_{T} (GeV/c)",40,2,6,20,0,20);
    TH3F *eta_pt_imass = new TH3F("eta_pt_imass","pt, eta imass pairs;#eta;p_{T} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",40,2,6,20,0,20,100,0,1);    
    TH3F *eta_pt_imass_pi0rej = new TH3F("eta_pt_imass_pi0rej","pt, eta imass pairs with pi0 rejection;#eta;p_{T} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",40,2,6,20,0,20,100,0,1);    

    TH3F *eta_pt_imass_clust = new TH3F("eta_pt_imass_clust","pt, eta imass cluster;#eta_{clust};p_{T,clust} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",40,2,6,20,0,20,100,0,1);

    AliRunLoader *fRunLoader = 0;
    cout << "FOLDER: " << nfolder << endl;
	
    TFile *fin_xsec = new TFile(Form("%s/%d/pyxsec.root",simFolder,nfolder));
    /*
      if (!fin_xsec->IsOpen()) {
      cout << "No cross section info found" << endl;
      continue;
    }
    */
    if (fin_xsec->IsOpen()) {
      TTree *xc_tree = (TTree*) fin_xsec->Get("Xsection");
    
      Double_t xsec;
      UInt_t ntrials;
      xc_tree->SetBranchAddress("xsection",&xsec);
      xc_tree->SetBranchAddress("ntrials",&ntrials);
      xc_tree->GetEvent(0);
      cout << "folder " << nfolder << " trials " << ntrials << " xsec " << xsec << endl;
      xsec_h->Fill(0.5,xsec,ntrials);
      trials_h->Fill(0.5,ntrials);
    }
    delete fin_xsec;

    sprintf(filename,"%s/%i/%s",simFolder,nfolder,"galice.root");
                
    Long_t id= 0 , size = 0, flags = 0, mt = 0;
    if (gSystem->GetPathInfo(filename,&id,&size,&flags,&mt) == 1) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;        
    }
     
    //Alice run loader

    fRunLoader = AliRunLoader::Open(filename);
        
    if (!fRunLoader) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;   
    }
    if (!fRunLoader->GetAliRun()) fRunLoader->LoadgAlice();
    if (!fRunLoader->TreeE()) fRunLoader->LoadHeader();
    if (!fRunLoader->TreeK()) fRunLoader->LoadKinematics();

    //AliRun *gAlice = fRunLoader->GetAliRun();

    //cout << "start loop" << endl;

    TObjArray primTracks;

    Int_t maxEvent = TMath::Min(endEvent, fRunLoader->GetNumberOfEvents());
    for (ievt = startEvent; ievt <= maxEvent; ievt++) {

      // Get MC Stack
      Int_t ie = fRunLoader->GetEvent(ievt);
      cout << "Event: " << ievt << " folder " << nfolder << " event " << ievt << endl;

      if (ie != 0)
         continue;
      nevt_h->Fill(0.5); // count events

      AliStack *stack = fRunLoader->Stack();

      // Build TObjArray of tracks for matching
      primTracks.Clear();

      for (Int_t iTrk = 0; iTrk < stack->GetNtrack(); iTrk++) {
	TParticle *part = stack->Particle(iTrk);
	if (part->GetPdgCode() == 111 && iTrk < stack->GetNprimary()) {
          eta_pt_pi0->Fill(part->Eta(), part->Pt());
        }
	if (part->GetStatusCode() >= 10) // Not stable (Need to cut out e.g. rho0)
	  continue;
        if (part->GetPdgCode() == 22) {
          if (part->GetMother(0) >= 8)
            eta_pt_gammadec->Fill(part->Eta(), part->Pt());
          else
            eta_pt_gammadir->Fill(part->Eta(), part->Pt());
        }
	if (part->GetPdgCode() == 111 || part->GetPdgCode() == 221) // don't match pi0 and eta; look for decay daughters
	  continue;
	if (!stack->IsPhysicalPrimary(iTrk))
	  continue;
	if (part->GetFirstDaughter() >= 0 && stack->IsPhysicalPrimary(part->GetFirstDaughter()))  // if decay daughters are phys prim, only use those
	  continue;

	if (part->Pz() < TMath::Abs(part->Px()) || part->Pz() <= 0)  // rough acceptance cut: 45 degrees forward + extro-plation only works for pz > 0
	  continue;      
	primTracks.AddLast(part);

	// could also store extrapolated positions here...
      }

      Int_t nPrim = primTracks.GetEntries();

      // Get Clusters
      TTree *tClusters = 0;
      if (clusterFile->GetDirectory(Form("Event%i",ievt))) {
        clusterFile->GetDirectory(Form("Event%i",ievt))->GetObject("fTreeR",tClusters);
      }
      else {
        cout << "Cannot find event " << ievt << " in cluster file " << clusterFile->GetName() << endl;
        clusterFile->ls();
      }

      TBranch * bClusters;
      //if (segmentToAnalyze == -1)
      bClusters = tClusters->GetBranch("AliFOCALCluster");  // Branch for final clusters

      //else
      // bClusters = tClusters->GetBranch("AliFOCALClusterItr"); // Branch for segment-by-segment clusters

      TClonesArray * clustersArray = 0;
      bClusters->SetAddress(&clustersArray);
      bClusters->GetEvent(0);

      TArrayF imass_arr(clustersArray->GetEntries()); // array to cache invariant mass values
      TArrayF Eclust2_arr(clustersArray->GetEntries()); // also store cluster energy of second photon candidate
      for (iClust = 0; iClust < clustersArray->GetEntries(); iClust++) {
	AliFOCALCluster *clust = (AliFOCALCluster*) clustersArray->At(iClust);
	Eclust = clust->E();
	Xclust = clust->X();
	Yclust = clust->Y();
	Zclust = clust->Z();

	ptclust = clust->E();

	//cout << "calc pt, eta " << Xclust << " " << Yclust << " " << Zclust << endl;
	Float_t trans = TMath::Sqrt(clust->X()*clust->X() + clust->Y()*clust->Y());      
	Float_t theta = TMath::Pi()/2;
	if (clust->Z() != 0) {
	  ptclust = trans/clust->Z()*clust->E();
	  theta = TMath::ATan(trans/clust->Z());
	}
	etaclust = 1e6;
	if (theta > 1e-6)
	  etaclust = -TMath::Log(TMath::Tan(theta/2.));

	Float_t phiclust = TMath::ATan2(clust->Y(),-clust->X());
	
	eta_pt_clust->Fill(etaclust,ptclust);

        //cout << "Got basic cluster info " << iClust << endl;
	// Cluster shapes
	W1Cclust = 0;
	W2Cclust = 0;
	W1Fclust = 0;
	W2Fclust = 0;
	ERclust = clust->GetSegmentEnergy(2)+clust->GetSegmentEnergy(4);
	if (ERclust > 0)
	  ERclust = clust->GetSegmentEnergy(5)/ERclust;
	else
	  ERclust = 1e3;
	Float_t totEC = 0, totEF = 0;
	for (Int_t iSeg = 0; iSeg < 6; iSeg++) {
	  if (iSeg == 1 || iSeg == 3) {
	    W1Fclust += clust->GetSegmentEnergy(iSeg) * clust->GetWidth1(iSeg);
	    W2Fclust += clust->GetSegmentEnergy(iSeg) * clust->GetWidth2(iSeg);
	    totEF += clust->GetSegmentEnergy(iSeg);
	  }
	  else {
	    W1Cclust += clust->GetSegmentEnergy(iSeg) * clust->GetWidth1(iSeg);
	    W2Cclust += clust->GetSegmentEnergy(iSeg) * clust->GetWidth2(iSeg);
	    totEC += clust->GetSegmentEnergy(iSeg);

	  }
	}
	if (totEC > 0) {
	  W1Cclust /= totEC;
	  W2Cclust /= totEC;
	}
	if (totEF > 0) {
	  W1Fclust /= totEF;
	  W2Fclust /= totEF;
	}

        //cout << "Got shapes" << endl;
	// sum pt for isolation
	// (can be made faster by caching pt, eta)
	ptisoR2 = 0;
	ptisoR4 = 0;

	for (Int_t iClust2 = 0; iClust2 < clustersArray->GetEntries(); iClust2++) {
	  if (iClust == iClust2)
	    continue;
	  AliFOCALCluster *clust2 = (AliFOCALCluster*) clustersArray->At(iClust2);
	  Float_t phiclust2 = TMath::ATan2(clust2->Y(),-clust2->X());
	  Float_t dphi = phiclust2-phiclust;
	  if (dphi < -TMath::Pi())
	    dphi += 2*TMath::Pi();
	  if (dphi > TMath::Pi())
	    dphi -= 2*TMath::Pi();
	  if (TMath::Abs(dphi) > 0.4)
	    continue;
	  
	  Float_t trans2 = TMath::Sqrt(clust2->X()*clust2->X() + clust2->Y()*clust2->Y());      
          Float_t theta2 = 0;
	  if (clust2->Z() != 0) {
	    theta2 = TMath::ATan(trans2/clust2->Z());
	  }
	  
	  Float_t etaclust2 = 1e6;
	  if (theta2 > 1e-6)
            etaclust2 = -TMath::Log(TMath::Tan(theta2/2.));
	  
	  Float_t R = TMath::Sqrt((etaclust2-etaclust)*(etaclust2-etaclust)+dphi*dphi);
	  if (R < 0.4) {
	    Float_t pt = trans2/clust2->Z()*clust2->E();
	    ptisoR4 += pt;
	    if (R < 0.2) 
	      ptisoR2 += pt;

	  }
	}

        EHCAL = clust->GetHCALEnergy();
        ptisoR2HCAL = clust->GetIsoEnergyR2()*ptclust/Eclust; // the correction from E to pt is a bit rough; not so obvious how to improve, though (large showers)
        ptisoR4HCAL = clust->GetIsoEnergyR4()*ptclust/Eclust;

        //cout << "Got isolation" << endl;
	//cout << "pt " << ptclust << " eta " << etaclust << endl;

	// Now find matching particle
	dist_match = 1000;
	pdg_match = -1;
	pdg_parent_match = -1;
	pt_parent_match = -1;
	eta_match = 0;
	pt_match = -1;
	E_match = -1;
	vx_match = 0;
	vy_match = 0;
	vz_match = 0;

	dist_match_ge = 1000;
	pt_match_ge = -1;
	E_match_ge = -1;

	n_match_02 = 0;
	n_match_05 = 0;

	// Only loop over preselected physical primaries 
	//   (includes pi0 decay daughters)

	ptisoR2Had = 0;
	ptisoR2EM = 0;
	ptisoR4Had = 0;
	ptisoR4EM = 0;

	for (Int_t iPrim = 0; iPrim < nPrim; iPrim++) {
	  TParticle *part = (TParticle*) primTracks[iPrim];

	  // first do eta-phi matching for isolation cut
	  Float_t deta = eta(part)-etaclust;
	  Float_t dphi = phi(part)-phiclust;
	  
	  if (dphi < -TMath::Pi())
	    dphi += 2*TMath::Pi();
	  if (dphi > TMath::Pi())
	    dphi -= 2*TMath::Pi();
	  if (TMath::Abs(dphi) > 0.4)
	    continue;

	  Float_t R = TMath::Sqrt(deta*deta+dphi*dphi);
	  if (R < 0.4) {
	    if (part->GetPdgCode() == 22 || TMath::Abs(part->GetPdgCode()) == 11)
	      ptisoR4EM += part->Pt();
	    else
	      ptisoR4Had += part->Pt();
	    if (R < 0.2) {
	      if (part->GetPdgCode() == 22 || TMath::Abs(part->GetPdgCode()) == 11)
		ptisoR2EM += part->Pt();
	      else
		ptisoR2Had += part->Pt();
	      
	    }
	  }
	  else 
	    continue; // don't try matching if R > 0.4

	  // Only works if Pz > 0
	  Float_t dz = clust->Z() - part->Vz();
	  Float_t x =  part->Vx() + part->Px()/part->Pz() * dz;
	  Float_t y =  part->Vy() + part->Py()/part->Pz() * dz;
	  Float_t dist = TMath::Sqrt((x-clust->X())*(x-clust->X())+(y-clust->Y())*(y-clust->Y()));
	  //cout << "x " << x << ", y " << y << ", dist " << dist << endl;
	  if (dist < 0.2)
	    n_match_02++;
	  if (dist < 0.5)
	    n_match_05++;
	  if (dist < dist_match) {
	    dist_match = dist;
	    pt_match = TMath::Sqrt(part->Px()*part->Px()+part->Py()*part->Py());
	    E_match = part->Energy();
	    eta_match = eta(part);
	    phi_match = phi(part);
	    pdg_match = part->GetPdgCode();
	    if (part->GetMother(0) >= 0) {
	      TParticle *parent = stack->Particle(part->GetMother(0));
	      pdg_parent_match = parent->GetPdgCode(); 
              pt_parent_match = parent->Pt();
	    }
	    /*
	      if (abs(pdg_match) == 11) {
	      cout << "Electron, vz " << part->Vz() << " mother ";
	      TParticle *mother = stack->Particle(part->GetMother(0));
	      cout  << part->GetMother(0) << " pdg " << mother->GetPdgCode()
	      << " grandmother "
	      << mother->GetMother(0) << " pdg " << stack->Particle(mother->GetMother(0))->GetPdgCode() << endl;
	      cout << "Nprim " << stack->GetNprimary() << endl;
	      }
	    */
	    vx_match =  part->Vx();
	    vy_match =  part->Vy();
	    vz_match =  part->Vz();
	  }

	  // also store gammas, electrons

	  if ((part->GetPdgCode() == 22 || TMath::Abs(part->GetPdgCode())==11) &&
	      dist < dist_match_ge) {
	    dist_match_ge = dist;
	    pt_match_ge = TMath::Sqrt(part->Px()*part->Px()+part->Py()*part->Py());
	    E_match_ge = part->Energy();
	  }
	}

        //cout << "Got matches" << endl;

	// make invariant mass pairs
        imass_pi0 = imass_arr[iClust];
        pi0_Eclust2 = Eclust2_arr[iClust];

	// Pair info for histogramming after pi0 rejection
	TArrayF imass_hist_arr(clustersArray->GetEntries());
	TArrayF eta_hist_arr(clustersArray->GetEntries());
	TArrayF pt_hist_arr(clustersArray->GetEntries());

        if (clust->E() >= Ecut) { 
	  TVector3 p1(clust->X(),clust->Y(),clust->Z());
          p1.SetMag(clust->E());
	  for (Int_t iClust2 = iClust+1; iClust2 < clustersArray->GetEntries(); iClust2++) {
	    AliFOCALCluster *clust2 = (AliFOCALCluster*) clustersArray->At(iClust2);
	    if (clust2->E() < Ecut) {
	      imass_hist_arr[iClust2]=-1;
	      continue;
	    }
	    TVector3 p2(clust2->X(),clust2->Y(),clust2->Z());
	    p2.SetMag(clust2->E());
	    Double_t imass = 2*clust->E()*clust2->E()-2*p1*p2;
            if (imass < 0) {
              cout << "imass " << imass << " E1 " << clust->E() << " E2 " << clust2->E() 
                  << endl << " p1 " << p1.X() << " " << p1.Y() << " " << p1.Z()  
                  << endl << " p2 " << p2.X() << " " << p2.Y() << " " << p2.Z() << endl;
              imass = 0;
            }
            else {
              imass = TMath::Sqrt(imass);
            }
	    TVector3 ptot(p1+p2);

	    // Store pair info for histogramming after pi0 rejection
	    imass_hist_arr[iClust2] = imass;
	    eta_hist_arr[iClust2] = ptot.Eta();
	    pt_hist_arr[iClust2] = ptot.Perp();

	    eta_pt_imass->Fill(ptot.Eta(),ptot.Perp(),imass);

	    eta_pt_imass_clust->Fill(p1.Eta(),p1.Perp(),imass);
	    eta_pt_imass_clust->Fill(p2.Eta(),p2.Perp(),imass);
	    // store mass of pair closest to pi0 mass in tree
	    if (imass_pi0==0 || (TMath::Abs(imass-kPi0Mass) < TMath::Abs(imass_pi0-kPi0Mass))) {
	      imass_pi0 = imass;
	      pi0_Eclust2 = clust2->E();
	      if (imass_arr[iClust2] == 0 ||
		  (TMath::Abs(imass-kPi0Mass) < TMath::Abs(imass_arr[iClust2]-kPi0Mass))) {
		imass_arr[iClust2] = imass;
		Eclust2_arr[iClust2] = clust->E();
	      }
	    }
	  }	

          // Hmm, seem to get too many entries when I change the condition to be compatible with pi0. Don't understand...
	  if (imass_pi0 < 0.07 || imass_pi0 > 0.17) {
	    for (Int_t iClust2 = iClust+1; iClust2 < clustersArray->GetEntries(); iClust2++) {
	      if (imass_hist_arr[iClust2] >= 0) {
		eta_pt_imass_pi0rej->Fill(eta_hist_arr[iClust2], pt_hist_arr[iClust2], imass_hist_arr[iClust2]);
	      }
	    }
	  }

        }
	results->Fill();
        //cout << "Cluster " << iClust << " filled " << endl;
      }
    }

    fout->Write();
    fout->Close();
    clusterFile->Close();
    fRunLoader->Delete();
  }
}

Float_t eta(TParticle *part) {
  Double_t pt = sqrt (part->Px()*part->Px()+part->Py()*part->Py());
  if (pt == 0)
    return 9999;
  return -log(tan(TMath::ATan2(pt,part->Pz())/2));
}

Float_t phi(TParticle *part) {
  return TMath::ATan2(part->Py(),-part->Px());
}


