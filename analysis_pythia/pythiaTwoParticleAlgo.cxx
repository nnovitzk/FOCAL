#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TProfile.h"
#include "TH2.h"
#include "TH3.h"
#include "TParticle.h"
#include "TSystem.h"
#include "Riostream.h"

#include "AliRunLoader.h"
#include "AliStack.h"
#include "AliFOCALCluster.h"

using std::cout;
using std::endl;

Float_t eta(TParticle* ); // declaration, implementation at end of file
Float_t phi(TParticle* );

void pythiaTwoParticleAlgo(Int_t startFolder, Int_t endFolder, Int_t startEvent, Int_t endEvent, const Char_t* simFolder, const Char_t *clustersFolder, const Char_t* dataSampleTag, const Char_t *outputdir) {

  //const Float_t Ecut = 5; //[GeV] cut on cluster energy for pi0 candidate pairs

  // Old value for 4m position
  // const Float_t Ecut = 0.5; //[GeV] cut on cluster energy for pi0 candidate pairs
  // New value 7m (20150324)
  const Float_t Ecut = 2.0; //[GeV] cut on cluster energy for pi0 candidate pairs
  const Float_t kPi0Mass = 0.135;


  //LoadFOCAL_LOI("../../../FOCALlibs");

  for (Int_t nfolder = startFolder; nfolder <= endFolder; nfolder++) {
    char filename[200];
    sprintf(filename,"%s/clusters_%s_%i.root",clustersFolder,dataSampleTag,nfolder);

    TFile *clusterFile = new TFile(filename);
    cout << "Clusters from: " << clusterFile->GetName() << endl;
    
    TFile *fout = new TFile(Form("%s/analysis_twopart_%s_%d.root",outputdir,dataSampleTag,nfolder),"RECREATE");
    fout->cd();

    Float_t Eclust, Xclust, Yclust, Zclust, ptclust, etaclust;
    /*
    Float_t E_match, pt_match, phi_match, eta_match;
    Float_t pt_parent_match;
    Float_t E_match_ge, pt_match_ge;
    Float_t vx_match, vy_match, vz_match;
    Float_t dist_match, dist_match_ge;
    Int_t pdg_match, pdg_parent_match;
    Int_t n_match_02, n_match_05;
    Float_t W1Cclust, W2Cclust, ERclust;
    Float_t W1Fclust, W2Fclust;
    Float_t imass_pi0, pi0_Eclust2; // for pi0 rejection
    Float_t ptisoR2, ptisoR4; // pt sums for isolation cut
    Float_t EHCAL; // pt sums for isolation cut
    Float_t ptisoR2HCAL, ptisoR4HCAL; // pt sums for isolation cut
    Float_t ptisoR2Had, ptisoR2EM, ptisoR4Had, ptisoR4EM; // pt sums for isolation cut
    */
    Int_t ievt;

    TProfile *xsec_h = new TProfile("xsec_h","Cross section",1,0,1);
    TH1D *trials_h = new TH1D("trials_h","Trials",1,0,1);
    TH1D *nevt_h = new TH1D("nevt_h","Number of Events",1,0,1);

  
    TH2F *eta_pt_clust = new TH2F("eta_pt_clust","pt, eta clusters;#eta;p_{T} (GeV/c)",30,2,5,15,0,15);
    TH3F *eta_pt_imass = new TH3F("eta_pt_imass","pt, eta imass pairs;#eta;p_{T} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,2,5,15,0,15,100,0,1);    
    //TH3F *eta_pt_imass_pi0rej = new TH3F("eta_pt_imass_pi0rej","pt, eta imass pairs with pi0 rejection;#eta;p_{T} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,2,5,15,0,15,100,0,1);    

    TH3F *eta_pt_imass_clust = new TH3F("eta_pt_imass_clust","pt, eta imass cluster;#eta_{clust};p_{T,clust} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,2,5,15,0,15,100,0,1);

    TH3F *dphi_pt_imass = new TH3F("dphi_pt_imass","dphi, pt, eta imass;#Delta#phi;p_{T,clust} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,-0.5*TMath::Pi(),1.5*TMath::Pi(),15,0,15,100,0,1);

    TH3F *dphi_pt_imass_pi0reject = new TH3F("dphi_pt_imass_pi0reject","dphi, pt, eta imass with pi0 rejection;#Delta#phi;p_{T,clust} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,-0.5*TMath::Pi(),1.5*TMath::Pi(),15,0,15,100,0,1);

    TH3F *dphi_pt_imass_pi0select = new TH3F("dphi_pt_imass_pi0select","dphi, pt, eta imass for pi0 window;#Delta#phi;p_{T,clust} (GeV/c);m_{#gamma#gamma} (GeV/c^{2})",30,-0.5*TMath::Pi(),1.5*TMath::Pi(),15,0,15,100,0,1);

    TH1F *ntrig = new TH1F("ntrig","number of trigger particles;class",3,0,3);
    ntrig->GetXaxis()->SetBinLabel(1,"all");
    ntrig->GetXaxis()->SetBinLabel(2,"pi0-reject");
    ntrig->GetXaxis()->SetBinLabel(3,"pi0-select");

    const Float_t minMassReject = 0.07;
    const Float_t maxMassReject = 0.18;
    //const Float_t minMassSide1 = 0.03;
    //const Float_t maxMassSide1 = 0.05;
    //const Float_t minMassSide2 = 0.2;
    //const Float_t maxMassSide2 = 0.25;
    const Float_t minPtTrig = 5;

    AliRunLoader *fRunLoader = 0;
    cout << "FOLDER: " << nfolder << endl;
	
    TFile *fin_xsec = new TFile(Form("%s/%d/pyxsec.root",simFolder,nfolder));
    /*
      if (!fin_xsec->IsOpen()) {
      cout << "No cross section info found" << endl;
      continue;
    }
    */
    if (fin_xsec->IsOpen()) {
      TTree *xc_tree = (TTree*) fin_xsec->Get("Xsection");
    
      Double_t xsec;
      UInt_t ntrials;
      xc_tree->SetBranchAddress("xsection",&xsec);
      xc_tree->SetBranchAddress("ntrials",&ntrials);
      xc_tree->GetEvent(0);
      cout << "folder " << nfolder << " trials " << ntrials << " xsec " << xsec << endl;
      xsec_h->Fill(0.5,xsec,ntrials);
      trials_h->Fill(0.5,ntrials);
    }
    delete fin_xsec;

    sprintf(filename,"%s/%i/%s",simFolder,nfolder,"galice.root");
                
    Long_t * id= 0 , *size = 0, *flags = 0, *mt = 0;
    if (gSystem->GetPathInfo(filename,id,size,flags,mt) == 1) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;        
    }
     
    //Alice run loader

    fRunLoader = AliRunLoader::Open(filename);
        
    if (!fRunLoader) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;   
    }
    if (!fRunLoader->GetAliRun()) fRunLoader->LoadgAlice();
    if (!fRunLoader->TreeE()) fRunLoader->LoadHeader();
    if (!fRunLoader->TreeK()) fRunLoader->LoadKinematics();

    //AliRun *gAlice = fRunLoader->GetAliRun();

    //cout << "start loop" << endl;

    TObjArray primTracks;

    Int_t maxEvent = TMath::Min(endEvent, fRunLoader->GetNumberOfEvents());
    for (ievt = startEvent; ievt <= maxEvent; ievt++) {

      // Get MC Stack
      Int_t ie = fRunLoader->GetEvent(ievt);
      cout << "Event: " << ievt << " folder " << nfolder << " event " << ievt << endl;

      if (ie != 0)
         continue;
      nevt_h->Fill(0.5); // count events

      AliStack *stack = fRunLoader->Stack();

      // Matching logic removed
      // Get Clusters
      TTree *tClusters = 0;
      if (clusterFile->GetDirectory(Form("Event%i",ievt))) {
        clusterFile->GetDirectory(Form("Event%i",ievt))->GetObject("fTreeR",tClusters);
      }
      else {
        cout << "Cannot find event " << ievt << " in cluster file " << clusterFile->GetName() << endl;
        clusterFile->ls();
      }

      TBranch * bClusters;
      //if (segmentToAnalyze == -1)
      bClusters = tClusters->GetBranch("AliFOCALCluster");  // Branch for final clusters

      //else
      // bClusters = tClusters->GetBranch("AliFOCALClusterItr"); // Branch for segment-by-segment clusters

      TClonesArray * clustersArray = 0;
      bClusters->SetAddress(&clustersArray);
      bClusters->GetEvent(0);

      Int_t pairs_made = 0;
      TArrayF imass_arr(clustersArray->GetEntries()); // array to cache invariant mass values
      TArrayI IDclust2_arr(clustersArray->GetEntries()); // also store cluster ID
      // pair kinematics
      TArrayF pt_pair(clustersArray->GetEntries()); // also store cluster energy of second photon candidat
      TArrayF phi_pair(clustersArray->GetEntries()); // also store cluster energy of second photon candidate
      TArrayF eta_pair(clustersArray->GetEntries()); // also store cluster energy of second photon candidate
      for (Int_t iClustTrig = 0; iClustTrig < clustersArray->GetEntries(); iClustTrig++) {
	AliFOCALCluster *clust = (AliFOCALCluster*) clustersArray->At(iClustTrig);
	Eclust = clust->E();
	Xclust = clust->X();
	Yclust = clust->Y();
	Zclust = clust->Z();

	ptclust = clust->E();

	//cout << "calc pt, eta " << Xclust << " " << Yclust << " " << Zclust << endl;
	Float_t trans = TMath::Sqrt(clust->X()*clust->X() + clust->Y()*clust->Y());      
	Float_t theta = TMath::Pi()/2;
	if (clust->Z() != 0) {
	  ptclust = trans/clust->Z()*clust->E();
	  theta = TMath::ATan(trans/clust->Z());
	}
	etaclust = 1e6;
	if (theta > 1e-6)
	  etaclust = -TMath::Log(TMath::Tan(theta/2.));

	//Float_t phiclust = TMath::ATan2(clust->Y(),-clust->X());
	Float_t phiclust = TMath::ATan2(clust->Y(),clust->X());
	
	eta_pt_clust->Fill(etaclust,ptclust);

	if (ptclust > minPtTrig) {
	  if (!pairs_made) {
	    pairs_made = 1;
	    for (Int_t iClust1 = 0; iClust1 < clustersArray->GetEntries(); iClust1++) {
	      AliFOCALCluster *clust1 = (AliFOCALCluster*) clustersArray->At(iClust1);
	      imass_arr[iClust1] = 1000;
	      IDclust2_arr[iClust1] = -1;
	      if (clust1->E() < Ecut) 
		continue;

	      TVector3 p1(clust1->X(),clust1->Y(),clust1->Z());
	      p1.SetMag(clust1->E());
	      
	      for (Int_t iClust2 = 0; iClust2 < clustersArray->GetEntries(); iClust2++) {
		if (iClust1 == iClust2)
		  continue;
		AliFOCALCluster *clust2 = (AliFOCALCluster*) clustersArray->At(iClust2);
		if (clust2->E() < Ecut) 
		  continue;

		TVector3 p2(clust2->X(),clust2->Y(),clust2->Z());
		p2.SetMag(clust2->E());
		Double_t imass = 2*clust1->E()*clust2->E()-2*p1*p2;
		if (imass < 0) {
		  cout << "imass " << imass << " E1 " << clust->E() << " E2 " << clust2->E() 
		       << endl << " p1 " << p1.X() << " " << p1.Y() << " " << p1.Z()  
		       << endl << " p2 " << p2.X() << " " << p2.Y() << " " << p2.Z() << endl;
		  imass = 0;
		}
		else {
		  imass = TMath::Sqrt(imass);
		}
		TVector3 ptot(p1+p2);
		eta_pt_imass->Fill(ptot.Eta(),ptot.Perp(),imass);
		eta_pt_imass_clust->Fill(p1.Eta(),p1.Perp(),imass);
		eta_pt_imass_clust->Fill(p2.Eta(),p2.Perp(),imass);

		if ((TMath::Abs(imass-kPi0Mass) < TMath::Abs(imass_arr[iClust1]-kPi0Mass))) {
		  imass_arr[iClust1] = imass;
		  IDclust2_arr[iClust1] = iClust2;
		  eta_pair[iClust1] = ptot.Eta();
		  phi_pair[iClust1] = ptot.Phi();
		  pt_pair[iClust1] = ptot.Perp();
		}
	      }	
	    }
	  }

	  ntrig->Fill(0.5);
	  if (imass_arr[iClustTrig] > minMassReject &&
	      imass_arr[iClustTrig] < maxMassReject) 
	    ntrig->Fill(2.5);
	  else
	    ntrig->Fill(1.5);
	  for (Int_t iClustAssoc = 0; iClustAssoc < clustersArray->GetEntries(); iClustAssoc++) {
	    if (iClustAssoc != iClustTrig &&
                IDclust2_arr[iClustAssoc] > 0 &&
		(IDclust2_arr[iClustAssoc] > iClustAssoc ||
		 IDclust2_arr[IDclust2_arr[iClustAssoc]] != iClustAssoc)) { // prevent double counting of pairs with two labels if they are paired together
	      Float_t dphi = phi_pair[iClustAssoc] - phiclust;
	      if (dphi < 0.5*TMath::Pi())
		dphi += 2*TMath::Pi();
	      if (dphi > 1.5*TMath::Pi())
		dphi -= 2*TMath::Pi();
	      dphi_pt_imass->Fill(dphi, pt_pair[iClustAssoc], imass_arr[iClustAssoc]);
	      if (imass_arr[iClustTrig] > minMassReject &&
		  imass_arr[iClustTrig] < maxMassReject) {
		dphi_pt_imass_pi0select->Fill(dphi, pt_pair[iClustAssoc], imass_arr[iClustAssoc]);
	      }
	      else {
		dphi_pt_imass_pi0reject->Fill(dphi, pt_pair[iClustAssoc], imass_arr[iClustAssoc]);
	      }	  
	    }
	  }
        //cout << "Cluster " << iClust << " filled " << endl;
	}
      }
    }
    fout->Write();
    fout->Close();
    clusterFile->Close();
    fRunLoader->Delete();
  }
}

Float_t eta(TParticle *part) {
  Double_t pt = sqrt (part->Px()*part->Px()+part->Py()*part->Py());
  if (pt == 0)
    return 9999;
  return -log(tan(TMath::ATan2(pt,part->Pz())/2));
}

Float_t phi(TParticle *part) {
  return TMath::ATan2(part->Py(),-part->Px());
}


