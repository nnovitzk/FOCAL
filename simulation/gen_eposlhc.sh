#!/bin/bash

# prepare environment
#alienv -a ubuntu1804_x86-64 enter CRMC/latest

# export LD_LIBRARY_PATH=/opt/alice/sw/ubuntu1804_x86-64/CRMC/v1.7.0-correctHepMC-1/lib:/opt/alice/sw/ubuntu1804_x86-64/HepMC/HEPMC_02_06_10-1/lib:/opt/alice/sw/ubuntu1804_x86-64/boost/v1.70.0-1/lib:.
# export PATH=/opt/alice/sw/ubuntu1804_x86-64/CRMC/v1.7.0-correctHepMC-1/bin:/opt/alice/sw/ubuntu1804_x86-64/HepMC/HEPMC_02_06_10-1/bin:.:/home/loizides/bin:/home/loizides/scripts:/home/loizides/scripts/cltpx1:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games

# cd $HOME/focal_sims/epos_ppb
config=crmc.param

# file of fifo
output=$1
if test -z "$output"; then
    output=myfifo
fi
# mkfifo $output  // Done by GenExt?

# run CRMC
crmc -c $config -n10000000 -m0 -S8800 -i1 -I208 -f $output

