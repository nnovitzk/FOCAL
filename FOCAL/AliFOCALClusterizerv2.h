#ifndef ALIFOCALClusterizerv2_H
#define ALIFOCALClusterizerv2_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
* See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  16 2010                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//
// Base class for the clusterization algorithm 

#include "AliFOCALClusterizer.h"
#include "AliFOCALDigitizer.h"
#include "AliFOCALCellMap.h"
#include "AliFOCALRinger.h"
#include <TVectorF.h>

class AliFOCALCluster;
class AliFOCALGeometry;
class AliFOCALPad;
class AliFOCALCell;
class AliFOCALhit;


class AliFOCALClusterizerv2 : public AliFOCALClusterizer
{

	public:
		AliFOCALClusterizerv2() ;        // default constructor 
		AliFOCALClusterizerv2(const AliFOCALClusterizerv2 &ali);
		AliFOCALClusterizerv2(AliFOCALClusterizerv2 *ali);
		AliFOCALClusterizerv2 &operator=(const AliFOCALClusterizerv2 &ali);
		virtual ~AliFOCALClusterizerv2() ; // destructor 
		
		virtual void AnalyzeEvent(TTree *tree, int type);
		virtual void Digits2Clusters(Option_t *option);     // Digits to cluster (Digits -> Cell -> Clusterizer)
		virtual void Hits2Clusters(Option_t *option);       // Hits to cluster (Hits -> Pad -> Cell -> Clusterizer)
		virtual void RunCluster(int Segments, TObjArray *cluster);
		
		virtual void SetClusteringEnergyThreshold(float cluth) { fClusteringEnergyThreshold = cluth; }
		virtual void SetLocalEnergyThreshold(float mine) { fLocalEnergyThreshold = mine; }
		virtual void SetChi2Limit(float min) { fCHI2Limit = min; }		
		virtual void SetDistance(float min) { fDistance = min; } // function to SET Distance
		virtual void SetSegLayer(bool var) { fSegLayer = var; }

                void SetPadNoiseSigma(float Noise) {fPadNoiseSigma = Noise;}
                void SetPixelNoiseProb(float Noise) {fPixelNoiseProb = Noise;}
		virtual void SetDebugMode(Bool_t val) {fDebugMode = val; }		
		virtual void SetMode(bool valC, bool valF) {modeC = valC; modeF = valF;}		
		virtual void SetModeCorrectGW(bool val) {modeCorrectGW = val; }		
		virtual void SetW_0(Float_t valC, Float_t valF) {wC_0 = valC; wF_0 = valF;}		

		virtual void GetNumberOfClustersFound(int numb )const { numb = fNumberOfClusters; }
		virtual float GetDistance() { return fDistance; } // function to GET Distance  // should probably be MaxRing?
		virtual float GetClusteringEnergyThreshold() const { return fClusteringEnergyThreshold; }
		virtual float GetLocalEnergyThreshold() const { return fLocalEnergyThreshold; }
		virtual Float_t GetRejectionRatio(Int_t segment) const;
		virtual Float_t GetRejectionRatio2(Int_t segment) const;
		virtual Float_t GetWeightingPar1(Int_t segment) const;
		virtual Float_t GetWeightingPar2(Int_t segment) const;
		virtual Float_t GetWeightingPar3(Int_t segment) const;
		virtual Float_t GetNCellsThreshold(Int_t segment) const;
		virtual Float_t GetShapeThreshold(Int_t segment) const;
		virtual Float_t * GetCalibrationPars(Int_t segment) const;
		virtual Float_t GetSeedEnergyTreshold(Int_t segment) const;
		virtual Float_t GetClusterEnergyTreshold(Int_t segment) const;
		virtual AliFOCALGeometry * GetGeometry() const { return fGeom; }
		virtual Bool_t GetDebugMode() const {return fDebugMode; }
				
		// Methods for clusterizer control
		void LoadDigits2Cell();    // Loading Digits and filling to Array of Pads
		void LoadHits2Cells();
		void LoadHits2CellsEmbedding();
		void ClearClusterArrays();
		void SortCells();
		void ResetSeeds();
		void CreateAndFillCellMap();
		void DeleteCellMap();
		void MakeClustersPerSegment(Bool_t * doSegment, TObjArray * preTracks);
                void MakeHCALClusters(void);
		Float_t GetHCALEnergy(Float_t x, Float_t y, Int_t dTow = 1);
		Float_t GetHCALIsoEnergy(Float_t x, Float_t y, Float_t Riso = 0.4);
		// Info things
		Int_t GetMinRing(Int_t segment, Float_t energy);
		Int_t GetMaxRing(Int_t segment, Float_t energy);
		Float_t GetWeight(Int_t segment, Int_t ring, Float_t energy);
		Float_t GetWeightCorrected(Int_t segment, Float_t dist, Float_t energy);
		Float_t GetAmplitude(Int_t segment, Int_t ring, Float_t totalE);
		AliFOCALCell * GetCell(Int_t seg, Int_t col, Int_t row);
		
		void FillClusterTree();
		void SaveImage(char * name);
		
		virtual const char *Version() const { return "clustering of FoCAL v2"; }
		
		void InitGeometry(const char *geometryfile);
		void InitGeometry();
		
		void InitParameters(const char *parameterFile);
		
		// Parameter file format version
		// FIXME: initialisations should be moved to cxx file
		static const Int_t kCurParFileFormat = 2;  // expect this parameter file format version
		static const Int_t kNPars = 14;

		// Parameter file array position definition

		// NB: changed 10 Dec 2014, added 3 par for profile
		static const Int_t parLocMinRad =  0;
		static const Int_t parLocMaxRad =  1;
		static const Int_t parLocSeedTh =  2;
		static const Int_t parLocClusTh =  3;
		static const Int_t parLocCelNTh =  4;
		static const Int_t parLocShapTh =  5;
		static const Int_t parLocWeigh1 =  6;
		static const Int_t parLocWeigh2 =  7;
		static const Int_t parLocWeigh3 =  8;
		static const Int_t parLocRejec1 =  9;
		static const Int_t parLocRejec2 =  10;
		static const Int_t parLocCaliSt =  11;
			
	protected:
		virtual void MakeClusters();
		
	private:
		/// Functions
		
		bool Neighboring_Cells(AliFOCALCell *twr1, AliFOCALCell *twr2);
		
		/// My Functions
	
		void AssignAmplitude(AliFOCALCell * c);
		Bool_t IsOverEnergyThreshold(Int_t segment, Float_t energy);
		void RejectCellAsSeed(AliFOCALCell * cell);
		void AssignCellAsSeed(AliFOCALCell * cell);
		
		/// Fields
		AliFOCALGeometry *fGeom;
        int fHCALSegID;
		float fCHI2Limit;
		int max_segment; /// variable with value of maximum segment "hitted" by particles in FoCal
		int fNumberOfClusters; // number of clusters found in EC section
		int fNumberOfClustersItr; // number of clusters found in EC section
		float fClusteringEnergyThreshold; // 
		float fLocalEnergyThreshold; // 
		float fDistance; // Variable to limit the distance to look clusters
		bool fSegLayer; // if fSegLayer == true it's saved the position of cluster of all segment, otherwise it's save 
					    // the cluster position per segment
		bool modeC;	// flag for computing coarse widths and cluster positions (log-energy weights if true, linear-energy weights if false)
		bool modeF;	// flag for computing coarse widths and cluster positions (power-law weights if true, linear-energy weights if false)
		bool modeCorrectGW;	// flag for computing which GetWeight function to be used: if true, GetWeight will use distances (new, corrected), else it will use rings (old)
		Float_t wC_0;	// parameter for computing log-energy coarse widths according to Terry's formyls, only used when modeC == true
		Float_t wF_0;	// parameter for computing log-energy coarse widths according to Terry's formyls, only used when modeC == true
					    
    /// My Fields
    Float_t fPixelNoiseProb;   // Noise probability per pixel (digitizer)
    Float_t fPadNoiseSigma;    // Width of gaussian noise for pads (digitizer)

    AliFOCALCellMap * fCellMap;
    AliFOCALRinger * fRinger;
    TObjArray * fSeeds;
    Float_t * * fParameters;
    Bool_t fDebugMode;
		
	/// ROOT Stuff
	ClassDef(AliFOCALClusterizerv2,4);  // Clusterization algorithm class
} ;

#endif // AliFOCALLCLUSTERIZER_H
