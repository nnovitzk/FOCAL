#ifndef ObjectMap_H
#define ObjectMap_H

class ObjectMap : public TObject {

  public:
 
  ObjectMap();
  ObjectMap(Int_t cols, Int_t rows);
  ObjectMap& operator=(const ObjectMap &fMap);
  ~ObjectMap();
  
  void CreateMap(Int_t nCol, Int_t nRow);
  void ResetMap();
  TObject * Get(Int_t col, Int_t row);
  Bool_t InsertAt(Int_t col, Int_t row, TObject * obj);
 
  Int_t fRows;
  Int_t fCols;
  TObject * * fObjectMap;
  
  ClassDef(ObjectMap,1);
};

#endif // ObjectMap_H
