#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  AliFOCALdigit+;
#pragma link C++ class  AliFOCALsdigit+;
#pragma link C++ class  AliFOCALhit+;
#pragma link C++ class  AliFOCALGeometry+;
#pragma link C++ class  AliFOCALv0+;
#pragma link C++ class  AliPIPEFOCAL+;

#pragma link C++ class  AliFOCALClusterizer+;
#pragma link C++ class  AliFOCALClusterizerv1+;
#pragma link C++ class  AliFOCALClusterizerv2+;
#pragma link C++ class  AliFOCALPad+;
#pragma link C++ class  AliFOCALTower+;
#pragma link C++ class  AliFOCALCluster+;
#pragma link C++ class  AliFOCALStrip+;
#pragma link C++ class  AliFOCALKinematics+;

#endif
