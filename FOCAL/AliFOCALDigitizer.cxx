/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//  Source File : AliFOCALDigitizer.cxx, Version 00         //
//                                                     //
//  Date   : August 03 2009                           //
//  Written by Taku Gunji
//                                                     //
//-----------------------------------------------------//

#include <Riostream.h>
#include <TBRIK.h>
#include <TNode.h>
#include <TTree.h>
#include <TGeometry.h>
#include <TObjArray.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TParticle.h>
#include <TRandom.h>
#include <TMath.h>


#include "AliLog.h"
#include "AliRun.h"
#include "AliHit.h"
#include "AliDetector.h"
#include "AliRunLoader.h"
#include "AliLoader.h"
#include "AliConfig.h"
#include "AliMagF.h"
#include "AliDigitizer.h"
#include "AliHeader.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBEntry.h"
#include "AliMC.h"

#include "AliFOCAL.h"
#include "AliFOCALhit.h"
#include "AliFOCALDigitizer.h"
#include "AliFOCALsdigit.h"
#include "AliFOCALPedestal.h"
#include "AliFOCALCalibData.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALdigit.h"


ClassImp(AliFOCALDigitizer)

using namespace std;

AliFOCALDigitizer::AliFOCALDigitizer() :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fNSeg(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0)
{
  AliDebug(1,"AliFOCALDigitizer's default contructor called, initialization necessary.");
}
//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, Option_t *option) :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fNSeg(0),
  fPixelNoiseProb(0),
  fPadNoiseSigma(0)
{
  Initialize(pathToGalice, geometryFile, option);
}
//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, const char * outputFile, Option_t *option) :
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fNSeg(0),
  fPixelNoiseProb(0),
  fPadNoiseSigma(0)
{
  InitializeLocal(pathToGalice, geometryFile, outputFile, option);
}
//____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(const AliFOCALDigitizer& digitizer):
  AliDigitizer(digitizer),
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fNSeg(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0)
{
  // copy constructor
  AliError("Copy constructor not allowed ");
  //geom = new AliFOCALGeometry();
  //geom->Init();  

}
//____________________________________________________________________________
AliFOCALDigitizer & AliFOCALDigitizer::operator=(const AliFOCALDigitizer& /*digitizer*/)
{
  // Assignment operator
  AliError("Assignement operator not allowed ");
  //geom = new AliFOCALGeometry();
  //  geom->Init();

  return *this;
}
////____________________________________________________________________________
AliFOCALDigitizer::AliFOCALDigitizer(AliDigitizationInput* manager):
  AliDigitizer(manager,"AliFOCALDigitizer","AliFOCALDigitizer"),
  fRunLoader(0),
  fFOCAL(0),
  fFOCALLoader(0),
  fGeom(0),
  fDoDigits(false),
  fDoSDigits(false),
  fLocalMode(false),
  fSDigits(0),
  fDigits(0),
  fSegmentMap(0),
  fSSegmentMap(0),
  fOutputFile(0),
  fTreeS(0),
  fTreeD(0),
  fNSDigit(0),
  fNDigit(0),
  fNSeg(0),
  fPixelNoiseProb(0.),
  fPadNoiseSigma(0.)
{
//  fDigInput = manager;
}

//____________________________________________________________________________
AliFOCALDigitizer::~AliFOCALDigitizer()
{

  cout << "~AliFOCALDigitzer called" << endl;
  // Default Destructor
  //
  if (fSDigits) {
    fSDigits->Clear();
    delete fSDigits;
    fSDigits=0;
  }

  if (fDigits) {
    fDigits->Clear();
    delete fDigits;
    fDigits=0;
  }

  if( fGeom ){
    delete fGeom;
    fGeom = 0;
  }

  if (fSegmentMap) {
    delete [] fSegmentMap;
  }

  if (fSSegmentMap)
    delete [] fSSegmentMap;
    
  

  if (fOutputFile) {
    fOutputFile->Write();
    fOutputFile->Close();
    delete fOutputFile;
  }
//  if (fTreeD)
//    delete fTreeD;
//  if (fTreeS)
//    delete fTreeS;
  

  if (fFOCAL)
    delete fFOCAL;

  if (fFOCALLoader)
    delete fFOCALLoader;

  if (fRunLoader)
    delete fRunLoader;
  cout << "~AliFOCALDigitzer finished" << endl;
}
//
// Member functions
//

//____________________________________________________________________________
void AliFOCALDigitizer::Digitize(Option_t* /*option*/) {
  AliDebug(1,"AliFOCALDigitizer::Digitize called with option.");
}

void AliFOCALDigitizer::Initialize(Option_t * option) {

  SetOption(option);
  
  if (!fGeom) {
    AliError("AliFOCALDigitizer::Initialize(Option * option): You need to set geometry before calling this method!");
    return;
  }
  
  InitializeArraysAndMaps();
}

//____________________________________________________________________________
void AliFOCALDigitizer::Initialize(const char * pathToGalice, const char * geometryFile, Option_t *option)
{
  // Loads galice.root file and corresponding header, kinematics
  // hits and sdigits or digits depending on the option
  //
  
  if (fRunLoader) {
    AliError("AliFOCALDigitizer::InitializeLocal: Already initialized!");
    return;
  }

  TString evfoldname = AliConfig::GetDefaultEventFolderName();
  fRunLoader = AliRunLoader::GetRunLoader(evfoldname);
  if (!fRunLoader)
      fRunLoader = AliRunLoader::Open(pathToGalice,AliConfig::GetDefaultEventFolderName(), "UPDATE");
  
  if (!fRunLoader)
   {
     AliError(Form("Can not open session for file %s.",pathToGalice));
   }

  SetOption(option);
  
  if(fDoDigits || fDoSDigits)
  {
    if (!fRunLoader->GetAliRun()) fRunLoader->LoadgAlice();
    if (!fRunLoader->TreeE()) fRunLoader->LoadHeader();
    if (!fRunLoader->TreeK()) fRunLoader->LoadKinematics();
  
    gAlice = fRunLoader->GetAliRun();
  
    if (gAlice)
    {
      AliDebug(1,"Alirun object found");
    }
    else
	{
	  AliError("Could not found Alirun object");
	}
  
    fFOCAL = (AliFOCAL*)gAlice->GetDetector("FOCAL");
    fGeom = AliFOCALGeometry::GetInstance(geometryFile);
  }

  fFOCALLoader = fRunLoader->GetLoader("FOCALLoader");
  if (fFOCALLoader == 0x0)
  {
    AliError("Can not find FOCALLoader");
  }

  if (fDoDigits || fDoSDigits) {
    fFOCALLoader->LoadHits("READ");
  }

  if (fDoSDigits)
  {
    fFOCALLoader->LoadSDigits("recreate");
  }
  if (fDoDigits)
  {
    fFOCALLoader->LoadDigits("recreate");
  }
  
  fLocalMode = false;
  
  InitializeArraysAndMaps();
}
//____________________________________________________________________________
void AliFOCALDigitizer::InitializeLocal(const char * pathToGalice, const char * geometryFile, const char * outputFile, Option_t *option)
{
  // Loads galice.root file and corresponding header, kinematics
  // hits and sdigits or digits depending on the option
  //
  if (fRunLoader) {
    AliError("AliFOCALDigitizer::InitializeLocal: Already initialized!");
    return;
  }
  
  fRunLoader = AliRunLoader::Open(pathToGalice);
  
  if (!fRunLoader)
  {
    AliError(Form("Can not open session for file %s.",pathToGalice));
  }

  SetOption(option);
  
  if(fDoDigits || fDoSDigits)
  {
    if (!fRunLoader->GetAliRun()) fRunLoader->LoadgAlice();
    if (!fRunLoader->TreeE()) fRunLoader->LoadHeader();
    if (!fRunLoader->TreeK()) fRunLoader->LoadKinematics();
  
    gAlice = fRunLoader->GetAliRun();
  
    if (gAlice)
    {
      AliDebug(1,"Alirun object found");
    }
    else
	{
	  AliError("Could not found Alirun object");
	}
  
    fFOCAL = (AliFOCAL*)gAlice->GetDetector("FOCAL");
    fGeom = AliFOCALGeometry::GetInstance(geometryFile);
  }

  fFOCALLoader = fRunLoader->GetLoader("FOCALLoader");
  if (fFOCALLoader == 0x0)
  {
    AliError("Can not find FOCALLoader");
  }

  if (fDoDigits || fDoSDigits) {
    fFOCALLoader->LoadHits("READ");
    fOutputFile = new TFile(outputFile,"RECREATE");
  }

// Will be done per folder
//  if (fDoSDigits)
//  {
//    fOutputFile->cd();
//    fTreeS = new TTree("TreeS","TreeS");
//  }
//  if (fDoDigits)
//  {
//    fOutputFile->cd();
//    fTreeD = new TTree("TreeD","TreeD");
//  }
  
  fLocalMode = true;
  
  InitializeArraysAndMaps();
  
//  if (fDoDigits)
//    cout << "HD GOGOGO" << endl;
//  else 
//    cout << "HD NONONO" << endl;
//  if (fDoSDigits)
//    cout << "HS GOGOGO" << endl;
//  else 
//    cout << "HS NONONO" << endl;
}

//____________________________________________________________________________
void AliFOCALDigitizer::InitializeArraysAndMaps() {
  
  // Get number of collumns, rows and segments
  fNSeg = GetNSeg();
  
  // Initialize SDigits if needed
  if (fDoSDigits) {
    
    if (fSDigits || fSSegmentMap) {
        AliDebug(1,"SDigit objects not empty, please clear them before calling init");
        return;
    }
    cout << "Initializing fSDigits" << endl;
    fSDigits = new TClonesArray("AliFOCALdigit", 1000);
    fNSDigit = 0;
    
    fSSegmentMap = new AliFOCALSegmentMap [fNSeg];
    Int_t cols,rows;
    for (Int_t i = 0; i < fNSeg ; i++) {
      cout << "\tsegment " << i << endl;
      fGeom->GetVirtualNColRow(i,cols,rows);
      fSSegmentMap[i].CreateMap(i,cols,rows);
    } 
  }
  
  // Initialize Digits if needed
  if (fDoDigits) {
    
    if (fDigits || fSegmentMap) {
        AliDebug(1,"Digit objects not empty, please clear them before calling init");
        return;
    }
    cout << "Initializing fDigits" << endl;
    fDigits = new TClonesArray("AliFOCALdigit", 1000);
    fNDigit = 0;
    
    fSegmentMap = new AliFOCALSegmentMap [fNSeg];
    Int_t cols,rows;
    for (Int_t i = 0; i < fNSeg ; i++) {
      fGeom->GetVirtualNColRow(i,cols,rows);
      cout << "Making segment maps; segment " << i << " cols " << cols << " " << rows << endl;
      fSegmentMap[i].CreateMap(i,cols,rows);
    } 
  }
}

//____________________________________________________________________________
AliFOCALdigit * AliFOCALDigitizer::GetDigit(Int_t col, Int_t row, Int_t seg) {
  
//  if (!fDigits || !fDigitMap){
//    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: not initialized));
//    return 0;
//  }

  Int_t nCol, nRow;
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fNSeg) || (col < 0) || (row < 0) || (seg < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: indexes %i,%i,%i out of bounds",col,row,seg));
    return 0;
  }
  
  Long_t index = row*nCol + col;
  if (!fSegmentMap[seg].fDigitMap[index]) {
    new((*fDigits)[fNDigit]) AliFOCALdigit(col,row,seg,0);
    fSegmentMap[seg].fDigitMap[index] = (AliFOCALdigit*)fDigits->AddrAt(fNDigit);
//    cout << Form("Creating at %i",fNDigit) << endl;
    fNDigit++;
  }    
  return fSegmentMap[seg].fDigitMap[index];
}

//____________________________________________________________________________
AliFOCALdigit * AliFOCALDigitizer::GetSDigit(Int_t col, Int_t row, Int_t seg) {

//  if (!fSDigits || !fSDigitMap){
//    AliDebug(1,Form("AliFOCALDigitizer::GetSDigit: not initialized));
//    return 0;
//  }
  
//  cout << "GetSDigit called " << col << "," << row << "," << seg << endl;
  
  Int_t nCol, nRow;
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fNSeg) || (col < 0) || (row < 0) || (seg < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: indexes %i,%i,%i out of bounds",col,row,seg));
    return 0;
  }
  
  Long_t index = row*nCol + col;
  if (!(fSSegmentMap[seg].fDigitMap[index])) {
//    cout << "Creating new digit: " << col << "," << row << "," << seg << endl;
    new((*fSDigits)[fNSDigit]) AliFOCALdigit(col,row,seg,0);
    fSSegmentMap[seg].fDigitMap[index] = (AliFOCALdigit*)fSDigits->AddrAt(fNSDigit);
//    cout << Form("SCreating at %i",fNSDigit) << endl;
    fNSDigit++;
  }    
  return fSSegmentMap[seg].fDigitMap[index];
}

// Remove Digit from the map
Bool_t AliFOCALDigitizer::RemoveDigit(AliFOCALdigit * digit) {
  
  if (!digit)
    return false;
  
  Int_t nCol, nRow, col, row, seg;
  col = digit->GetCol();
  row = digit->GetRow();
  seg = digit->GetSegment();
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fNSeg) || (col < 0) || (row < 0) || (seg < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveDigit: Digit at %i,%i,%i is out of bounds",col,row,seg));
    return false;
  }
  
  Long_t index = row*nCol + col;
  if (!fSegmentMap[seg].fDigitMap[index]) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveDigit: Digit at %i,%i,%i is not in the map",col,row,seg));
    return false;
  } else {
    fSegmentMap[seg].fDigitMap[index] = 0;
    return true;
  }
}

// Remove Digit from the map
Bool_t AliFOCALDigitizer::RemoveSDigit(AliFOCALdigit * digit) {
  
  if (!digit)
    return false;
  
  Int_t nCol, nRow, col, row, seg;
  col = digit->GetCol();
  row = digit->GetRow();
  seg = digit->GetSegment();
  fGeom->GetVirtualNColRow(seg,nCol,nRow);
  
  if ( (col >= nCol) || (row >= nRow) || (seg >= fNSeg) || (col < 0) || (row < 0) || (seg < 0) ) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveSDigit: Digit at %i,%i,%i is out of bounds",col,row,seg));
    return false;
  }
  
  Long_t index = row*nCol + col;
  if (!fSSegmentMap[seg].fDigitMap[index]) {
    AliDebug(1,Form("AliFOCALDigitizer::RemoveSDigit: Digit at %i,%i,%i is not in the map",col,row,seg));
    return false;
  } else {
    fSSegmentMap[seg].fDigitMap[index] = 0;
    return true;
  }
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::EnergyToAmplitude(Float_t energy, Int_t segment) {
  //
  // Convert deposited energy to signal units in Digits
  //
  // Includes noise for Pads; noise for pixels is generated elsewhere
  //
  bool isPixel = fGeom->GetVirtualIsPixel(segment);
  Float_t relThickness = fGeom->GetVirtualRelativeSensitiveThickness(segment);
  Float_t pixelTreshold = fGeom->GetVirtualPixelTreshold(segment);
  Float_t rescaledEnergy;
    
  // Rescaling to correct thickness of sensitive materital
  // Fluctuations TODO!!!
  rescaledEnergy = energy * relThickness;
  
  // Currently returns energy in keV
  if (!isPixel) {
    rescaledEnergy += gRandom->Gaus(0, fPadNoiseSigma);  // Add noise
    if (rescaledEnergy >= pixelTreshold)
      return (Int_t)(rescaledEnergy/1000);
    else
      return 0;
  }
  else {
    if (rescaledEnergy >= pixelTreshold) 
      return 1;
    return 0;
  }
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::EnergyToSAmplitude(Float_t energy, Int_t segment) {
  
  bool isPixel = fGeom->GetVirtualIsPixel(segment);
  Float_t relThickness = fGeom->GetVirtualRelativeSensitiveThickness(segment);
  Float_t pixelTreshold = fGeom->GetVirtualPixelTreshold(segment);
  Float_t rescaledEnergy;
    
  // Rescaling to correct thickness of sensitive materital
  // Fluctuations TODO!!!
  rescaledEnergy = energy * relThickness;
  
  // Currently returns energy in keV
  if (!isPixel)
    return (Int_t)(rescaledEnergy/1000);
  else
    return (rescaledEnergy >= pixelTreshold) ? 1 : 0;
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2Digits(TBranch * branchH) {

  cout << "AliFOCALDigitizer::Hits2Digits called." << endl;
  
  if (!branchH) {
    AliError("AliFOCALDigitizer::Hits2Digits: branchH == 0, returning!");
    return;
  }
    
  Hits2DigitsPadsPixelsOrEmbedding(branchH, 0);
  
//  TClonesArray* hits = 0;
//  Int_t ntracks = branchH->GetEntries();
////  cout << "    Event has " << ntracks << " tracks." << endl;
//  branchH->SetAddress(&hits);
////  cout << hits << endl;

//  // loop over 'tracks'
//  for (Int_t track=0; track<ntracks;track++)
//  {
//    branchH->GetEntry(track);
//	  {
//	    int nfocal = hits->GetEntries();
////	    cout << "      Track has " << nfocal << " hits." << endl;
//	    // loop over hits
//	    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++)
//      {
////        cout << "\t\tHit: " << ifocal << endl;
//        
//        AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits->UncheckedAt(ifocal);
//        
//        Float_t x, y, z;  
//        x = (Float_t) fFOCALHit->X(); 
//        y = (Float_t) fFOCALHit->Y(); 
//        z = (Float_t) fFOCALHit->Z();

//        // deposited energy in the pixel in GeV
//        Float_t energy = fFOCALHit->GetEnergy();
//        
//        Int_t col,row,seg;
//        if (!XYZ2ColRowSeg(x,y,z,col,row,seg)) {
////          AliDebug(1,Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z));
////          cout << Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z) << endl;
//          continue;
//        }
//        
//        AliFOCALdigit * digit;
//        
//        if (fDoDigits) {
////          cout << "digit" << endl;
//          digit = GetDigit(col,row,seg);
//          if (digit)
//            digit->IncreaseAmp(EnergyToAmplitude(energy,seg));
//        }
//        
//        if (fDoSDigits) {
//          digit = GetSDigit(col,row,seg);
//          if (digit) {
//            digit->IncreaseAmp(EnergyToSAmplitude(energy,seg));
////            if (row < 10)
////              cout << Form("SDigit: %i, %i, %i",col,row,seg) << endl;
//          }
//        }
//        
//      } // Hits Loop ended
//	  }
//  } // Track Loop ended
//  delete hits;
}

//____________________________________________________________________________
void AliFOCALDigitizer::Hits2DigitsEvent(Int_t ievt) {

  cout << "Hits2Digits called on event " << ievt << endl;

  // load event
  fRunLoader->GetEvent(ievt);
  TTree* treeH = fFOCALLoader->TreeH();
  
  Int_t ntracks    = (Int_t) treeH->GetEntries();
  AliDebug(1,Form("Number of Tracks in the TreeH = %d", ntracks));
  
  if ( fLocalMode && ( fDoDigits || fDoSDigits )) {
     fOutputFile->cd();
     gDirectory->mkdir(Form("Event%d",ievt));
  }

  // initialize fDigits and corresponding maps/TTrees...
  if (fDoDigits) {
    
    if (!fLocalMode) {
    
      fTreeD = fFOCALLoader->TreeD();
      if (fTreeD == 0x0)
      {
        fFOCALLoader->MakeTree("D");
        fTreeD = fFOCALLoader->TreeD();
      }
    } else {
      fOutputFile->cd();
      gDirectory->cd(Form("Event%d",ievt));
      fTreeD = new TTree("TreeD","TreeD");
    } 
    fTreeD->Branch("FOCAL", fDigits, 16000);
    
  }  
  // initialize fSDigits and corresponding maps/TTrees...
  if (fDoSDigits) {
    
    if (!fLocalMode) {
    
      fTreeS = fFOCALLoader->TreeS();
      if (fTreeS == 0x0)
      {
        fFOCALLoader->MakeTree("S");
        fTreeS = fFOCALLoader->TreeS();
      }
    } else {
      fOutputFile->cd();
      gDirectory->cd(Form("Event%d",ievt));
      fTreeS = new TTree("TreeS","TreeS");
    } 
    fTreeS->Branch("FOCAL", fSDigits, 16000);
    
  }  
  // load hits
  Hits2Digits(treeH->GetBranch("FOCAL"));
  
  // store
  

  
}

// General method for embedding, calls embedding for Pixels/Pads as necessary
void AliFOCALDigitizer::Hits2DigitsEmbedding(TBranch * branchHits, TBranch * branchBackground) {

  cout << "AliFOCALDigitizer::Hits2DigitsEmbedding called." << endl;
  
  if ((!branchHits) || (!branchBackground)) {
    AliError("AliFOCALDigitizer::Hits2Digits: branchHits == 0 or branchBackground == 0, returning!");
    return;
  }
  
  Hits2DigitsPadsPixelsOrEmbedding(branchHits, branchBackground);
}

// Embedding for Pixels
void AliFOCALDigitizer::Hits2DigitsPadsPixelsOrEmbedding(TBranch * branchHits, TBranch * branchBackground) {

  // Initialize
  cout << "AliFOCALDigitizer::Hits2DigitsPadsPixelsOrEmbedding called." << endl;
  
  if (fDoSDigits)
    ResetSDigit();
  if (fDoDigits)
    ResetDigit();
  
  // Load branches into TCloneArrays
  TClonesArray* hits = 0;
  Int_t nTracksHits = branchHits->GetEntries();
  cout << "    Signal Event has " << nTracksHits << " tracks." << endl;
  branchHits->SetAddress(&hits);
  
  TClonesArray* embedded = new TClonesArray("AliFOCALhit",1000);
  
  // Load All hits into 'embedded' TClonesArray
  
  cout << "Summing signal" << endl;
  for (Int_t track=0; track<nTracksHits;track++)
  {
    //    cout << "Hits track " << track << endl;
    branchHits->GetEntry(track); // load track
    //    cout << Form("Hits: Entries = %i, Last = %i, Size = %i, Empty? = %o, OnHeap? = %o, IsOwner? = %o",
    //    		  hits->GetEntries(), hits->GetLast(), hits->GetSize(), hits->IsEmpty(), hits->IsOnHeap(), hits->IsOwner())
    //    				  << endl;
    // Sort out hits from 'pad' segmets
    {
		  int nfocal = hits->GetEntries();
		  //		  	    cout << "      Track has " << nfocal << " hits." << endl;
		  // loop over hits
		  for (Int_t ifocal = 0; ifocal < nfocal; ifocal++)
		  {
		    AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits->UncheckedAt(ifocal);

		    Float_t x, y, z;
		    x = (Float_t) fFOCALHit->X();
		    y = (Float_t) fFOCALHit->Y();
		    z = (Float_t) fFOCALHit->Z();
        
        Int_t col,row,seg;
		    if (!XYZ2ColRowSeg(x,y,z,col,row,seg)) {
		      //          AliDebug(1,Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z));
		      //        cout << Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z) << endl;
		      hits->RemoveAt(ifocal);
		      continue;
		    }
		    
		    if (fGeom->GetVirtualIsPixel(seg))
		      continue;

		    // deposited energy in the pixel in GeV
		    Float_t energy = fFOCALHit->GetEnergy();

		    

		    AliFOCALdigit * digit;

		    if (fDoDigits) {
		      //		      cout << Form("Digit called, track %i, hit %i", track, ifocal) << endl;
		      digit = GetDigit(col,row,seg);
		      if (digit)
			      digit->IncreaseDepEn(energy);
		    }

		    if (fDoSDigits) {
		      digit = GetSDigit(col,row,seg);
		      if (digit) {
			      digit->IncreaseDepEn(energy);
		    //            if (row < 10)
			      //		                  cout << Form("SDigit: %i, %i, %i",col,row,seg) << endl;
		      }
		    }
		    hits->RemoveAt(ifocal);

		  } // Hits Loop ended
	  }
	  hits->Compress();
    embedded->AbsorbObjects(hits,0,hits->GetLast()); // add all the hits to embedded tclonesarray
//    delete hits; // sometimes causes segfault, not sure why
  }
  //  cout << Form("Hits: Entries = %i, Last = %i, Size = %i, Empty? = %o, OnHeap? = %o, IsOwner? = %o",
  //		  hits->GetEntries(), hits->GetLast(), hits->GetSize(), hits->IsEmpty(), hits->IsOnHeap(), hits->IsOwner())
  //				  << endl;
  cout << "Deleting hits" << endl;
  delete hits;
  
  if (branchBackground) {
  
    TClonesArray* background = 0;
    Int_t nTracksBackground = branchBackground->GetEntries();
    cout << "    Background Event has " << nTracksBackground << " tracks." << endl;
    branchBackground->SetAddress(&background);
    
    cout << "Summing background" << endl;
    for (Int_t track=0; track<nTracksBackground;track++)
    {
  //    cout << "Background track " << track << endl;
      branchBackground->GetEntry(track); // load track
      // Sort out hits from 'pad' segmets
      {
		    int nfocal = background->GetEntries();
		    //	    cout << "      Track has " << nfocal << " hits." << endl;
		    // loop over hits
		    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++)
		    {
		      AliFOCALhit * fFOCALHit = (AliFOCALhit*) background->UncheckedAt(ifocal);

		      Float_t x, y, z;
		      x = (Float_t) fFOCALHit->X();
		      y = (Float_t) fFOCALHit->Y();
		      z = (Float_t) fFOCALHit->Z();

          Int_t col,row,seg;
		      if (!XYZ2ColRowSeg(x,y,z,col,row,seg)) {
		      //          AliDebug(1,Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z));
		      //          cout << Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z) << endl;
		        background->RemoveAt(ifocal);
		        continue;
		      }

		      if (fGeom->GetVirtualIsPixel(seg))
		        continue;

		      // deposited energy in the pixel in GeV
		      Float_t energy = fFOCALHit->GetEnergy();

		      

		      AliFOCALdigit * digit;

		      if (fDoDigits) {
		      //          cout << "digit" << endl;
		        digit = GetDigit(col,row,seg);
		        if (digit)
			        digit->IncreaseDepEn(energy);
		      }

		      if (fDoSDigits) {
		        digit = GetSDigit(col,row,seg);
		        if (digit) {
			        digit->IncreaseDepEn(energy);
		      //            if (row < 10)
		      //              cout << Form("SDigit: %i, %i, %i",col,row,seg) << endl;
		        }
		      }
		      background->RemoveAt(ifocal);

		    } // Hits Loop ended
	    }
	    background->Compress();
      embedded->AbsorbObjects(background,0,background->GetLast()); // add all the hits to embedded tclonesarray
  //    delete background; // sometimes causes segfault, not sure why
    }
    cout << "Deleting background" << endl;
    delete background;
  }
  cout << "Done combining hits" << endl;
  
  // All pad digits are summed, we now need to apply thresholds and calculate output signal
  if (fDoDigits) {
	  for (Int_t d = 0; d < fNDigit; d++) {
	    AliFOCALdigit * dig = (AliFOCALdigit*) fDigits->UncheckedAt(d);
	    
	    Int_t signal = EnergyToAmplitude(dig->GetDepEn(),dig->GetSegment());
	    if (signal > 0) {
	      dig->SetAmp(signal);
//	      cout << "Keeping " << signal << endl;
	    } else {
	      // Digit would have zero signal
	      // Remove it from the map:
//	      cout << Form("Deleting at %i with signal %i",d,signal) << endl;
	      RemoveDigit(dig);
	      // Remove it from the list;
	      fDigits->RemoveAt(d);
//	      delete dig;
	    }
	  }
	  fDigits->Compress();
	  fNDigit = fDigits->GetEntries();
  }
  // All pad sdigits are summed, we now need to apply thresholds and calculate output signal
  if (fDoSDigits) {
    for (Int_t d = 0; d < fNSDigit; d++) {
	    AliFOCALdigit * dig = (AliFOCALdigit*) fSDigits->UncheckedAt(d);
	    
	    Int_t signal = EnergyToSAmplitude(dig->GetDepEn(),dig->GetSegment());
	    if (signal > 0) {
	      dig->SetAmp(signal);
//	      cout << "Keeping " << signal << endl;
	    } else {
	      // Digit would have zero signal
	      // Remove it from the map:
//	      cout << Form("Deleting at %i with signal %i",d,signal) << endl;
	      RemoveSDigit(dig);
	      // Remove it from the list;
	      fSDigits->RemoveAt(d);
//	      delete dig;
	    }
	  }
	  fSDigits->Compress();
	  fNSDigit = fSDigits->GetEntries();
  }
  
//  cout << fDigits->GetEntries() << endl;
//  for (Int_t d = 0; d < fDigits->GetEntries(); d++) {
//    AliFOCALdigit * dig = (AliFOCALdigit *) fDigits->UncheckedAt(d);
//    cout << Form("\tDigit: at=%i\tseg=%i\tcol=%i\trow=%i\tapm=%i",d,dig->GetSegment(), dig->GetCol(), dig->GetRow(), dig->GetAmp()) << endl;
//  }
  
  Int_t padDigits = fNDigit;
  Int_t padSDigits = fNSDigit;
  
  // Assing indexes to all hits;
  
    // needed variables:
  Float_t XSize = fGeom->GetFOCALSizeX();
  Float_t YSize = fGeom->GetFOCALSizeY();
//  Float_t ZSize = fGeom->GetFOCALSizeZ();
//  Float_t Z0    = fGeom->GetFOCALZ0() - ZSize/2;
  
  Int_t XTot = Int_t(XSize/fGeom->GetGlobalPixelSize() + 0.001);
  Int_t YTot = Int_t(YSize/fGeom->GetGlobalPixelSize() + 0.001);
//  Int_t ZTot = fGeom->GetNumberOfLayers();
  
  cout << "Assigning indexes" << endl;
    // index calculation:
  Int_t embeddedEntries = embedded->GetEntries();
  AliFOCALhit * hit;
  for (Int_t i = 0; i < embeddedEntries; i++) {
    hit = (AliFOCALhit*) embedded->UncheckedAt(i);
    
    Int_t segment = fGeom->GetVirtualSegment(hit->Z());
    if (segment < 0) {
      embedded->Remove(hit);
      continue;
    }  
    
    Int_t x = (Int_t) ((hit->X() + XSize/2)/XSize*XTot);
    Int_t y = (Int_t) ((hit->Y() + YSize/2)/YSize*YTot);
//    Int_t z = ((hit->Z() - Z0)/ZSize)*ZTot;
    
    hit->SetIndex((ULong64_t)((ULong64_t)(segment)*(ULong64_t)(XTot)*(ULong64_t)(YTot) + (ULong64_t)(x)*(ULong64_t)(YTot) + (ULong64_t)(y)));
//    cout << Form("Hit: %i\tAssigned Index = %lu, Segment = %i, Column = %i, Row = %i", i, hit->fIndex, segment, x, y) << endl;
  }
  
  // Sort Hits according to the indexes
  cout << "Sorting hits" << endl;
  embedded->Compress();
  embedded->Sort();
  
  Int_t curSegment = -1;
  Int_t curPixCol  = -1;
  Int_t curPixRow  = -1;
  
  Int_t newSegment = -1;
  ULong64_t newLayerIndex = 0;
  Int_t newPixCol  = -1;
  Int_t newPixRow  = -1;
  
  Float_t energy = 0;
  AliFOCALdigit * digit;
  Float_t pixelPadRatio;
  
  cout << "Starting walkthrough" << endl;
  
  // Walkthrough the hits to divide them into pixels and macro-pixels
  embeddedEntries = embedded->GetEntries();
  for (Int_t i = 0; i < embeddedEntries; i++) {
    
    hit = (AliFOCALhit*) embedded->UncheckedAt(i);
    
    newSegment = (Int_t)(hit->GetIndex() / ((ULong64_t)(XTot)*(ULong64_t)(YTot)));
    newLayerIndex = hit->GetIndex() % ((ULong64_t)(XTot)*(ULong64_t)(YTot));
    newPixCol = (Int_t)(newLayerIndex / (ULong64_t)(YTot));
    newPixRow = (Int_t)(newLayerIndex % (ULong64_t)(YTot));
    
    // cout << Form("Hit: %i\tIndex = %lu, Segment = %i, Column = %i, Row = %i", i, hit->GetIndex(), newSegment, newPixCol, newPixRow) << endl;
    
    if ((newPixRow != curPixRow) || (newPixCol != curPixCol) || (newSegment != curSegment)) {
    
      // Skip first non-existent "pixel"
      if (curSegment != -1) {
      
        // Calculate "pixels per pad" ratio:
        pixelPadRatio = fGeom->GetVirtualPadSize(curSegment)/fGeom->GetGlobalPixelSize();
//        cout << "Ratio = " << pixelPadRatio << endl;
        
        if (fDoDigits) {
          digit = GetDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curSegment);
          if (digit)
            digit->IncreaseAmp(EnergyToAmplitude(energy,curSegment));
        }
        
        if (fDoSDigits) {
          digit = GetSDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curSegment);
//          cout << Form("SDigit: %i, %i, %i",(Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curSegment) << endl;
          if (digit) {
            digit->IncreaseAmp(EnergyToSAmplitude(energy,curSegment));
//            cout << Form("SDigit: %i, %i, %i",(Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curSegment) << endl;
          }
        }
      }
      
      curPixCol = newPixCol;
      curPixRow = newPixRow;
      curSegment = newSegment;
      energy = hit->GetEnergy();
    } 
    else
      energy += hit->GetEnergy();
  }
  
  if (curSegment != -1) {
    cout << "Creating last digit" << endl;
    
    // Last digit:
        
    // Calculate "pixels per pad" ratio:
  //  cout << curSegment << endl;
    pixelPadRatio = fGeom->GetVirtualPadSize(curSegment)/fGeom->GetGlobalPixelSize();
  //  cout << 1 << endl;
    if (fDoDigits) {
      digit = GetDigit(Int_t(curPixCol/pixelPadRatio),Int_t(curPixRow/pixelPadRatio),curSegment);
      if (digit)
        digit->IncreaseAmp(EnergyToAmplitude(energy,curSegment));
    }
  //  cout << 1 << endl;
    if (fDoSDigits) {
      digit = GetSDigit((Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curSegment);
  //    cout << Form("SDigit: %i, %i, %i",(Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curSegment) << endl;
      if (digit) {
        digit->IncreaseAmp(EnergyToSAmplitude(energy,curSegment));
  //      cout << Form("SDigit: %i, %i, %i",(Int_t)(curPixCol/pixelPadRatio),(Int_t)(curPixRow/pixelPadRatio),curSegment) << endl;
      }
    }
  }

  // Add pixel noise

  if (fDoDigits) {
    cout << "Add pixel noise" << endl;
    for (Int_t iseg = 0; iseg < GetNSeg(); iseg++) {
      if (fGeom->GetVirtualIsPixel(iseg)) {
	// cout << "XTot " << XTot << " Pixel Size: " << fGeom->GetGlobalPixelSize() 
	//	     << " Pad size: " << fGeom->GetVirtualPadSize(iseg) << endl;
	
	// pixels per macro-pixel
	pixelPadRatio = fGeom->GetVirtualPadSize(iseg)/fGeom->GetGlobalPixelSize();
	Int_t nPixTot = XTot * YTot;
	Int_t nPixNoise = gRandom->Poisson(fPixelNoiseProb * nPixTot);

	cout << "Creating " << nPixNoise << " noise hits in " << nPixTot << " pixels" << endl;
	for (Int_t iNoise = 0 ; iNoise < nPixNoise; iNoise++) {
	  Int_t iCol = Int_t(gRandom->Rndm()*XTot);
	  Int_t iRow = Int_t(gRandom->Rndm()*YTot);
	  digit = GetDigit(Int_t(iCol/pixelPadRatio), Int_t(iRow/pixelPadRatio), iseg);
	  if (digit)
	    digit->IncreaseAmp(1);  // Add 1 noise hit
	}
      }
    }
  }
  cout << "Removing digits with zero signal" << endl;
  
  // Remove digits with 0 signal
  if (fDoDigits) {
    for (Int_t d = padDigits; d < fNDigit; d++) {
	    AliFOCALdigit * dig = (AliFOCALdigit*) fDigits->UncheckedAt(d);
	    
	    if (dig->GetAmp() <= 0) {
	      // Digit has zero signal
	      // Remove it from the map:
	      RemoveDigit(dig);
	      // Remove it from the list;
	      fDigits->RemoveAt(d);
	    }
	  }
	  fDigits->Compress();
	  fNDigit = fDigits->GetEntries();
  }
  // Remove sdigits with 0 signal
  if (fDoSDigits) {
    for (Int_t d = padSDigits; d < fNSDigit; d++) {
	    AliFOCALdigit * dig = (AliFOCALdigit*) fSDigits->UncheckedAt(d);
	    
	    if (dig->GetAmp() <= 0) {
	      // Digit has zero signal
	      // Remove it from the map:
	      RemoveSDigit(dig);
	      // Remove it from the list;
	      fSDigits->RemoveAt(d);
	    }
	  }
	  fSDigits->Compress();
	  fNSDigit = fSDigits->GetEntries();
  }
  
  cout << "Created ";
  
  if (fDoDigits)
    cout << fDigits->GetEntries();
  else if (fDoSDigits)
    cout << fSDigits->GetEntries();
    
  cout << " digits." << endl;
  
  cout << "Deleting arrays" << endl;
  
  delete embedded;

  cout << "Digitizing done" << endl;
}

// Embedding for Pads
void AliFOCALDigitizer::Hits2DigitsEmbeddingPads(TBranch * branchHits, TBranch * branchBackground) {

  cout << "AliFOCALDigitizer::Hits2DigitsEmbeddingPads called." << endl;
     
  TClonesArray* hits = 0;
  Int_t ntracks = branchHits->GetEntries();
  cout << "    Signal Event has " << ntracks << " tracks." << endl;
  branchHits->SetAddress(&hits);

  // loop over 'tracks' from Hits
  for (Int_t track=0; track<ntracks;track++)
  {
    branchHits->GetEntry(track);
	  {
	    int nfocal = hits->GetEntries();
//	    cout << "      Track has " << nfocal << " hits." << endl;
	    // loop over hits
	    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++)
      {
        AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits->UncheckedAt(ifocal);
        
        Float_t x, y, z;  
        x = (Float_t) fFOCALHit->X(); 
        y = (Float_t) fFOCALHit->Y(); 
        z = (Float_t) fFOCALHit->Z();

        // deposited energy in the pixel in GeV
        Float_t energy = fFOCALHit->GetEnergy();
        
        Int_t col,row,seg;
        if (!XYZ2ColRowSeg(x,y,z,col,row,seg)) {
//          AliDebug(1,Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z));
//          cout << Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z) << endl;
          continue;
        }
        
        AliFOCALdigit * digit;
        
        if (fDoDigits) {
//          cout << "digit" << endl;
          digit = GetDigit(col,row,seg);
          if (digit)
            digit->IncreaseAmp(EnergyToAmplitude(energy,seg));
        }
        
        if (fDoSDigits) {
          digit = GetSDigit(col,row,seg);
          if (digit) {
            digit->IncreaseAmp(EnergyToSAmplitude(energy,seg));
//            if (row < 10)
//              cout << Form("SDigit: %i, %i, %i",col,row,seg) << endl;
          }
        }
        
      } // Hits Loop ended
	  }
	  delete hits;
  } // Track Loop ended
  
  ntracks = branchBackground->GetEntries();
  cout << "    Background Event has " << ntracks << " tracks." << endl;
  TClonesArray* hits2 = 0;
  branchBackground->SetAddress(&hits2);
  
  // loop over 'tracks' from Background
  for (Int_t track=0; track<ntracks;track++)
  {
    branchBackground->GetEntry(track);
	  {
	    int nfocal = hits2->GetEntries();
//	    cout << "      Track has " << nfocal << " hits." << endl;
	    // loop over hits
	    for (Int_t ifocal = 0; ifocal < nfocal; ifocal++)
      {
        AliFOCALhit * fFOCALHit = (AliFOCALhit*) hits2->UncheckedAt(ifocal);
        
        Float_t x, y, z;  
        x = (Float_t) fFOCALHit->X(); 
        y = (Float_t) fFOCALHit->Y(); 
        z = (Float_t) fFOCALHit->Z();

        // deposited energy in the pixel in GeV
        Float_t energy = fFOCALHit->GetEnergy();
        
        Int_t col,row,seg;
        if (!XYZ2ColRowSeg(x,y,z,col,row,seg)) {
//          AliDebug(1,Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z));
//          cout << Form("Hit (%.1f,%.1f,%.1f) out of bounds",x,y,z) << endl;
          continue;
        }
        
        AliFOCALdigit * digit;
        
        if (fDoDigits) {
//          cout << "digit" << endl;
          digit = GetDigit(col,row,seg);
          if (digit)
            digit->IncreaseAmp(EnergyToAmplitude(energy,seg));
        }
        
        if (fDoSDigits) {
          digit = GetSDigit(col,row,seg);
          if (digit) {
            digit->IncreaseAmp(EnergyToSAmplitude(energy,seg));
//            if (row < 10)
//              cout << Form("SDigit: %i, %i, %i",col,row,seg) << endl;
          }
        }
        
      } // Hits Loop ended
	  }
	  delete hits2;
  } // Track Loop ended
}

//____________________________________________________________________________
void AliFOCALDigitizer::StoreDigits() {

  cout << "AliFOCALDigitizer::StoreDigits called" << endl;
  
  if (fDoDigits) {
  
    if (!fSDigits || !fSSegmentMap){
      AliDebug(1,Form("AliFOCALDigitizer::StoreDigit: not initialized"));
      return;
    }
   
    fTreeD->Fill();
    if (!fLocalMode) {
      fFOCALLoader->WriteDigits("OVERWRITE");
    } else {
//      fOutputFile->cd();
      fTreeD->Write();
      fOutputFile->Write();
      fTreeD->Delete();
    }
  }
  
  if (fDoSDigits) {
  
    if (!fSDigits || !fSSegmentMap){
      AliDebug(1,Form("AliFOCALDigitizer::StoreDigit: not initialized"));
      return;
    }
    
    fTreeS->Fill();
    if (!fLocalMode) {
      fFOCALLoader->WriteSDigits("OVERWRITE");
    } else {
//      fOutputFile->cd();
      fTreeS->Write();
      fOutputFile->Write();
      fTreeS->Delete();
    }
  }
}

//____________________________________________________________________________
// I'm not sure how this works.......
//void AliFOCALDigitizer::Exec(Option_t *option)
//{
//  // Does the event merging and digitization
//  const char *cdeb = strstr(option,"deb");
//  if(cdeb)
//    {
//      AliDebug(100," *** FOCAL Exec is called ***");
//    }

//  Int_t ninputs = fManager->GetNinputs();
//  AliDebug(1,Form("Number of files to be processed = %d",ninputs));
//  ResetCell();
//  ResetCellADC();

//  for (Int_t i = 0; i < ninputs; i++)
//    {
//      Int_t troffset = fManager->GetMask(i);
//      MergeSDigits(i, troffset);
//    }

//  fRunLoader = AliRunLoader::GetRunLoader(fManager->GetOutputFolderName());
//  fFOCAL  = (AliFOCAL*)gAlice->GetDetector("FOCAL");
//  fFOCALLoader = fRunLoader->GetLoader("FOCALLoader");
//  if (fFOCALLoader == 0x0)
//    {
//      AliError("Can not find FOCAL or FOCALLoader");
//    }
//  fFOCALLoader->LoadDigits("update");
//  TTree* treeD = fFOCALLoader->TreeD();
//  if (treeD == 0x0)
//    {
//      fFOCALLoader->MakeTree("D");
//      treeD = fFOCALLoader->TreeD();
//    }
//  Int_t bufsize = 16000;
//  if (!fDigits) fDigits = new TClonesArray("AliFOCALdigit", 1000);
//  treeD->Branch("FOCALDigit", &fDigits, bufsize);
//  

//  Float_t deltaE=0;
//  float d_time[fgkNTimeBin]={fgkNTimeBin*0};
//  float d_fadc[fgkNTimeBin]={fgkNTimeBin*0};
//  

//  for (Int_t isec = 0; isec < fgkNSector; isec++)
//    {
//      for (Int_t iseg = 0; iseg < fgkNSegment; iseg++)
//        {
//          for (Int_t jrow = 0; jrow < fgkNRow; jrow++)
//            {
//              for (Int_t kcol = 0; kcol < fgkNCol; kcol++)
//                {
//		  if(fnhit[isec][iseg][jrow][kcol]!=0){
//		    ftime[isec][iseg][jrow][kcol]= 
//		      ftime[isec][iseg][jrow][kcol]/fnhit[isec][iseg][jrow][kcol];
//		  }
//		  
//		} // Track Loop ended
//	    }
//	}
//    }


//  for (Int_t isec = 0; isec < fgkNSector; isec++)
//    {
//      for (Int_t iseg = 0; iseg < fgkNSegment; iseg++)
//	{
//	  for (Int_t jrow = 0; jrow < fgkNRow; jrow++)
//	    {
//	      for (Int_t kcol = 0; kcol < fgkNCol; kcol++)
//		{
//		  Float_t adc = 0;
//		  GeV2ADC(fedep[isec][iseg][jrow][kcol], adc);
//		  
//		  ///// gain and pedestal correction 
//		  fadc[isec][iseg][jrow][kcol] += adc;	      
//		  
//		  
//		  Float_t d_time[fgkNTimeBin]={fgkNTimeBin*0};
//		  Float_t d_fadc[fgkNTimeBin]={fgkNTimeBin*0};
//		  
//		  GeV2FADC(fedep[isec][iseg][jrow][kcol],
//			   ftime[isec][iseg][jrow][kcol],
//			   d_time, d_fadc); 
//		  for(int it=0;it<fgkNTimeBin; it++){
//		    ///// gain and pedestal correction
//		    fftime[isec][iseg][jrow][kcol][it] = d_time[it];
//		    ffadc[isec][iseg][jrow][kcol][it] += d_fadc[it];
//		  }
//		  deltaE = fadc[isec][iseg][jrow][kcol];
//		  if (deltaE > 0. && fnhit[isec][iseg][jrow][kcol]>=1 && 
//		      ftime[isec][iseg][jrow][kcol]>0.)
//		    {
//		      AddDigit(isec, iseg, -1, jrow, kcol, deltaE, d_time, d_fadc);
//		    }
//		}
//	    }
//	}
//    }


//  treeD->Fill();
//  ResetDigit();
//  fFOCALLoader->WriteDigits("OVERWRITE");
//  ResetCellADC();
//}


//____________________________________________________________________________
void AliFOCALDigitizer::ResetSDigit()
{
  if (!fDoSDigits)
    return;
    
  // Clears SDigits
  fNSDigit = 0;
  fSDigits->Clear();
  for (Long_t i = 0; i < fNSeg; i++)
    fSSegmentMap[i].ResetMap();
  
}

//____________________________________________________________________________
void AliFOCALDigitizer::ResetDigit()
{
  if (!fDoDigits)
    return;
    
  // Clears Digits
  fNDigit = 0;
  fDigits->Clear();
  for (Long_t i = 0; i < fNSeg; i++)
    fSegmentMap[i].ResetMap();
}

//____________________________________________________________________________
bool AliFOCALDigitizer::XYZ2ColRowSeg(Float_t x, Float_t y, Float_t z, Int_t & col, Int_t & row, Int_t & seg) {

  Int_t layer;
  return fGeom->GetVirtualInfo(x,y,z,col,row,layer,seg);
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::GetNSeg() {
   return fGeom->GetVirtualNSegments();
}

//____________________________________________________________________________
Int_t AliFOCALDigitizer::GetNColRow(Int_t segment, Int_t & cols, Int_t & rows) {
   return fGeom->GetVirtualNColRow(segment, cols, rows);
}

//____________________________________________________________________________
void AliFOCALDigitizer::SetOption(Option_t * option) {

  if (strstr(option,"HS")) 
    fDoSDigits = true; 
  else
    fDoSDigits = false;
    
  if (strstr(option,"HD"))
    fDoDigits = true;
  else 
    fDoDigits = false;
}








