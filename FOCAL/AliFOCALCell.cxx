/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 30   2009                            //
//                                                     //
//  Cell code for ALICE-FOCAL                          //
//                                                     //
//-----------------------------------------------------//


#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

#include "AliFOCALCell.h"


ClassImp(AliFOCALCell)

AliFOCALCell::AliFOCALCell():
  TObject(),
  fCellid(0),
  fRow(0),
  fCol(0),
  fSegment(0),
  fTower(0),
  fX(0),
  fY(0),
  fZ(0),
  fE(0),
  fWeight(0),
  fSeedEnergy(0),
  fCenter(true)
{
  //Default constructor
}

AliFOCALCell::AliFOCALCell(Int_t cellID, Int_t row, Int_t col, Int_t segment, Int_t tower, 
			   Float_t x, Float_t y, Float_t z, Float_t e):
  TObject(),
  fCellid(cellID),
  fRow(row),
  fCol(col),
  fSegment(segment),
  fTower(tower),
  fX(x),
  fY(y),
  fZ(z),
  fE(e),
  fWeight(0),
  fSeedEnergy(0),
  fCenter(true)
{
  //Default constructor
}

AliFOCALCell::AliFOCALCell(AliFOCALCell *fCell):
  TObject(),
  fCellid(0),
  fRow(0),
  fCol(0),
  fSegment(0),
  fTower(0),
  fX(0),
  fY(0),
  fZ(0),
  fE(0),  
  fWeight(0),
  fSeedEnergy(0),
  fCenter(true)
{
  
  *this = fCell; 

}
AliFOCALCell::AliFOCALCell(const AliFOCALCell &fCell):
  TObject(fCell),
  fCellid(fCell.fCellid),
  fRow(fCell.fRow),
  fCol(fCell.fCol),
  fSegment(fCell.fSegment),
  fTower(fCell.fTower),
  fX(fCell.fX),
  fY(fCell.fY),
  fZ(fCell.fZ),
  fE(fCell.fE),
  fWeight(fCell.fWeight),
  fSeedEnergy(fCell.fSeedEnergy),
  fCenter(fCell.fCenter)
{


}

AliFOCALCell & AliFOCALCell::operator=(const AliFOCALCell &fCell){
  if(this != &fCell){
    fCellid=fCell.fCellid;
    fRow = fCell.fRow;
    fCol = fCell.fCol;
    fSegment = fCell.fSegment;
    fTower = fCell.fTower;
    fX = fCell.fX;
    fY = fCell.fY;
    fZ = fCell.fZ;
    fE = fCell.fE;
    fWeight = fCell.fWeight;
    fSeedEnergy = fCell.fSeedEnergy;
    fCenter = fCell.fCenter;
  }
  return *this;
}

AliFOCALCell::~AliFOCALCell()
{
  // Default destructor
}

Int_t AliFOCALCell::Compare(const TObject * obj) const {
  
  if (this == obj) return 0;
  if (strcmp(this->ClassName(),obj->ClassName()) == 0) {
    AliFOCALCell * cell = (AliFOCALCell*) obj;
    if (fE == cell->fE) return 0;
    return fE > cell->fE ? 1 : -1;
  } 
  else
    return 1;
}


Float_t AliFOCALCell::LogWeightE(Float_t totalE, Float_t w0, bool mode) {
  
  if (mode)
    return LogWeight(totalE, w0);
  else
    return E();
}

Float_t AliFOCALCell::LogWeight(Float_t totalE, Float_t w0) {
    
  return TMath::Max(0.0, w0 + TMath::Log(fE/totalE));
}

Float_t AliFOCALCell::PowWeightE(Float_t totalE, Float_t w0, bool mode) {
  
  if (mode)
    return PowWeight(totalE, w0);
  else
    return E();
}

Float_t AliFOCALCell::PowWeight(Float_t totalE, Float_t w0) {
    
  return TMath::Power(fE/totalE,w0);
}

