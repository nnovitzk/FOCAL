#ifndef ALIGENPYTHIAFOCAL_H
#define ALIGENPYTHIAFOCAL_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

//
// Derived class from AliGenPythia with option to check for a particle in the FOCAL acceptance
//   For technical reasons there is interference with EMCAL, so you cannot check EMCAL and FOCAL at the same time...
//
// marco.van.leeuwen@cern.ch
//

#include "AliGenPythia.h"

class AliGenPythiaFOCAL: public AliGenPythia {

 public:
  AliGenPythiaFOCAL();
  AliGenPythiaFOCAL(Int_t npart);
   ~AliGenPythiaFOCAL() {;}
   virtual void  SetCheckFOCAL(Bool_t b) {fCheckFOCAL  = b; fCheckEMCAL = b; // this is needed to make sure that the check is done; fCheckFOCAL is not in the base class
   }
   void SetFOCALEta(Float_t minEta, Float_t maxEta) { fFOCALMinEta = minEta; fFOCALMaxEta = maxEta; }
   virtual void  SetFragPhotonInFOCAL  (Bool_t b) {fCheckFOCAL    = b; fCheckEMCAL = b; fFragPhotonInCalo = b;}
   virtual void  SetHadronInFOCAL      (Bool_t b) {fCheckFOCAL    = b; fCheckEMCAL = b; fHadronInCalo     = b;}
   virtual void  SetElectronInFOCAL    (Bool_t b) {fCheckFOCAL    = b; fCheckEMCAL = b; fEleInCalo        = b;}
   
   virtual void  SetDecayPhotonInFOCAL (Bool_t d)  {fDecayPhotonInCalo = d; fCheckFOCAL    = d; fCheckEMCAL = d;}
   virtual void  SetPi0InFOCAL         (Bool_t b, Bool_t f = kFALSE) {fPi0InCalo = b; fForceNeutralMeson2PhotonDecay = f; fCheckFOCAL  = b; fCheckEMCAL = b; }
   
   virtual void  SetEtaInFOCAL         (Bool_t b, Bool_t f = kFALSE) {fEtaInCalo = b; fForceNeutralMeson2PhotonDecay = f; fCheckFOCAL  = b; fCheckEMCAL = b; }
   virtual void  SetPi0PhotonDecayInFOCAL  (Bool_t b, Bool_t f = kFALSE) {fPi0InCalo = b; fDecayPhotonInCalo = b; fForceNeutralMeson2PhotonDecay = f; fCheckFOCAL = b; fCheckEMCAL = b; }
   virtual void  SetEtaPhotonDecayInFOCAL  (Bool_t b, Bool_t f = kFALSE) {fEtaInCalo = b; fDecayPhotonInCalo = b; fForceNeutralMeson2PhotonDecay = f; fCheckFOCAL = b; fCheckEMCAL = b; }
   Bool_t IsInFOCAL  (Float_t eta) const;
   Bool_t CheckDetectorAcceptance(Float_t phi, Float_t eta, Int_t iparticle);
   virtual Bool_t TriggerOnSelectedParticles(Int_t np);

 protected: 
   Bool_t fCheckFOCAL;        // Option to ask for FragPhoton or Pi0 or Eta or gamma decays in FOCAL acceptance
   Float_t fFOCALMinEta;
   Float_t fFOCALMaxEta;
  
   ClassDef(AliGenPythiaFOCAL,0)
};

#endif
