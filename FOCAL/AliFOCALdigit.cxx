/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//  Date   : August 05 2003                            //
//  used to store the info into TreeS                  //
//                                                     //
//-----------------------------------------------------//
#include "Riostream.h"
#include "Rtypes.h"
#include "AliFOCALdigit.h"
#include <stdio.h>

ClassImp(AliFOCALdigit)

AliFOCALdigit::AliFOCALdigit():
  AliDigitNew(),
  fCol(-1),
  fRow(-1),
  fSegment(-1)
{
  fAmp = 0;
  fDepEn = 0;
  fId = -1;
  fIndexInList = -1;
}

AliFOCALdigit::AliFOCALdigit(Int_t id, Int_t index, Int_t col, Int_t row, Int_t seg, Int_t amp):
  AliDigitNew(),
  fCol(col),
  fRow(row),
  fSegment(seg)
{
  fAmp = amp;
  fDepEn = 0;
  fId = id;
  fIndexInList = index;
}


AliFOCALdigit::AliFOCALdigit(Int_t col, Int_t row, Int_t seg, Int_t amp):
  AliDigitNew(),
  fCol(col),
  fRow(row),
  fSegment(seg)
{
  fAmp = amp;
  fDepEn = 0;
  fId = -1;
  fIndexInList = -1;
}

AliFOCALdigit::AliFOCALdigit(AliFOCALdigit *focaldigit):
  AliDigitNew(*focaldigit)
{
  *this = *focaldigit;
}

AliFOCALdigit::AliFOCALdigit(const AliFOCALdigit& focaldigit):
  AliDigitNew(focaldigit),
  fCol(focaldigit.fCol),
  fRow(focaldigit.fRow),
  fSegment(focaldigit.fSegment)
{
  fAmp = focaldigit.fAmp;
  fDepEn = focaldigit.fDepEn;
  fId = focaldigit.fId;
  fIndexInList = focaldigit.fIndexInList;
}

AliFOCALdigit & AliFOCALdigit::operator=(const AliFOCALdigit& focaldigit)
{
  //Assignment operator 
  if(this != &focaldigit)
    {
      fId = focaldigit.fId;
      fIndexInList = focaldigit.fIndexInList;
      fSegment     = focaldigit.fSegment;
      fRow = focaldigit.fRow;
      fCol = focaldigit.fCol;
      fAmp = focaldigit.fAmp;
      fDepEn = focaldigit.fDepEn;
    }
  return *this;
}


AliFOCALdigit::~AliFOCALdigit()
{
  // Default Destructor
}


