#ifndef ALIFOCALClusterizer_H
#define ALIFOCALClusterizer_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  30 2009                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//
// Base class for the clusterization algorithm 


#include "Rtypes.h"
#include "TObject.h"
#include "AliLog.h"
#include "TFile.h" 
#include "TBranch.h"

class TClonesArray;
class TTree;
class TFile;

class AliFOCALClusterizer : public TObject
{
	public:
		AliFOCALClusterizer() ;        // default constructor 
		AliFOCALClusterizer(const AliFOCALClusterizer &); 
		AliFOCALClusterizer & operator = (const AliFOCALClusterizer &);
		virtual ~AliFOCALClusterizer() ; // destructor 

		virtual void InitParameters(const char * /* file */) { AliInfo("Overload this method."); }
		virtual void Init() { AliInfo("Overload this method."); }

		TClonesArray * GetCluster() const { return fCluster; }
		TClonesArray * GetClusterItr() const { return fClusterItr; }
		TClonesArray * GetClusterHCal() const { return fClusterHCal; }
		TClonesArray * GetCells() const {return fCell; }

		virtual void Digits2Clusters(Option_t *option) = 0;
		virtual void Hits2Clusters(Option_t *option) = 0;

		virtual void SetClusteringEnergyThreshold(Float_t) = 0;
		virtual void SetLocalEnergyThreshold(Float_t) = 0;
		virtual void SetInput(TTree *inputTree, const char *format); //Digits or Hits
		virtual void SetBackground(TTree *backgroundTree); // For embedding
		virtual void SetOutput(const char *name); // clustering info.
		virtual TFile * CreateOutputFile(const char * fileName); // Set output file, do not create anything inside, return pointer to it
		virtual TDirectory * SetOutputForEvent(const char * eventName); // Create directory inside the output file, create TTrees, returns pointer to created 
		virtual Bool_t SaveEventOutput(const char * eventName);
		virtual Bool_t CloseOutputFile();

		/////////////////////////////////////////////////////
		//this is only for analyzing from AliFOCALhit 
		//virtual void AnalyzeEvent(TTree *hits) ;
		virtual void AnaEnd(void);
		////////////////////////////////////////////////////

	protected:
		virtual void MakeClusters() = 0;
  
		TFile *fout ;
		TBranch *branch;
		TBranch *fBackgroundBranch;  
		TClonesArray *fInputArray; // Array with FOCAL digits
		TTree *fTreeR; // Tree with output clusters
  
		TClonesArray *fCluster;    // Array with final FOCAL clusters
		TClonesArray *fClusterItr; // Array with segment-by-segment FOCAL clusters
		TClonesArray *fClusterHCal; // Array with final HCAL clusters
		TClonesArray *fPad;    // Array with FOCAL Pads   (copy only from hits)
		TClonesArray *fCell;  // Array with FOCAL Cells (copy from digitizer)
  
	ClassDef(AliFOCALClusterizer,2);  // Clusterization algorithm class
} ;

#endif // AliFOCALLCLUSTERIZER_H

