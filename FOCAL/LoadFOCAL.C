void LoadFOCALSim(const Char_t *libdir) {
   gSystem->Load("libpythia6_4_28.so");
   gSystem->Load(Form("%s/AliGenHijingFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliGenPythiaFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALComposition_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALGeometry_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALhit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALKinematics_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALLoader_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALPad_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALPedestal_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALdigit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALsdigit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALDigitizer_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALv0_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALv1_cxx.so",libdir));
   gSystem->Load(Form("%s/AliPIPEFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliHALLv4_cxx.so",libdir));
}

void LoadFOCAL(const Char_t *libdir) {
   gSystem->Load(Form("%s/AliFOCALComposition_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALGeometry_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALhit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALKinematics_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALLoader_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALPad_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALdigit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALsdigit_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALDigitizer_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALv0_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALv1_cxx.so",libdir));
   gSystem->Load(Form("%s/AliPIPEFOCAL_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALCalibData_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALCluster_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALClusterizer_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALCell_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALCellMap_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALRinger_cxx.so",libdir));
   gSystem->Load(Form("%s/ObjectMap_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALClusterizerv1_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALClusterizerv2_cxx.so",libdir));
   gSystem->Load(Form("%s/AliFOCALClusterizerv3_cxx.so",libdir));
}
   
