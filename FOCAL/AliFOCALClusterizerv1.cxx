/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 16   2010                            //
//                                                     //
//  Clusterizerv1 for ALICE-FOCAL                      //
//                                                     //
//-----------------------------------------------------//

#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

// --- ROOT system ---
#include <cassert>

class TROOT;
#include <TH1.h>
#include <TFile.h>
class TFolder;
#include <TMath.h>
#include <TMinuit.h>
#include <TTree.h>
class TSystem;
#include <TBenchmark.h>
#include <TBrowser.h>
#include <TROOT.h>

/// -- Aliroot 
#include "AliFOCALhit.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALPad.h"
#include "AliFOCALCell.h"
#include "AliFOCALCluster.h"
#include "AliFOCALClusterizerv1.h"

#include "AliLog.h"
#include "AliRun.h"
#include "AliHit.h"
#include "AliDetector.h"
#include "AliRunLoader.h"
#include "AliLoader.h"
#include "AliConfig.h"
#include "AliMagF.h"
#include "AliDigitizer.h"
#include "AliHeader.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBEntry.h"

ClassImp(AliFOCALClusterizerv1)

using namespace std;

//________________________________________________________________________//
AliFOCALClusterizerv1::AliFOCALClusterizerv1():
  AliFOCALClusterizer(),
  fGeom(0),
  fCHI2Limit(0),
  fNumberOfClusters(0),
  fNumberOfClustersItr(0),
  fClusteringEnergyThreshold(0),
  fLocalEnergyThreshold(0),
  fSegLayer(true)
{
  //Init();
}

//____________________________________________________________________________//
AliFOCALClusterizerv1::AliFOCALClusterizerv1(const AliFOCALClusterizerv1 &ali):
  AliFOCALClusterizer(ali),
  fGeom(ali.fGeom),
  fCHI2Limit(ali.fCHI2Limit),
  fNumberOfClusters(ali.fNumberOfClusters),
  fNumberOfClustersItr(ali.fNumberOfClustersItr),
  fClusteringEnergyThreshold(ali.fClusteringEnergyThreshold),
  fLocalEnergyThreshold(ali.fLocalEnergyThreshold),
  fSegLayer(ali.fSegLayer)
{
  //Init();
} 

//____________________________________________________________________________//
AliFOCALClusterizerv1::AliFOCALClusterizerv1(AliFOCALClusterizerv1 *ali):
  AliFOCALClusterizer(),
  fGeom(0),
  fCHI2Limit(0),
  fNumberOfClusters(0),
  fNumberOfClustersItr(0),
  fClusteringEnergyThreshold(0),
  fLocalEnergyThreshold(0),
  fSegLayer(true)
{
	*this = ali;
}

//____________________________________________________________________________//
AliFOCALClusterizerv1& AliFOCALClusterizerv1::operator=(const AliFOCALClusterizerv1 &ali)
{
	if(this!=&ali)
	{
		fGeom = ali.fGeom;
		fCHI2Limit = ali.fCHI2Limit;
		fNumberOfClusters = ali.fNumberOfClusters;
		fNumberOfClustersItr = ali.fNumberOfClustersItr;
		fClusteringEnergyThreshold = ali.fClusteringEnergyThreshold;
		fLocalEnergyThreshold = ali.fLocalEnergyThreshold;
		fSegLayer = ali.fSegLayer;
	}
	return *this;
	
}

//____________________________________________________________________________//
AliFOCALClusterizerv1::~AliFOCALClusterizerv1()
{
	// Default destructor
	delete fGeom;

}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::InitGeometry(char *geometryfile)
{
	fGeom = AliFOCALGeometry::GetInstance(geometryfile);
}
 
//____________________________________________________________________________//
void AliFOCALClusterizerv1::InitGeometry()
{
	fGeom = AliFOCALGeometry::GetInstance();
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::Hits2Clusters(Option_t * /*option*/)
{
	fNumberOfClusters = 0;
	fNumberOfClustersItr = 0;
	
	LoadHits2Cells();
	MakeClusters();
	
	fTreeR->Fill();
	
	AliDebug(1,Form("FOCAL Clusterizer found %d Cluster Points",fCluster->GetEntriesFast()));
	
	fClusterItr->Delete();
	fCluster->Delete();
	fPad->Delete();
	fCell->Delete();	
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::Digits2Clusters(Option_t * /*option*/)
	// load calibration parameters 
{
	// load digits 
	// pedestal extraction 
	// convert adc to energy
	// and fill to fPad and fStrip  
	LoadDigits2Cell();
	
	MakeClusters();
	
	fTreeR->Fill();
	
	AliDebug(1,Form("FOCAL Clusterizer found %d Rec Points",fCluster->GetEntriesFast()));
	
	fClusterItr->Delete();
	fCluster->Delete();
	fPad->Delete();
}

//____________________________________________________________________________
void AliFOCALClusterizerv1::LoadHits2Cells() {
  
  cout << "Hits2Cells called." << endl;
	
	if (!branch) {
	  AliError("AliFOCALClusterizerv1: LoadHits2Cells called, but input is not set!");
	  return;
	}
	
	if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	
	AliFOCALDigitizer * digitizer = new AliFOCALDigitizer();
	digitizer->SetGeometry(fGeom);
	digitizer->Initialize("HS");
	digitizer->Hits2Digits(branch);
	fInputArray = digitizer->GetSDigits();
	
	fCell->Clear();
	int fNCell=0;

	//  AliFOCALhit *focalhit = new AliFOCALhit(); 
	AliFOCALdigit * digit; 
	cout << "Number of digits: " << fInputArray->GetEntries() << endl;
	for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
	{
		digit = (AliFOCALdigit*)fInputArray->UncheckedAt(itrk);
		Float_t x,y,z;
		fGeom->GetXYZFromColRowSeg(digit->GetCol(), digit->GetRow(), digit->GetSegment(), x, y, z);
		new((*fCell)[fNCell]) AliFOCALCell(fNCell, digit->GetRow(), digit->GetCol(), digit->GetSegment(), -1, 
		   x, y, z, (Float_t)digit->GetAmp());
		fNCell++;
	}
	
	
	digitizer->SetGeometry(0); // Neccesary, or digitizer would delete it
	delete digitizer;
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::LoadHits2Pad(Option_t *option)
{
	if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	fPad->Clear();
	int fNPads=0;
	
	TObjArray *hitsC = new TObjArray(10000);
	
	//// classify the hit information to pad or strips
	//  AliFOCALhit *focalhit = new AliFOCALhit(); 
	AliFOCALhit *focalhit = new AliFOCALhit(); 
	for(int i=0;i<branch->GetEntries(); i++)
	{
		fInputArray = new TClonesArray("AliFOCALhit",10000);
		branch->SetAddress(&fInputArray);
		branch->GetEntry(i);
		for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
		{
			focalhit = (AliFOCALhit*)fInputArray->UncheckedAt(itrk);
			//Bool_t b_pad = fGeom->isPadOrStrip(focalhit->GetVolume(0));
			Bool_t b_pad = kTRUE; // Hack; strip stuff has been removed from FOCALGeometry
			if(b_pad==true)
			{
				hitsC->AddLast(focalhit);
			}
		}
		fInputArray->Delete();
	}
	
	//cout<<hitsC->GetEntriesFast()<<" "<<hitsS->GetEntriesFast()<<endl;
	//// simply fill to the AliFOCALPad and AliFOCALStrip
	for(int i=0;i<hitsC->GetEntriesFast(); i++)
	{
		focalhit = (AliFOCALhit*)hitsC->UncheckedAt(i);
		TClonesArray &cell = *fPad;
		int id = focalhit->GetVolume(0); // pad  or strip
		int loc_cell = focalhit->GetVolume(1); // number of segment
		int row, col, stack, layer, segment; 
		// row is the location of pad in the tower
		// col is the location of pad in the tower 
		// cellid is the tower location in the brick 
		// layer is the place of the si pad layer
		fGeom->GetPadPositionId2RowColStackLayer(id, 
		row, col, stack, layer, segment);
		float energy = focalhit->GetEnergy()/1.0e+09; //GeV
		
		Double_t x, y, z;
		fGeom->GetGeoPadCenter(loc_cell-1, layer, stack, row, col, x, y, z); 
		new(cell[fNPads++]) AliFOCALPad(id, row, col, layer, segment, loc_cell-1, x, y, z,	energy);
	}
	
	Pads2Cells(option);
	
	delete focalhit;
	delete hitsC;
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::Pads2Cells(Option_t *opt)
{
	fCell->Clear(); 
	int fNCell = 0;
	
	if(!strcmp(opt,"sum")){ /* // meanin pads are summed up between 7 layers 7:7:7 */ }
	else{  }
	
	//// copy the elements of AliFOCALPad 
	TObjArray *lpads = new TObjArray();
	TIter nextpad(fPad);
	AliFOCALPad *focalpad = new AliFOCALPad(); 
	while ( (focalpad = dynamic_cast<AliFOCALPad*>(nextpad())) )
	{
		lpads->AddLast(focalpad);
	}
	nextpad.Reset();

	//// add pads to the Cells
	//  TClonesArray &lpads = *fPad;
	AliFOCALPad *new_pad = new AliFOCALPad();
	AliFOCALPad *curr_pad = new AliFOCALPad();
	
	float esum=0;
	float zsum=0;
	for (int i=0; i<lpads->GetEntriesFast(); i++)
	{
		TClonesArray &cell = *fCell;
		esum=0; zsum=0;
		
		new_pad = (AliFOCALPad*)lpads->UncheckedAt(i);
		if(new_pad == 0x0)
		{
			continue;
		}
		esum = new_pad->E();
		zsum = new_pad->E()*new_pad->Z();
		
		for(int j=i+1; j<lpads->GetEntriesFast();j++)
		{
			curr_pad  = (AliFOCALPad*)lpads->UncheckedAt(j);
			if(curr_pad  == 0x0)
			{
				continue;
			}
			if(//new_pad->PadID()==curr_pad->PadID() &&   // pad position in wafers mactches 
			new_pad->Row()==curr_pad->Row() &&       // wafer position matches 
			new_pad->Col()==curr_pad->Col() &&        
			new_pad->Segment()==curr_pad->Segment()  && // longtudinal segment mahctes         
			new_pad->Tower()==curr_pad->Tower() )  // brich matches 
			{
				esum += curr_pad->E();
				zsum += curr_pad->E()*curr_pad->Z();
				lpads->Remove(curr_pad);
			}
		}
		new(cell[fNCell++]) AliFOCALCell(new_pad->PadID(), new_pad->Row(), new_pad->Col(), new_pad->Segment(), 
		new_pad->Tower(), new_pad->X(), new_pad->Y(), zsum/esum, esum);
	}
	new_pad->Clear();
	curr_pad->Clear();
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::LoadDigits2Cell()
{
	/// not available now 
	/// all the Digits information will be converted 
	/// to AliFOCALCells and AliFOCALStrips 
	
	cout << "Digits2Cells called." << endl;
	
	if (!branch) {
	  AliError("AliFOCALClusterizerv1: LoadDigits2Cell called, but input is not set!");
	  return;
	}
	
	if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	fCell->Clear();
	int fNCell=0;

	//  AliFOCALhit *focalhit = new AliFOCALhit(); 
	AliFOCALdigit * digit; 
	branch->SetAddress(&fInputArray);
	cout << "Entries = " << branch->GetEntries() << endl;
	for(int i=0;i<branch->GetEntries(); i++)
	{	
		branch->GetEntry(i);
		cout << fInputArray->GetEntries() << endl;
		for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
		{
			digit = (AliFOCALdigit*)fInputArray->UncheckedAt(itrk);
			Float_t x,y,z;
			fGeom->GetXYZFromColRowSeg(digit->GetCol(), digit->GetRow(), digit->GetSegment(), x, y, z);
			new((*fCell)[fNCell]) AliFOCALCell(fNCell, digit->GetRow(), digit->GetCol(), digit->GetSegment(), -1, 
			   x, y, z, (Float_t)digit->GetAmp());
			fNCell++;
		}
	}
	
}

//____________________________________________________________________________//
void AliFOCALClusterizerv1::MakeClusters()
{
	fNumberOfClusters = 0;  
	fNumberOfClustersItr = 0;  
	if (fGeom == 0) AliFatal("Did not get geometry from FOCALLoader");
	fCluster->Clear();
	fClusterItr->Clear();
	
	/// we start looking at AliFOCALCells
	
	// Set up TObjArray with pointers to digits to work on
	TObjArray *cellsC = new TObjArray();
	TIter nextCell(fCell);
	AliFOCALCell *cell = new AliFOCALCell();
	
	// copy all the AliFOCALCells to the ObjArray
	while ( (cell = dynamic_cast<AliFOCALCell*>(nextCell())) )
	{
		if(cell->E()>fLocalEnergyThreshold)
		{
			cellsC->AddLast(cell);
		}
	}
    
	/// search for the local maximum cell
	AliFOCALCell *curr_cell = new AliFOCALCell();
	AliFOCALCell *new_cell = new AliFOCALCell();
	
	for(int i=0;i<cellsC->GetEntriesFast(); i++)
	{
		new_cell  = (AliFOCALCell*)cellsC->UncheckedAt(i);
		if(new_cell->Center()==false)
		{
			continue;
		}
		for(int j=i+1;j<cellsC->GetEntriesFast(); j++)
		{
			curr_cell  = (AliFOCALCell*)cellsC->UncheckedAt(j);
			if(Neighboring_Cells(new_cell, curr_cell) == true)
			{
				if(new_cell->E() > curr_cell->E())
				{
					int idx = cellsC->IndexOf(curr_cell);
					cellsC->Remove(curr_cell);
					curr_cell->SetCellCenter(false);
					cellsC->AddAt(curr_cell, idx);
				}
				else
				{
					int idx = cellsC->IndexOf(new_cell);
					cellsC->Remove(new_cell);
					new_cell->SetCellCenter(false);
					cellsC->AddAt(new_cell, idx);
				}
			}
		}
	}

	//// take 3x3 for the local maximum cell 
	int Segments = fGeom->GetNumberOfSegments();
	TObjArray *cluster; /// TObjArray that save information about clusters in each segment and these combined segments
	try
	{
		cluster = new TObjArray [Segments+1];
	}
	catch(bad_alloc xa)
	{
		cout << "Allocation Failure!\n";
		return;
	}
	
	max_segment = 0; /// variable with value of maximum segment "hitted" by particles in FoCal
	/// loop to put together clusters in each segment
	for(int i=0; i < cellsC->GetEntriesFast(); i++)
	{
		new_cell  = (AliFOCALCell*)cellsC->UncheckedAt(i);
		if(new_cell->Center() == false)
		{
			continue;
		}
		float new_e = new_cell->E();
		float new_x = new_cell->E()*new_cell->X();
		float new_y = new_cell->E()*new_cell->Y();
		float new_z = new_cell->E()*new_cell->Z();
		
		for(int j=i+1; j < cellsC->GetEntriesFast(); j++)
		{
			curr_cell  = (AliFOCALCell*)cellsC->UncheckedAt(j);
			if(curr_cell->Center()==true)
			{
				continue;
			}
			if(Neighboring_Cells(new_cell, curr_cell) == true)
			{
				new_e += curr_cell->E();
				new_x += curr_cell->E()*curr_cell->X();
				new_y += curr_cell->E()*curr_cell->Y();
				new_z += curr_cell->E()*curr_cell->Z();
			}
		}
		if(	((new_cell->Segment() != Segments && new_e >fClusteringEnergyThreshold) || new_cell->Segment() == Segments) && new_cell->Segment() != (Segments+1) )
		{
			//AliFOCALCluster *clst = new AliFOCALCluster(new_x/new_e, new_y/new_e, new_z/new_e, new_e);
			AliFOCALCluster *clst = new AliFOCALCluster(new_x/new_e, new_y/new_e, new_z/new_e, new_e, new_cell->Segment());
			clst->SetFlag(true);
			cluster[new_cell->Segment()].AddLast(clst);
						
			/// test if actual segment is less than new_cell segment
			if(max_segment < new_cell->Segment())
			{
				max_segment = new_cell->Segment();
			}
			
			clst->Clear();
		}
	}
	/// choose if will save the cluster of all segments or the clusters per segment
	if(fSegLayer)
	{
		cout << "Maximum segment: " << max_segment << endl;
		/// Function to relate clusters of each segment
		RunCluster(Segments,cluster);
		
		AliFOCALCluster *padcluster = new AliFOCALCluster();
		/// loop to copy cluster[] to arrays of AliFoCalCluster class, and find out the total number of created clusters
		for(int i = 0; i < cluster[Segments].GetEntriesFast(); i++)
		{
			///It don't need to know the segment, because it's the cluster of all segments
			TClonesArray &clusters  = *fCluster;
			TClonesArray &clustersItr  = *fClusterItr; 
			padcluster = (AliFOCALCluster*) cluster[Segments].UncheckedAt(i);
			new(clusters[fNumberOfClusters++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E()); 
			new(clustersItr[fNumberOfClustersItr++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E());
			//new(clusters[fNumberOfClusters++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E(), padcluster->Segment()); 
			//new(clustersItr[fNumberOfClustersItr++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E(), padcluster->Segment());
		}
	}
	else
	{
		for(int i = 0; i < Segments; i++)
		{
			for(int j = 0; j < cluster[i].GetEntriesFast(); j++)
			{
				cluster[Segments].AddLast(cluster[i].UncheckedAt(j));
			}
		}
				
		AliFOCALCluster *padcluster = new AliFOCALCluster();
		for(int i = 0; i < cluster[Segments].GetEntriesFast(); i++)
		{
			/// here it's needed to know the segments
			TClonesArray &clusters  = *fCluster;
			TClonesArray &clustersItr  = *fClusterItr; 
			padcluster = (AliFOCALCluster*) cluster[Segments].UncheckedAt(i);
			//new(clusters[fNumberOfClusters++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E()); 
			//new(clustersItr[fNumberOfClustersItr++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E());
			new(clusters[fNumberOfClusters++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E(), padcluster->Segment()); 
			new(clustersItr[fNumberOfClustersItr++]) AliFOCALCluster(padcluster->X(), padcluster->Y(), padcluster->Z(), padcluster->E(), padcluster->Segment());
		}
	}
	
	//// copy the TClonesArray to TObjArray
	//  TObjArray *fclus_obj = new TObjArray();
	AliFOCALCluster *fcluster = new AliFOCALCluster();
	TObjArray *fClusterObj = new TObjArray();
	for(int i = 0; i < fCluster->GetEntriesFast(); i++)
	{
		fcluster = (AliFOCALCluster*)fCluster->UncheckedAt(i);
		fClusterObj->AddLast(fcluster);
		std::cout << fcluster->X() << " " << fcluster->Y() << " " << fcluster->Z() << " " << fcluster->Segment() << endl;
		fcluster->Clear();
	}
	
	cout << " fNumberOfClusters = " << fNumberOfClusters << " " << fNumberOfClustersItr << endl;
	
	cellsC->Clear();  
	curr_cell->Clear();
	new_cell->Clear();
	fcluster->Clear();
	delete cellsC; 
	delete cell;
	delete [] cluster;
	//delete fcluster;
	//delete fClusterObj;
	//delete fcluster;
}

//________________________________________________________________________
// function to relate cluster of segments
void AliFOCALClusterizerv1::RunCluster(int Segments, TObjArray *cluster)
{
	float x0, y0, z0;
	float x1, y1, z1;
	float new_e;
	float new_x;
	float new_y;
	float new_z;
	AliFOCALCluster *clus = new AliFOCALCluster();
	AliFOCALCluster *clus1 = new AliFOCALCluster();

	// loop on segments
	for(int iSeg = 0; iSeg <= max_segment; iSeg++)
	{
			new_e = new_x = new_y = new_z = 0;
			int max_entrie_iSeg = cluster[iSeg].GetEntriesFast();

			// loop on cluster of primary segments
			for(int i = 0; i < max_entrie_iSeg; i++)
			{
					x0 = y0 = z0 = 0;
					clus = (AliFOCALCluster*) cluster[iSeg].UncheckedAt(i);

					// if this object it was already used continue to the next
					if(!(clus->Flag()))
					{
						continue;
					}
					
					z0 = fGeom->GetFOCALSegmentZ(iSeg);
					x0 = clus->X();
					y0 = clus->Y();

					new_x += x0*clus->E();
					new_y += y0*clus->E();
					new_z += z0*clus->E();
					new_e += clus->E();
					
					// loop on segments foward segment before
					for(int jSeg = iSeg+1; jSeg <= max_segment; jSeg++)
					{
							int max_entrie_jSeg = cluster[jSeg].GetEntriesFast();

							// loop on seconds, third, etc segments
							for(int j = 0; j < max_entrie_jSeg; j++)
							{
									x1 = y1 = z1 = 0;
									clus1 = (AliFOCALCluster*) cluster[jSeg].UncheckedAt(j);

									if(!(clus1->Flag()))
									{
										continue;
									}
									
									z1 = fGeom->GetFOCALSegmentZ(jSeg);
									x1 = clus1->X();
									y1 = clus1->Y();
									
									float dx = x1 - x0/(z0)*z1;
									float dy = y1 - y0/(z0)*z1;

									if(-fDistance<dx && dx<fDistance && -fDistance<dy && dy<fDistance)
									{
											new_x += x1*clus1->E();
											new_y += y1*clus1->E();
											new_z += z1*clus1->E();
											new_e += clus1->E();

											clus1->SetFlag(false);
											clus1->Clear();
											break;

									}
									clus1->Clear();
							}
					}
					
					AliFOCALCluster *clst = new AliFOCALCluster(new_x/new_e, new_y/new_e, new_z/new_e, new_e);
					cluster[Segments].AddLast(clst);
					clst->Clear();
					clus->SetFlag(false);
					clus->Clear();
			}
	}
}

bool AliFOCALClusterizerv1::Neighboring_Cells(AliFOCALCell *twr1, AliFOCALCell *twr2)
{
	float dx = twr1->X()-twr2->X();
	float dy = twr1->Y()-twr2->Y();
	
	if(twr1->Segment()!=twr2->Segment())
	{
		return false;
	}

	if(dx>-5 && dx<5 && dy>-5 && dy<5)
	{
		return true;
	}
	return false;
}

//_____________________________________________________________________________
void AliFOCALClusterizerv1::AnalyzeEvent(TTree *tree, int type)
{
	// type = 0 -> execute the Digits2Cluster method
	// type = 1 -> execute the Hits2Cluster method
	switch(type)
	{
		case 0:
			SetInput(tree, "DIGITS");
			Digits2Clusters("sum");
			break;
		case 1:
			SetInput(tree, "HITS");
			Hits2Clusters("sum");
			break;
		default:
			cout << "Choose betwen digits (0) or hits (1)\n";
	}
}


