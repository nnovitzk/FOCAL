/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
    //-----------------------------------------------------//
    //                                                     //
    //                                                     //
    //  Date   : July 16   2010                            //
    //                                                     //
    //  Clusterizerv1 for ALICE-FOCAL                      //
    //                                                     //
    //-----------------------------------------------------//

#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

    // --- ROOT system ---
#include <cassert>

class TROOT;
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
class TFolder;
#include <TMath.h>
#include <TMinuit.h>
#include <TTree.h>
#include <TPolyMarker.h>
#include <TCanvas.h>
#include <TStyle.h>
class TSystem;
#include <TBenchmark.h>
#include <TBrowser.h>
#include <TROOT.h>
#include <TParticle.h>

    /// -- Aliroot
#include "AliFOCALhit.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALPad.h"
#include "AliFOCALCell.h"
#include "AliFOCALCluster.h"
#include "AliFOCALClusterizerv2.h"

#include "AliLog.h"
#include "AliRun.h"
#include "AliHit.h"
#include "AliDetector.h"
#include "AliStack.h"
#include "AliRunLoader.h"
#include "AliLoader.h"
#include "AliConfig.h"
#include "AliMagF.h"
#include "AliDigitizer.h"
#include "AliHeader.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBEntry.h"

ClassImp(AliFOCALClusterizerv2)

using namespace std;

//________________________________________________________________________//
AliFOCALClusterizerv2::AliFOCALClusterizerv2():
AliFOCALClusterizer(),
fGeom(0),
fHCALSegID(0),
fCHI2Limit(0),
fNumberOfClusters(0),
fNumberOfClustersItr(0),
fClusteringEnergyThreshold(0),
fLocalEnergyThreshold(0),
fSegLayer(true),
fPixelNoiseProb(0.),
fPadNoiseSigma(0.),
fCellMap(0),
fRinger(0),
fSeeds(0),
fParameters(0),
fDebugMode(false)
{
        //Init();
}

//____________________________________________________________________________//
AliFOCALClusterizerv2::AliFOCALClusterizerv2(const AliFOCALClusterizerv2 &ali):
AliFOCALClusterizer(ali),
fGeom(ali.fGeom),
fHCALSegID(ali.fHCALSegID),
fCHI2Limit(ali.fCHI2Limit),
fNumberOfClusters(ali.fNumberOfClusters),
fNumberOfClustersItr(ali.fNumberOfClustersItr),
fClusteringEnergyThreshold(ali.fClusteringEnergyThreshold),
fLocalEnergyThreshold(ali.fLocalEnergyThreshold),
fSegLayer(ali.fSegLayer),
fPixelNoiseProb(ali.fPixelNoiseProb),
fPadNoiseSigma(ali.fPadNoiseSigma),
fCellMap(ali.fCellMap),
fRinger(ali.fRinger),
fSeeds(ali.fSeeds),
fParameters(ali.fParameters),
fDebugMode(ali.fDebugMode)
{
        //Init();
}

//____________________________________________________________________________//
AliFOCALClusterizerv2::AliFOCALClusterizerv2(AliFOCALClusterizerv2 *ali):
AliFOCALClusterizer(),
fGeom(0),
fHCALSegID(0),
fCHI2Limit(0),
fNumberOfClusters(0),
fNumberOfClustersItr(0),
fClusteringEnergyThreshold(0),
fLocalEnergyThreshold(0),
fSegLayer(true),
fPixelNoiseProb(0.),
fPadNoiseSigma(0.),
fCellMap(0),
fRinger(0),
fSeeds(0),
fParameters(0),
fDebugMode(false)
{
	*this = ali;
}

//____________________________________________________________________________//
AliFOCALClusterizerv2& AliFOCALClusterizerv2::operator=(const AliFOCALClusterizerv2 &ali)
{
	if(this!=&ali)
	{
		fGeom = ali.fGeom;
        fHCALSegID = ali.fHCALSegID;
		fCHI2Limit = ali.fCHI2Limit;
		fNumberOfClusters = ali.fNumberOfClusters;
		fNumberOfClustersItr = ali.fNumberOfClustersItr;
		fClusteringEnergyThreshold = ali.fClusteringEnergyThreshold;
		fLocalEnergyThreshold = ali.fLocalEnergyThreshold;
		fSegLayer = ali.fSegLayer;
        fPixelNoiseProb = ali.fPixelNoiseProb;
        fPadNoiseSigma = ali.fPadNoiseSigma;
        fCellMap = ali.fCellMap;
        fRinger = ali.fRinger;
        fSeeds = ali.fSeeds;
        fParameters = ali.fParameters;
        fDebugMode = ali.fDebugMode;
	}
	return *this;
	
}

//____________________________________________________________________________//
AliFOCALClusterizerv2::~AliFOCALClusterizerv2()
{
        // Default destructor
	if (fSeeds) {
        fSeeds->Clear();
        delete fSeeds;
        fSeeds = 0;
	}
	delete fGeom;
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::InitGeometry(const char *geometryfile)
{
	fGeom = AliFOCALGeometry::GetInstance(geometryfile);
    fHCALSegID = -1;
    for (Int_t iSeg = 0; iSeg < fGeom->GetVirtualNSegments(); iSeg++) {
        if (fGeom->GetVirtualIsHCal(iSeg)) {
            fHCALSegID = iSeg;
            cout << "HCAL segment is " << fHCALSegID << endl;
        }
    }
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::InitGeometry()
{
	fGeom = AliFOCALGeometry::GetInstance();
    fHCALSegID = -1;
    for (Int_t iSeg = 0; iSeg < fGeom->GetVirtualNSegments(); iSeg++) {
        if (fGeom->GetVirtualIsHCal(iSeg)) {
            fHCALSegID = iSeg;
            cout << "HCAL segment is " << fHCALSegID << endl;
        }
    }
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::InitParameters(const char *parameterFile) {
    
  if (fGeom == 0) {
    cout << "ERROR: Geometry must be initialized, to load parameters!" << endl;
    return;
  }

  Int_t fileFormatVersion = -1;

  ifstream fin(parameterFile);
  if(!fin.good())
    {
      AliLog::Message(AliLog::kError,"No parameter file for FoCAL. Use default ones", "AliFOCALClusterizerv2.cxx", "AliFOCALClusterizerv.cxx", "InitParameters()", "AliFOCALClusterizerv.cxx", __LINE__);
      return ;
    }
  else
    {
      cout<<" Using parameterFile: "<< parameterFile << endl;
    }
	
  // Define parameters array
  fParameters = new Float_t * [fGeom->GetVirtualNSegments()];
	
  string input;
  const char *pbrk;
  TString delim(' ');
	
  cout<<" start loading parameter file "<<endl;
  while(getline(fin, input))
    {
      cout<<input.c_str()<<" "<<endl;
      pbrk=strpbrk("#",input.c_str());
      if(pbrk!=NULL)
	{
	  cout << "Skipping comment" << endl;
	  continue;
	}
		
      TString str(input.c_str());
      TObjArray *obj = str.Tokenize(delim);
		
      Int_t entries = obj->GetEntries();
		
      if (entries == 0)
	continue;
		
      TObjString *a0 = (TObjString*)obj->UncheckedAt(0);
      TString command(a0->GetString());
		
      if(command.Contains("PARAMETERS_SEGMENT")!=0) {
            
	int stack;
	char *cdata = (char*)command.Data();
	sscanf(cdata, "PARAMETERS_SEGMENT_N%d",&stack);
			
	if (stack < 0 || stack >= fGeom->GetVirtualNSegments()) {
	  cout << "Parameters: Segment " << stack << " out of bounds!" << endl;
	  continue;
	}
			
	TObjString * objstr;
	if (entries - 1 != kNPars)
	  AliFatal(Form("Wrong number of parameters for segment %d, expect %d", stack, kNPars));
	fParameters[stack] = new Float_t [kNPars];
	for (Int_t i = 1; i < entries ; i++) {
                
	  objstr = (TObjString*)obj->UncheckedAt(i);
	  fParameters[stack][i-1] = objstr->GetString().Atof();
	}
      }

      if(command.Contains("FILE_FORMAT_VERSION")!=0) {
	fileFormatVersion = ((TObjString*) obj->At(1))->GetString().Atoi();
      }
    }
	
  if (fileFormatVersion != kCurParFileFormat) { 
    AliFatal(Form("Wrong parameter file format; expect version %d", kCurParFileFormat));
    exit(10);
  }
  
  cout << "Parameters check:" << endl;
  for (Int_t i = 0; i < fGeom->GetVirtualNSegments(); i++) {
    
    if (!fParameters[i]) {
      cout << "  Segment " << i << ": parameters not defined." << endl;
      continue;
    }
    
    cout << "  Segment " << i << ":" << endl;
    cout << "    MinRing  = " << GetMinRing(i,0);
    cout << "    MaxRing  = " << GetMaxRing(i,0);
    cout << "    Weight1  = " << GetWeightingPar1(i);
    cout << "    Weight2  = " << GetWeightingPar2(i);
    cout << "    Weight3  = " << GetWeightingPar3(i);
    cout << "    Reject1  = " << GetRejectionRatio(i);
    cout << "    Reject2  = " << GetRejectionRatio2(i);
    cout << "    Treshold = " << GetSeedEnergyTreshold(i);
    cout << "  ClusterETh = " << GetClusterEnergyTreshold(i);
    cout << "    NCellsTh = " << GetNCellsThreshold(i) << endl;
  }
}

//____________________________________________________________________________//
Float_t AliFOCALClusterizerv2::GetHCALEnergy(Float_t x, Float_t y, Int_t dTow) {
  // Get HCAL energy around position x,y
  // Uses center tower +- dTow (so dTow = 1 is a 3x3 'cluster'
  //
  if ( fHCALSegID < 0 ) return 0;  
  Float_t segmentZ = fGeom->GetVirtualSegmentZ(fHCALSegID);
  Int_t col, row, layer, segment;
  if (!fGeom->GetVirtualInfo(x,y,segmentZ, col, row, layer, segment)) {
    AliWarning(Form("Asking for info at x=%f y=%f, outside detector",x,y));
    return 0;
  }

  Float_t eHCAL = 0;

  for (Int_t ccol = col-dTow; ccol <= col+dTow; ccol++) {
    for (Int_t crow = row-dTow; crow <= row+dTow; crow++) {
      AliFOCALCell *cell = GetCell(fHCALSegID, ccol, crow);
      if (cell)
	eHCAL += cell->E();
    }
  }
  return eHCAL;
}

//____________________________________________________________________________//
Float_t AliFOCALClusterizerv2::GetHCALIsoEnergy(Float_t x, Float_t y, Float_t Riso) {
  // Get HCAL energy around position x,y
  // Uses center tower +- dTow (so dTow == 1 is a 3x3 'cluster')
  //
  // Uses fRinger; state of fRinger changes...
  if ( fHCALSegID < 0 ) return 0; 
  Float_t segmentZ = fGeom->GetVirtualSegmentZ(fHCALSegID);
  Int_t col, row, layer, segment;
  if (!fGeom->GetVirtualInfo(x,y,segmentZ, col, row, layer, segment)) {
    AliWarning(Form("Asking for info at x=%f y=%f, outside detector",x,y));
    return 0;
  }


  Float_t eHCAL = 0;
 
  AliFOCALCell *cell = GetCell(fHCALSegID, col, row);
  if (cell)
    eHCAL = cell->E();
  else {
    AliInfo(Form("No HCAL cell found at center of ring: x %f y %f",x,y));
  }
  
  Int_t ring = 1;

  Float_t phiCent = TMath::ATan2(-y,x);
  Double_t rCent = TMath::Sqrt(x*x + y*y);
  Double_t theta = TMath::ATan(rCent/segmentZ);
  Float_t etaCent = TMath::Log(TMath::Tan(theta/2));
  
  Int_t nAdded = 0;
  do {
    nAdded = 0;
    fRinger->SetRing(ring);
    Int_t tX,tY;
    while(fRinger->GetNext(tX,tY)) {
      cell = GetCell(fHCALSegID,col+tX,row+tY);
      if (cell) {
	Float_t phiCell = TMath::ATan2(-cell->Y(), cell->X());
	Float_t dphi = phiCell-phiCent;
	if (dphi < -TMath::Pi())
	  dphi += 2*TMath::Pi();
	if (dphi > TMath::Pi())
	  dphi -= 2*TMath::Pi();
	if (dphi > Riso)
	  continue;

	Double_t rCell = TMath::Sqrt(cell->X()*cell->X() + cell->Y()*cell->Y());
	theta = TMath::ATan(rCell/segmentZ);
	Float_t etaCell =  TMath::Log(TMath::Tan(theta/2));       
      
	Float_t R2 = dphi*dphi + (etaCell-etaCent)*(etaCell-etaCent);
	if (R2 < Riso*Riso) {
          //cout << "Adding cell tX " << tX << " tY " << tY << " R2 " << R2 << endl;
	  eHCAL += cell->E();
	  nAdded ++;
	}
      }
    }
    AliDebug(3,Form("ring %d, nAdded %d",ring,nAdded));
    ring++;
  } while (nAdded);

  return eHCAL;
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::Hits2Clusters(Option_t * /*option*/)
{
	fNumberOfClusters = 0;
	fNumberOfClustersItr = 0;
	
	LoadHits2Cells();
	
	MakeClusters();
	
	FillClusterTree();
    
	
	AliDebug(1,Form("FOCAL Clusterizer found %d Cluster Points",fCluster->GetEntriesFast()));
	
        //	fClusterItr->Delete();
        //	fCluster->Delete();
        //	fPad->Delete();
        //	fCell->Delete();
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::Digits2Clusters(Option_t * /*option*/)
// load calibration parameters
{
        // load digits
        // pedestal extraction
        // convert adc to energy
        // and fill to fPad and fStrip
	LoadDigits2Cell();
	
	MakeClusters();
	
	FillClusterTree();
	
	AliDebug(1,Form("FOCAL Clusterizer found %d Rec Points",fCluster->GetEntriesFast()));
	
        //	fClusterItr->Delete();
        //	fCluster->Delete();
        //	fPad->Delete();
}

//____________________________________________________________________________
void AliFOCALClusterizerv2::LoadHits2Cells() {    
   AliDebug(1,"Hits2Cells called.");
	
   if (!branch) {
     AliError("AliFOCALClusterizerv2: LoadHits2Cells called, but input is not set!");
     return; // 0;
   }
	
   if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	
   AliFOCALDigitizer * digitizer = new AliFOCALDigitizer();
   digitizer->SetGeometry(fGeom);
   digitizer->SetPixelNoiseProb(fPixelNoiseProb);
   digitizer->SetPadNoiseSigma(fPadNoiseSigma);
   digitizer->Initialize("HD");
   digitizer->Hits2Digits(branch);
   fInputArray = digitizer->GetDigits();
	
   fCell->Clear();
   int fNCell=0;
    
   //  AliFOCALhit *focalhit = new AliFOCALhit();
   AliFOCALdigit * digit;
   AliDebug(1,Form("Number of digits: %d",fInputArray->GetEntries()));
   //Double_t totalLGLamp = 0;
   for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
     {
       digit = (AliFOCALdigit*)fInputArray->UncheckedAt(itrk);
       if (digit->GetAmp() <= 0)
	 continue;
       Float_t x,y,z;
       fGeom->GetXYZFromColRowSeg(digit->GetCol(), digit->GetRow(), digit->GetSegment(), x, y, z);
       new((*fCell)[fNCell]) AliFOCALCell(fNCell, digit->GetRow(), digit->GetCol(), digit->GetSegment(), -1,
					  x, y, z, (Float_t)digit->GetAmp());
       /*
       if (!fGeom->GetVirtualIsPixel(digit->GetSegment()))
	 totalLGLamp += digit->GetAmp();
       */
       fNCell++;
     }
	
	
   digitizer->SetGeometry(0); // Neccesary, or digitizer would delete it
   delete digitizer;
   fInputArray = 0;
   AliDebug(1,"Hits2Cells end");
   //return totalLGLamp;
 }

//____________________________________________________________________________
void AliFOCALClusterizerv2::LoadHits2CellsEmbedding() {
    
  AliDebug(1,"Hits2CellsEmbedding called.");
	
  if ((!branch)||(!fBackgroundBranch)) {
    AliError("AliFOCALClusterizerv2: LoadHits2CellsEmbedding called, but input or background is not set!");
    return;
  }
	
  if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	
  AliFOCALDigitizer * digitizer = new AliFOCALDigitizer();
  digitizer->SetGeometry(fGeom);
  digitizer->SetPixelNoiseProb(fPixelNoiseProb);
  digitizer->SetPadNoiseSigma(fPadNoiseSigma);
  digitizer->Initialize("HD");
  digitizer->Hits2DigitsEmbedding(branch,fBackgroundBranch);
  fInputArray = digitizer->GetDigits();
	
  fCell->Clear();
  int fNCell=0;
    
  //  AliFOCALhit *focalhit = new AliFOCALhit();
  AliFOCALdigit * digit;
  cout << "Number of digits: " << fInputArray->GetEntries() << endl;
  for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
    {
      digit = (AliFOCALdigit*)fInputArray->UncheckedAt(itrk);
      if (digit->GetAmp() <= 0)
	continue;
      Float_t x,y,z;
      fGeom->GetXYZFromColRowSeg(digit->GetCol(), digit->GetRow(), digit->GetSegment(), x, y, z);
      new((*fCell)[fNCell]) AliFOCALCell(fNCell, digit->GetRow(), digit->GetCol(), digit->GetSegment(), -1,
					 x, y, z, (Float_t)digit->GetAmp());
      fNCell++;
    }
	
	
  digitizer->SetGeometry(0); // Neccesary, or digitizer would delete it
  delete digitizer;
  fInputArray = 0;
  AliDebug(1, "Hits2CellsEmbedding end");
}


//____________________________________________________________________________//
void AliFOCALClusterizerv2::LoadDigits2Cell()
{
  AliDebug(1,"AliFOCALClusterizerv2::LoadDigits2Cell called.");
	
	if (!branch) {
        AliError("AliFOCALClusterizerv2: LoadDigits2Cell called, but input is not set!");
        return;
	}
	
	if (fGeom==0) AliFatal("Did not get geometry from FOCALLoader");
	fCell->Clear();
	int fNCell=0;
    
        //  AliFOCALhit *focalhit = new AliFOCALhit();
	AliFOCALdigit * digit;
	branch->SetAddress(&fInputArray);
        //	cout << "Entries = " << branch->GetEntries() << endl;
	for(int i=0;i<branch->GetEntries(); i++)
	{
		branch->GetEntry(i);
		cout << "Entries = " << fInputArray->GetEntries() << endl;
		for(int itrk=0; itrk<fInputArray->GetEntries(); itrk++)
		{
			digit = (AliFOCALdigit*)fInputArray->UncheckedAt(itrk);
			Float_t x,y,z;
			fGeom->GetXYZFromColRowSeg(digit->GetCol(), digit->GetRow(), digit->GetSegment(), x, y, z);
			new((*fCell)[fNCell]) AliFOCALCell(fNCell, digit->GetRow(), digit->GetCol(), digit->GetSegment(), -1,
                                               x, y, z, (Float_t)digit->GetAmp());
			fNCell++;
		}
		delete fInputArray;
		fInputArray = 0;
	}
	AliDebug(1,"AliFOCALClusterizerv2::LoadDigits2Cell done");
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::ClearClusterArrays() {
    
  AliDebug(1,"Clearing fields");
        // Clear Clusters and ClusterIter
    if (!fClusterItr)
        fClusterItr = new TClonesArray("AliFOCALCell",1000);
    
    fClusterItr->Clear();
    
    if (!fCluster)
        fCluster = new TClonesArray("AliFOCALCluster",1000);
    
    fCluster->Clear();
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::CreateAndFillCellMap() {
    
  // Create Map of Cells or Reset it, if it already exists
    Int_t numberOfSegments = fGeom->GetVirtualNSegments();
    
    //cout << "Creating Cellmap" << endl;
    if (!fCellMap) {
        fCellMap = new AliFOCALCellMap[numberOfSegments];
        for (Int_t i = 0; i < numberOfSegments; i++) {
            Int_t nCol, nRow;
            if (!fGeom->GetVirtualNColRow(i, nCol, nRow)) {
                AliError("fGeometry: Virtual Segment config mismatch!");
                continue;
            }
            fCellMap[i].CreateMap(i, nCol, nRow);
        }
    } else {
        fCellMap->ResetMap();
    }
    
        // Initialize Ringer
    if (!fRinger)
        fRinger = new AliFOCALRinger(500);
    
    if (fDebugMode)
       cout << "Filling Cellmap." << endl;
        // Fill the map from the TClonesArray
    for(Int_t i=0; i < fCell->GetEntries(); i++)
	{
		AliFOCALCell * cell  = (AliFOCALCell*)fCell->UncheckedAt(i);
		if (IsOverEnergyThreshold(cell->Segment(),cell->E())) {
            cell->SetCellCenter(true);
            AssignAmplitude(cell);
		}
		else
            cell->SetCellCenter(false);
		Int_t col = cell->Col();
		Int_t row = cell->Row();
		Int_t segment = cell->Segment();
		Int_t nCol,nRow;
		fGeom->GetVirtualNColRow(segment,nCol,nRow);
		fCellMap[segment].fCellMap[row*nCol + col] = cell;
	}
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::DeleteCellMap() {
    
        // Clean up
	delete [] fCellMap;
	fCellMap = 0;
}

//_____________________________________________________________________________
void AliFOCALClusterizerv2::ResetSeeds() {
    
    //cout << "Creating seeds TObjArray." << endl;
    if (!fSeeds)
        fSeeds = new TObjArray(1000);
    
    fSeeds->Clear();
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::MakeClustersPerSegment(Bool_t * doSegment, TObjArray * preTracks) {
    //
    // TODO: skip HCal segment
    //
    
  //cout << "Looking for seeds." << endl;
  // Find Seeds
  Int_t entries = fCell->GetEntries();
  for(Int_t i=entries-1; i >= 0; i--) // Loop from high to low energy
    {
      AliFOCALCell * cell  = (AliFOCALCell*)fCell->UncheckedAt(i);
		
      Int_t segment  = cell->Segment();
		
      if (segment == fHCALSegID || (doSegment && !doSegment[segment]))
	continue;
		
      //Check if the cell wasn't already rejected as seed
      if (!cell->Center())
	continue;
        
      Float_t energy = cell->E();
        
      // Check if the cell is over threshold
      if (!IsOverEnergyThreshold(segment,energy))
	break; // The cells are energy sorted, no other will be over the threshold
		
      AssignCellAsSeed(cell);
    }

  if (fDebugMode)	
    cout << "Found " << fSeeds->GetEntries() << " seeds" << endl;
    
  // Add pretracks at the end; after all seeds are found
  if (preTracks) {
        
    if (fDebugMode)	
      cout << "Using preTracks" << endl;
        
    Int_t nPreTracks = preTracks->GetEntries();
        
    // Insert seed to each (wanted) segment for each PreTrack
    for (Int_t seg = 0; seg < fGeom->GetVirtualNSegments(); seg++) {
      if (doSegment && !doSegment[seg])
	continue;
		    
      Float_t segmentZ = fGeom->GetVirtualSegmentZ(seg);
            
      for (Int_t ntrack = 0; ntrack < nPreTracks; ntrack++) {
                
	TVectorF * track = (TVectorF*) preTracks->UncheckedAt(ntrack);
                
	Float_t x = (*track)[0]*segmentZ + (*track)[1];
	Float_t y = (*track)[2]*segmentZ + (*track)[3];
                
	if (fDebugMode)
	  cout << Form("Pre-seed %i: Looking for seed at xy=[%.2f,%.2f].",ntrack,x,y) << endl;
                
	Int_t col, row, layer, segment;
	if (!fGeom->GetVirtualInfo(x,y,segmentZ, col, row, layer, segment)) {
	  if (fDebugMode)
	    cout << "Pre seed " << ntrack << " out of bounds for segment " << seg << "." << endl;
	  continue;
	}
                
	AliFOCALCell * cell = GetCell(seg, col, row);
                
	for (Int_t ring = 1; ring <= GetMinRing(seg,0); ring++) {
	  fRinger->SetRing(ring);
	  Int_t tX,tY;
	  while(fRinger->GetNext(tX,tY)) {
	    AliFOCALCell * cell2 = GetCell(seg,col+tX,row+tY);
	    if (cell2) {
	      if (!cell2->Center())
		continue;
	      if (!cell)
		cell = cell2;
	      else {
		if (cell2->E() > cell->E())
		  cell = cell2;
	      }
	    }
	  }
	}
                
	if (!cell) {
	  //		      cout << "Pre seed " << ntrack << ": no cell found for segment " << seg << ", skipping." << endl;
	  //		      Float_t tempX,tempY,tempZ;
	  //		      fGeom->GetXYZFromColRowSeg(col+tX, row+tY, seg, tempX, tempY, tempZ);
	  if (fDebugMode)
	    cout << Form("Pre seed %i: no cell found at xy=[%.2f,%.2f] (colrow[%i,%i]) for segment %i, skipping",ntrack,x,y,col,row,seg) << endl;
	  continue;
	}
                
	// Seed thresholds are now minimal, we do not want to consider anything below
	//                    // If the cell is not over treshold, assign an amplitude to it
	//                if (!cell->Center())
	//                    AssignAmplitude(cell);
                
	if (fDebugMode)
	  cout << Form("Pre-seed %i: Assigned seed energy = %.2f",ntrack, cell->SeedEnergy()) << endl;
                
	// cell->SetCellCenter(false); // Set cell as not center, so it would not be added again... // Not useful after swap of seed loop and pretrack loop
                
	AssignCellAsSeed(cell);
      }
    }
  }
    
  Bool_t doSecondRejection = false;
  for (Int_t i = 0; i < fGeom->GetVirtualNSegments(); i++)
    if (GetRejectionRatio2(i) > 0)
      doSecondRejection = true;
    
  if (doSecondRejection) {
    if (fDebugMode)	
       cout << "Rejecting seeds - second run:" << endl;
        
    Int_t rejected = 0;
    entries = fSeeds->GetEntries();
    for (Int_t i=entries-1; i >= 0; i--) {
      AliFOCALCell * cell  = (AliFOCALCell*)fSeeds->UncheckedAt(i);
            
      Float_t seedE  = cell->SeedEnergy();
      Int_t segment = cell->Segment();
            
            
      if (doSegment && !doSegment[segment])
	continue;
            
      // Rejection condition
      if (GetWeight(segment,0,seedE)/cell->Weight() < GetRejectionRatio2(segment)) {
                
	if (fDebugMode)
	  cout << "\t" << i << ": REJECTED: Ratio: " << GetWeight(segment,0,seedE) << "/" <<
	    cell->Weight() << ", x=" << cell->X() << " y=" << cell->Y() << ", E=" << cell->E() << endl;
                
	RejectCellAsSeed(cell);
	rejected++;
      }
      else
	if (fDebugMode)
	  cout << "\t" << i << ": ACCEPTED: Ratio: " << GetWeight(segment,0,seedE) << "/" <<
	    cell->Weight() << ", x=" << cell->X() << " y=" << cell->Y() << ", E=" << cell->E() << endl;
            
    }
        
    cout << "Rejected in second run: " << rejected << " seeds." << endl;
    // Compress seeds (remove empty possitions)
    fSeeds->Compress();
  }
	
  if (fDebugMode)	
    cout << "Applying additional rejection criteria (on TotalE and NCells)" << endl;
    
  // Reject clusters based on total number of Cells and Total Energy
  entries = fSeeds->GetEntries();
  Int_t rejectedTotal = 0;
  Int_t rejectedEnergy = 0;
  Int_t rejectedNCells = 0;
  Int_t rejectedShape = 0;
  for (Int_t i = 0; i < entries; i++) {  // The seed array is ordered high-low, so start from entry 0; pretracks are at the end
    AliFOCALCell * cell  = (AliFOCALCell*)fSeeds->UncheckedAt(i);
        
    if (!cell) {
      if (fDebugMode)
	cout << Form("\tCell %i missing!", i) << endl;
      continue;
    }
    // load info
    Float_t energy = cell->E();
    Float_t seedE  = cell->SeedEnergy();
    Int_t col = cell->Col();
    Int_t row = cell->Row();
        
    Int_t segment = cell->Segment();

    /*
    if (fDebugMode)
      cout << "Seed segment " << cell->Segment() << " row " << cell->Row() << " " << cell->Col() << " do: " << (doSegment[segment]?1:0) << endl;
    */
    if (doSegment && !doSegment[segment])
      continue;
		
    // Add weighted part of seed's energy/position to total cluster energy/position
    Float_t en = energy*GetWeight(segment,0,seedE)/cell->Weight(); // 
    Float_t seedEwght = en; 

    Float_t totalX = cell->X()*en;
    Float_t totalY = cell->Y()*en;
    Float_t totalZ = cell->Z()*en;
    Float_t totalE = en;
    Float_t totalWeight = en;
    Float_t numberOfCells = 1;
		
    // Get minial and maximal cluster radius for given segment, energy
    Int_t maxRing = GetMaxRing(segment, seedE);
        
    for (Int_t j = 1 ; j <= maxRing ; j++) {
      Float_t weight = GetWeight(segment,j,seedE);
      fRinger->SetRing(j);
      Int_t x,y;
            

      while (fRinger->GetNext(x,y)) {
	if(modeCorrectGW)
	  weight = GetWeightCorrected(segment,TMath::Sqrt((Float_t)(x)*x+y*y),seedE);
	AliFOCALCell * neighbor = GetCell(segment,col+x,row+y);
	if (neighbor && neighbor->Weight() > 0) {
	  en = neighbor->E()*weight/neighbor->Weight();
	  if (j <= 3) {
	    totalX += neighbor->X()*en;
	    totalY += neighbor->Y()*en;
	    totalZ += neighbor->Z()*en;
	    totalWeight += en;
	  }
	  totalE += en;
	  numberOfCells+=weight/neighbor->Weight();
	  // Kept following for future possible rejections
	  //              cout<<neighbor->X()<<"\t"<<neighbor->Y()<<"\t"<<en<<endl;
	  // Width calculation - individual cells
	  //                    xm+=en*neighbor->X();
	  //                    ym+=en*neighbor->Y();
	  //                    xsquaredm+=en*neighbor->X()*neighbor->X();
	  //                    ysquaredm+=en*neighbor->Y()*neighbor->Y();
	  //                    xym+=en*neighbor->X()*neighbor->Y();
	  //                    countneighbor++;
	}
      }
    }
        
    bool willBeRejected = false;
    bool nRejection = false;
    bool eRejection = false;
    bool sRejection = false;
        
    if (numberOfCells < GetNCellsThreshold (segment)) {
      willBeRejected = true; 
      rejectedNCells++;
      nRejection = true;   
    }
        
    if (totalE < GetClusterEnergyTreshold(segment)) {
      willBeRejected = true;
      eRejection = true;
      rejectedEnergy++;          
    }
        
    // Shape cut
    if (seedEwght/totalE < GetShapeThreshold(segment)) {
      willBeRejected = true;
      sRejection = true;
      rejectedShape++;
    }
        
    if (willBeRejected) {
      if (cell->Center()) {
	RejectCellAsSeed(cell);
	rejectedTotal++;
	if (fDebugMode)
	  cout << "\tCLUSTER_REJECTED: " << Form("Seg=%i\tseedE=%.1f\tE=%.1f\tNCells=%.1f\tx=%f\ty=%f\t(E:%o,N:%o,S:%o)", 
						 segment, seedEwght, totalE, numberOfCells, totalX/totalWeight, totalY/totalWeight,
						 eRejection, nRejection, sRejection) << endl;
      } else {
	if (fDebugMode)
	  cout << "\tCLUSTER_REJECTED_OVERRIDE: " << Form("Seg=%i\tseedE=%.1f\tE=%.1f\tNCells=%.1f\tx=%f\ty=%f\t(E:%o,N:%o,S:%o)\t\tNot rejected <= create by preseed.", 
							  segment, seedEwght, totalE, numberOfCells, totalX/totalWeight, totalY/totalWeight,
							  eRejection, nRejection, sRejection) << endl;
      }
    }
        
  }
    
  fSeeds->Compress();
    
  cout << Form("Rejected %i clusters based on additional criteria (Energy: %i,NCells: %i, Shape: %i)",
	       rejectedTotal,rejectedEnergy,rejectedNCells,rejectedShape) << endl;
    
  cout << "Summing clusters." << endl;
    
  //	----------------------------- implementing Terry's Log width calculation in section below --------------------    
    
  // SumUp clusters
  entries = fSeeds->GetEntries();
  for (Int_t i=0; i < entries; i++) {
    AliFOCALCell * cell  = (AliFOCALCell*)fSeeds->UncheckedAt(i);
	    
    // load info
    Float_t energy = cell->E();
    Float_t seedE  = cell->SeedEnergy();
    Int_t col = cell->Col();
    Int_t row = cell->Row();
    Int_t segment = cell->Segment();
		
    if (doSegment && !doSegment[segment])
      continue;
		
    // Add weighted part of seed's energy to total cluster energy/position
    Float_t en = energy*GetWeight(segment,0,seedE)/cell->Weight(); // 
    Float_t seedEwght = en; 
    Float_t totalE = en;
		  
    // Get minial and maximal cluster radius for given segment, energy
    Int_t maxRing = GetMaxRing(segment, seedE);		    
		    
    // loop over neighbors in order to compute total energy of clusters
    for (Int_t j = 1 ; j <= maxRing ; j++) {
      Float_t weight = GetWeight(segment,j,seedE);
      fRinger->SetRing(j);
      Int_t x,y;
            
      while (fRinger->GetNext(x,y)) {
	if(modeCorrectGW)
	  weight = GetWeightCorrected(segment,TMath::Sqrt((Float_t)(x)*x+y*y),seedE);
	AliFOCALCell * neighbor = GetCell(segment,col+x,row+y);
	if (neighbor && neighbor->Weight() != 0){
	  en = neighbor->E()*weight/neighbor->Weight();
	  totalE += en;
	}
      }
    }		    
        
        
    Float_t w_0;
    bool mode, isPad;
    isPad = !fGeom->GetVirtualIsPixel(segment);
    if (isPad) { // then it is pad
      w_0 = wC_0;
      mode = modeC;
    } else {
      w_0 = wF_0;
      mode = modeF;
    }

    // cout << " seed energy " << energy << " after wght " << en << endl;
                                                                           
    // Terry's Logarithmic energy weight contributions:    
    Float_t enLW = 0;
    if (isPad) {
      if(cell->Weight() != 0){ 
	enLW = cell->LogWeightE(totalE*cell->Weight()/GetWeight(segment,0,seedE),w_0,mode); // 
      } 
    }
    else {
      if(cell->Weight() != 0){
	enLW = cell->PowWeightE(totalE*cell->Weight()/GetWeight(segment,0,seedE),w_0,mode); // 
      }
    }
		    
    Float_t totalX = cell->X()*enLW;
    Float_t totalY = cell->Y()*enLW;
    Float_t totalZ = cell->Z()*enLW;
    Float_t totalELW = enLW; 
    Float_t totalWeight = enLW;
    Float_t numberOfCells = 1;
		
    // Get minial and maximal cluster radius for given segment, energy
    //        Int_t minRing = GetMinRing(segment, seedE); // already computed above 
    //        Int_t maxRing = GetMaxRing(segment, seedE);
         
    // Width calculation - prepare calculation
    // cout<<"!!!!!!!!!!!!!!!!\nbefore adding the neighbours contribution\n!!!!!!!!!!!!!!!"<<endl;
    Double_t xm=cell->X();
    Double_t ym=cell->Y();
    Double_t xsquaredm=cell->X()*cell->X();
    Double_t ysquaredm=cell->Y()*cell->Y();
    Double_t xym=cell->X()*cell->Y();
    // cout<<"xm  "<<xm<<"\tym  "<<ym<<"\txsquaredm  "<<xsquaredm<<"\tysquaredm  "<<ysquaredm<<"\txym  "<<xym<<endl;	
         
    Double_t sigmaxx=xsquaredm - xm*xm;
    Double_t sigmayy=ysquaredm - ym*ym;
    Double_t sigmaxy=xym - xm*ym;
    // cout<<"sigmaxx  "<<sigmaxx<<"\tsigmayy  "<<sigmayy<<"\tsigmaxy  "<<sigmaxy<<"\tenergy  "<<totalELW<<endl;
         
    Double_t Width1=0;
    Double_t Width2=0;
    Double_t Phi=4.0;
    // cout<<"Width1  "<<Width1<<"\tWidth2   "<<Width2<<endl;
    xm=totalELW*cell->X();
    ym=totalELW*cell->Y();
    xsquaredm=totalELW*cell->X()*cell->X();
    ysquaredm=totalELW*cell->Y()*cell->Y();
    xym=totalELW*cell->X()*cell->Y();
    Int_t countneighbor=0;
    Double_t dif=0;
    Double_t svar=0;
     
    // loop over neighbors and add them into the cluster
    for (Int_t j = 1 ; j <= maxRing ; j++) {
      Float_t weight = GetWeight(segment,j,seedE);
      fRinger->SetRing(j);
      Int_t x,y;
            

      while (fRinger->GetNext(x,y)) {
	if(modeCorrectGW)
	  weight = GetWeightCorrected(segment,TMath::Sqrt((Float_t)(x)*x+y*y),seedE);
                  
	AliFOCALCell * neighbor = GetCell(segment,col+x,row+y);
	if (neighbor) {
	  if(isPad) {
	    if(neighbor->Weight() != 0){ //Camila
	      enLW = neighbor->LogWeightE(totalE*neighbor->Weight()/weight,w_0,mode);
	    }
	  } 
	  else {
	    if(neighbor->Weight() != 0){ //Camila
	      enLW = neighbor->PowWeightE(totalE*neighbor->Weight()/weight,w_0,mode);
	    }
	  }
	  if (j <= 3) {
	    totalX += neighbor->X()*enLW;
	    totalY += neighbor->Y()*enLW;
	    totalZ += neighbor->Z()*enLW;
	    totalWeight += enLW;
	  }
	  if(neighbor->Weight() != 0){ //Camila
	    numberOfCells+=weight/neighbor->Weight();
	  }
	  totalELW += enLW;
	  //              cout<<neighbor->X()<<"\t"<<neighbor->Y()<<"\t"<<enLW<<enLWdl;
	  // Width calculation - individual cells
	  xm+=enLW*neighbor->X();
	  ym+=enLW*neighbor->Y();
	  xsquaredm+=enLW*neighbor->X()*neighbor->X();
	  ysquaredm+=enLW*neighbor->Y()*neighbor->Y();
	  xym+=enLW*neighbor->X()*neighbor->Y();
	  countneighbor++;
	}
      }
    }
    //cout<<"number of cells for this cluster=================== "<<countneighbor+1<<endl;
    //cout<<"total energy for this cluster====================== "<<totalELW<<endl;
    // Width calculation
    if(totalELW != 0){ //Camila
      xm/=totalELW;
      ym/=totalELW;
      xsquaredm/=totalELW;
      ysquaredm/=totalELW;
      xym/=totalELW;
    }

    sigmaxx = xsquaredm - xm*xm;
    sigmayy = ysquaredm - ym*ym;
    sigmaxy = xym - xm*ym;
        
    //cout<<"xm  "<<xm<<"\tym  "<<ym<<"\txsquaredm  "<<xsquaredm<<"\tysquaredm  "<<ysquaredm<<"\txym  "<<xym<<endl;
    //cout<<"sigmaxx  "<<sigmaxx<<"\tsigmayy  "<<sigmayy<<"\tsigmaxy  "<<sigmaxy<<endl;
        
    // Shape cut (only for coarse layers)
    if(totalWeight != 0){ //Camila
      new((*fClusterItr)[fClusterItr->GetEntries()]) AliFOCALCluster(totalX/totalWeight, totalY/totalWeight, totalZ/totalWeight, totalE, segment, seedEwght, numberOfCells);

      if (fDebugMode)
	cout << "\tCLUSTER_FOUND:    " << Form("Seg=%i\tseedE=%.1f\tE=%.1f\tNCells=%.1f\tx=%f\ty=%f", 
					       segment, seedEwght, totalE,numberOfCells,totalX/totalWeight, totalY/totalWeight) << endl;

      dif=sigmayy-sigmaxx;
      //cout<<"dif  "<<dif;
      svar=TMath::Sqrt(dif*dif + 4*sigmaxy*sigmaxy);
      //cout<<"\tsvar  "<<svar<<endl;
      //cout<<"sigmaxx+sigmayy  "<<sigmaxx+sigmayy<<endl;

      Double_t num1=sigmaxx+sigmayy+svar;
      Width1 = TMath::Sqrt(TMath::Abs(num1)/2.);
      if(num1<0.){
	Width1 = Width1*(-1.);
	if (fDebugMode)
	  cout << Form("\t\tWarning: Num1 less than zero: Segment=%i\tE=%.2e\tX=%.2f\tY=%.2f",
		       segment,totalE,totalX/totalWeight,totalY/totalWeight) << endl;
      }

      //cout<<"Width1  "<<Width1;

      Double_t num2=sigmaxx+sigmayy-svar;
      Width2 = TMath::Sqrt(TMath::Abs(num2)/2.);
      if(num2<0.){
	Width2 = Width2*(-1.);	
	if (fDebugMode)
	  if(totalWeight != 0){  ///Camila
	    cout << Form("\t\tWarning: Num2 less than zero: Segment=%i\tE=%.2e\tX=%.2f\tY=%.2f",
			 segment,totalE,totalX/totalWeight,totalY/totalWeight) << endl;
	  }
      }  

      if (num1<0. || num2<0.) {
	Phi = -4.0;
      } else {
	if((2.0*sigmaxy*ym-(dif-svar)*xm) != 0){  ///Camila
	  Phi = TMath::ATan((2.0*sigmaxy*xm+(dif+svar)*ym)/(2.0*sigmaxy*ym-(dif-svar)*xm));        
	}
      }
        
      //cout<<"\tWidth2   "<<Width2<<endl; 

      // Width calculation - Here you need to set the calculated width to created cluster, set also energy

      AliFOCALCluster *newClust = (AliFOCALCluster*)fClusterItr->At(fClusterItr->GetEntries()-1);
      newClust->SetSegIndex(0,segment);
      newClust->SetWidth1(0,(Float_t)Width1);
      newClust->SetWidth2(0,(Float_t)Width2);
      newClust->SetPhi(0,(Float_t)Phi);
      newClust->SetSegmentEnergy(0,totalE);
      newClust->SetNcells(0,numberOfCells);
      newClust->SetSeedEnergy(0,seedEwght);
    }
  }
  cout << "\t\tFound " << fClusterItr->GetEntries() << " sub-clusters" << endl;    
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::MakeHCALClusters() {
    //
    // For now a very simple algorithm, just copy all HCAL cell information to output clusters
    //
    if ( fHCALSegID < 0 ) return;
    Float_t *calPars = GetCalibrationPars(fHCALSegID);
    Int_t entries = fCell->GetEntries();
    for(Int_t i=entries-1; i >= 0; i--) { // Loop from high to low energy
        
        AliFOCALCell * cell  = (AliFOCALCell*)fCell->UncheckedAt(i);
        
        Int_t segment  = cell->Segment();
		
        if (segment != fHCALSegID) 
            continue;

        // Make new cluster
        Double_t calibE = cell->E() / calPars[0]; // uses only linear part of aclibration
        new((*fClusterItr)[fClusterItr->GetEntries()]) AliFOCALCluster(cell->X(), cell->Y(), cell->Z(), calibE, segment, 1., 1);
    }
    
}

//____________________________________________________________________________//
void AliFOCALClusterizerv2::MakeClusters()
{
    cout << "\tMakeClusters called." << endl;
        // Check Things
    cout << "\t\tChecking fCell" << endl;
    if (!fCell) {
        AliError("AliFOCALCLusterizerv2::MakeClusters(): fCell is empty!");
        return;
    }
    
    ClearClusterArrays();
    
        // Sort cells
    cout << "Sorting cells." << endl;
    SortCells();
    
    CreateAndFillCellMap();
    
    ResetSeeds();
    
    MakeClustersPerSegment(0,0);
	
    MakeHCALClusters();

	DeleteCellMap();
}

//_____________________________________________________________________________
void AliFOCALClusterizerv2::SaveImage(char * name) {
    
    cout << "Saving of " << name << " started" << endl;
    
    Int_t nSegments = fGeom->GetVirtualNSegments();
    TH2F * * histograms = new TH2F * [nSegments];
    TH2F * * histogramsWeight = new TH2F * [nSegments];
    TPolyMarker * * pm = new TPolyMarker * [nSegments];
    TPolyMarker * * pmSeeds = new TPolyMarker * [nSegments];
    TPolyMarker * * pmPhotons = new TPolyMarker * [nSegments];
    Int_t * positions = new Int_t[nSegments];
    Int_t * positionsSeeds = new Int_t[nSegments];
    Int_t * positionsPhotons = new Int_t[nSegments];
    Int_t col, row;
    for (Int_t i = 0; i < nSegments; i++) {
        fGeom->GetVirtualNColRow(i,col,row);
        Float_t sizeX = fGeom->GetFOCALSizeX();
        Float_t sizeY = fGeom->GetFOCALSizeY();
        
        histograms[i] = new TH2F(Form("%s_%i",name,i),Form("%s_%i",name,i),col,-1*sizeX/2,sizeX/2,row,-1*sizeY/2,sizeY/2);
        histogramsWeight[i] = new TH2F(Form("%sWeight_%i",name,i),Form("%sWeight_%i",name,i),col,-1*sizeX/2,sizeX/2,row,-1*sizeY/2,sizeY/2);
        
        pm[i] = new TPolyMarker();
        positions[i] = 0;
        pmSeeds[i] = new TPolyMarker();
        positionsSeeds[i] = 0;
        pmPhotons[i] = new TPolyMarker();
        positionsPhotons[i] = 0;
    }
    
    cout << "Filling histograms" << endl;
    
    for(Int_t i=0; i < fCell->GetEntries(); i++) {
		AliFOCALCell * cell  = (AliFOCALCell*)fCell->UncheckedAt(i);
		Float_t x = cell->X();
		Float_t y = cell->Y();
		Int_t segment = cell->Segment();
		Float_t energy = cell->E();
		Float_t weight = cell->Weight();
		histograms[segment]->Fill(x,y,energy);
		histogramsWeight[segment]->Fill(x,y,weight);
	}
	
	cout << "Filling clusters" << endl;
	
	Int_t nClusters = fClusterItr->GetEntries();
    for (Int_t i = 0; i < nClusters; i++) {
        AliFOCALCluster * cluster = (AliFOCALCluster*) fClusterItr->UncheckedAt(i);
        Int_t segment = cluster->Segment();
        if (segment < 0)
            continue;
        pm[segment]->SetPoint(positions[segment]++,cluster->X(),cluster->Y());
    }
    
    cout << "Filling seeds" << endl;
	
	Int_t nSeeds = fSeeds->GetEntries();
    for (Int_t i = 0; i < nSeeds; i++) {
        AliFOCALCell * cell = (AliFOCALCell*) fSeeds->UncheckedAt(i);
        Int_t segment = cell->Segment();
        pmSeeds[segment]->SetPoint(positionsSeeds[segment]++,cell->X(),cell->Y());
    }
    
    cout << "Getting kinematics" << endl;
    AliRunLoader *rl = AliRunLoader::Instance();
    Int_t nphot = 0;
    Float_t x0_disp = 0;
    Float_t y0_disp = 0;
    Float_t e_phot_max = 0;
    if (rl->Stack()) {
        AliStack *stack = rl->Stack();
        Int_t npart = stack->GetNtrack();
        cout << "ntrack " << npart << endl;
        for (Int_t ipart = 0; ipart < npart; ipart++) {
            TParticle *part = stack->Particle(ipart);
            if (part->Pz() <= 0) // Skip particles flying in negative z direction
                continue;
            if (part->GetPdgCode()==22 && part->GetMother(0) < stack->GetNprimary()) {// photon
                nphot++;
                for (Int_t iSeg = 0; iSeg < nSegments; iSeg++) {
                    Float_t dz = fGeom->GetFOCALSegmentZ(iSeg) - part->Vz();
                    Float_t x =  part->Vx() + part->Px()/part->Pz() * dz;
                    Float_t y =  part->Vy() + part->Py()/part->Pz() * dz;
                    cout << "Photon at " << x << " " << y << " " << dz << endl;
                    pmPhotons[iSeg]->SetPoint(positionsPhotons[iSeg]++,x,y);
                    if (part->Energy() > e_phot_max) {
                        e_phot_max = part->Energy();
                        x0_disp = x;
                        y0_disp = y;
                    }
                }
            }
        }
    }
    else
        cout << "No Stack found " << endl;
    
    cout << "Drawing" << endl;
    
    gStyle->SetPalette(1);
    TCanvas * c1 = new TCanvas("c1_xx","c1_xx: event display",1000,600);
    c1->Divide(3,2);
        //c1->SetLogz(1);
    
    for (Int_t i = 0; i < nSegments; i++) {
        c1->cd(i+1);
        
        cout << "adding pm" << endl;
        histograms[i]->GetListOfFunctions()->Add(pm[i]);
        histograms[i]->GetListOfFunctions()->Add(pmSeeds[i]);
        histograms[i]->GetListOfFunctions()->Add(pmPhotons[i]);
        cout << "stopped adding pm" << endl;
        
        histograms[i]->SetXTitle("x [cm]");
        histograms[i]->SetYTitle("y [cm]");
        histograms[i]->SetStats(0);
        
        histogramsWeight[i]->SetXTitle("x [cm]");
        histogramsWeight[i]->SetYTitle("y [cm]");
        histogramsWeight[i]->SetStats(0);
        
        cout << "setting up pm" << endl;
        
        pm[i]->SetMarkerStyle(4);
        pm[i]->SetMarkerColor(kBlack);
        pm[i]->SetMarkerSize(1.5);
        
        cout << "setting up pmSeeds" << endl;
        
        pmSeeds[i]->SetMarkerStyle(5);
        pmSeeds[i]->SetMarkerColor(kBlack);
        pmSeeds[i]->SetMarkerSize(1.5);
        
        cout << "setting up pmPhotons" << endl;
        
        pmPhotons[i]->SetMarkerStyle(25);
        pmPhotons[i]->SetMarkerColor(kBlack);
        pmPhotons[i]->SetMarkerSize(1.5);
        
        cout << "drawing..." << endl;
        
        histograms[i]->GetXaxis()->SetRangeUser(x0_disp - 5.0, x0_disp+5.0);
        histograms[i]->GetYaxis()->SetRangeUser(y0_disp - 5.0, y0_disp+5.0);
        histograms[i]->Draw("colz");
        
        
            //histogramsWeight[i]->Draw("colz");
            //c1->SetName(Form("%sWeight_%i",name,i));
            //c1->SaveAs(Form("%sWeight_%i.root",name,i));
        
            //    delete pm[i];       // Deleted by histogram
            //    delete pmSeeds[i];  // Deleted by histogram
        
    }
    cout<< "saving " << c1->GetName() << endl;
    c1->SetName(Form("%s_all",name));
    c1->SaveAs(Form("%s_all.root",name));
    
    for (Int_t i = 0; i < nSegments; i++) {
        delete histograms[i];
        delete histogramsWeight[i];
    }
    delete [] histograms;
    delete [] histogramsWeight;
    delete [] pm;
    delete [] pmSeeds;
    delete [] positions;
    delete [] positionsSeeds;
    
    cout << "Saving of " << name << " stopped" << endl;
    
}

//___________________________________________________________________//
void AliFOCALClusterizerv2::AssignCellAsSeed(AliFOCALCell * cell) {
    
    // First check whether not already in list
    TIter seedIt(fSeeds);
    while (TObject *cell2 = seedIt()) {
      if (cell2 == cell)
         return; // Cell already in seed list
    }
    fSeeds->AddLast(cell);
    
    Int_t segment  = cell->Segment();
	Float_t energy = cell->E();
	Float_t seedE  = cell->SeedEnergy();
        if (seedE == 0) { // Calculate Seed energy (needed for seeds below threshold that are used with preSeeds/preTracks
           AssignAmplitude(cell);
           seedE = cell->SeedEnergy(); 
        } 
    
	cell->AddWeight(GetWeight(segment,0,seedE));
	
        // load info
	Int_t col = cell->Col();
	Int_t row = cell->Row();
	
	
        // Get minial and maximal cluster radius for given segment, energy
	Int_t minRing = GetMinRing(segment, seedE);
	Int_t maxRing = GetMaxRing(segment, seedE);
	
	if (fDebugMode)
	  cout << Form("\tAssigning seed: Segment=%i\tx=%.2f\ty=%.2f\tcol=%i\trow=%i\tE=%.2f\tseedE=%.2f",
	                  segment,cell->X(),cell->Y(),col,row,energy,seedE) << endl;
	
        // Assign seeds, reject close ones and assign weights
	for (Int_t j = 1 ; j <= maxRing ; j++) {
        Float_t weight = GetWeight(segment,j,seedE);
            //		  cout << i << ": " << j << ": weight=" << weight << endl;
        fRinger->SetRing(j);
        Int_t x,y;
        while (fRinger->GetNext(x,y)) {
                //	      cout << x << "," <<  y << endl;
	    if(modeCorrectGW)
	      weight = GetWeightCorrected(segment,TMath::Sqrt((Float_t)(x)*x+y*y),seedE);
            AliFOCALCell * neighbor = GetCell(segment,col+x,row+y);
            if (neighbor) {
                Float_t neighborWeight = GetWeight(neighbor->Segment(),0,neighbor->SeedEnergy());
                if (j <= minRing) {
                    if (fDebugMode)
                            cout << "\t\t" << "PREREJECTED: Ratio: " << neighborWeight << "/" <<
                            weight << ", (j=" << j << " seedE=" << seedE << "), x=" << neighbor->X() << " y=" << neighbor->Y() << ", E=" << neighbor->E() << endl;
                    neighbor->SetCellCenter(false);
                }
                else if ((GetRejectionRatio(segment) > 0)&&(neighbor->Center())) {
                        // Rejection condition
                    if (neighborWeight/weight < GetRejectionRatio(segment)) {
                        
                        if (fDebugMode)
                            cout << "\t\t" << "PREREJECTED: Ratio: " << neighborWeight << "/" <<
                            weight << ", (j=" << j << " seedE=" << seedE << "), x=" << neighbor->X() << " y=" << neighbor->Y() << ", E=" << neighbor->E() << endl;
                        neighbor->SetCellCenter(false);
                    }
                    else
                        if (fDebugMode)
                            cout << "\t\t" << "PREACCEPTED: Ratio: " << neighborWeight << "/" <<
                            weight << ", (j=" << j << " seedE=" << seedE << "), x=" << neighbor->X() << " y=" << neighbor->Y() << ", E=" << neighbor->E() << endl;
                }
                neighbor->AddWeight(weight);
                    //		      cout << "Adding weight to " << col+x << "," << row+y << endl;
            }
        }
	}
}

//___________________________________________________________________//
void AliFOCALClusterizerv2::RejectCellAsSeed(AliFOCALCell * cell) {
    
        // load info
    Int_t col = cell->Col();
    Int_t row = cell->Row();
	Int_t segment = cell->Segment();
	Float_t seedE = cell->SeedEnergy();
	
        // remove selfweight
	cell->AddWeight(-1*GetWeight(segment,0,seedE));
	
        // mark as not seed
	cell->SetCellCenter(false);
	
        // Get minial and maximal cluster radius for given segment, energy
    Int_t maxRing = GetMaxRing(segment, seedE);
    
        //		  cout << "\t\t\tStarted unweighting." << endl;
        //Remove weights added by this seed
    for (Int_t j = 1 ; j <= maxRing ; j++) {
        Float_t weight = GetWeight(segment,j,seedE);
        fRinger->SetRing(j);
        Int_t x,y;
        while (fRinger->GetNext(x,y)) {
	    if(modeCorrectGW)
	      weight = GetWeightCorrected(segment,TMath::Sqrt((Float_t)(x)*x+y*y),seedE);
	    AliFOCALCell * neighbor = GetCell(segment,col+x,row+y);
	    if (neighbor) {
	      neighbor->AddWeight(-1*weight);
	    }
        }
    }
        // Remove cell from list of seeds
    fSeeds->Remove(cell);
}

//________________________________________________________________________
// function to relate cluster of segments
void AliFOCALClusterizerv2::RunCluster(int Segments, TObjArray *cluster)
{
  float x0, y0, z0;
  float x1, y1, z1;
  float new_e;
  float new_x;
  float new_y;
  float new_z;
  AliFOCALCluster *clus = new AliFOCALCluster();
  AliFOCALCluster *clus1 = new AliFOCALCluster();
    
  // loop on segments
  for(int iSeg = 0; iSeg <= max_segment; iSeg++)
    {
      new_e = new_x = new_y = new_z = 0;
      int max_entrie_iSeg = cluster[iSeg].GetEntriesFast();
        
      // loop on cluster of primary segments
      for(int i = 0; i < max_entrie_iSeg; i++)
        {
	  x0 = y0 = z0 = 0;
	  clus = (AliFOCALCluster*) cluster[iSeg].UncheckedAt(i);
            
	  // if this object it was already used continue to the next
	  if(!(clus->Flag()))
            {
	      continue;
            }
            
	  z0 = fGeom->GetFOCALSegmentZ(iSeg);
	  x0 = clus->X();
	  y0 = clus->Y();
            
	  new_x += x0*clus->E();
	  new_y += y0*clus->E();
	  new_z += z0*clus->E();
	  new_e += clus->E();
            
	  // loop on segments foward segment before
	  for(int jSeg = iSeg+1; jSeg <= max_segment; jSeg++)
            {
	      int max_entrie_jSeg = cluster[jSeg].GetEntriesFast();
                
	      // loop on seconds, third, etc segments
	      for(int j = 0; j < max_entrie_jSeg; j++)
                {
		  x1 = y1 = z1 = 0;
		  clus1 = (AliFOCALCluster*) cluster[jSeg].UncheckedAt(j);
                    
		  if(!(clus1->Flag()))
                    {
		      continue;
                    }
                    
		  z1 = fGeom->GetFOCALSegmentZ(jSeg);
		  x1 = clus1->X();
		  y1 = clus1->Y();
                    
		  float dx = x1 - x0/(z0)*z1;
		  float dy = y1 - y0/(z0)*z1;
                    
		  if(-fDistance<dx && dx<fDistance && -fDistance<dy && dy<fDistance)
                    {
		      new_x += x1*clus1->E();
		      new_y += y1*clus1->E();
		      new_z += z1*clus1->E();
		      new_e += clus1->E();
                        
		      clus1->SetFlag(false);
		      clus1->Clear();
		      break;
                        
                    }
		  clus1->Clear();
                }
            }
            
	  AliFOCALCluster *clst = new AliFOCALCluster(new_x/new_e, new_y/new_e, new_z/new_e, new_e);
	  cluster[Segments].AddLast(clst);
	  clst->Clear();
	  clus->SetFlag(false);
	  clus->Clear();
        }
    }
}

//_____________________________________________________________________________
bool AliFOCALClusterizerv2::Neighboring_Cells(AliFOCALCell *twr1, AliFOCALCell *twr2)
{
	float dx = twr1->X()-twr2->X();
	float dy = twr1->Y()-twr2->Y();
	
	if(twr1->Segment()!=twr2->Segment())
	{
		return false;
	}
    
	if(dx>-5 && dx<5 && dy>-5 && dy<5)
	{
		return true;
	}
	return false;
}

//_____________________________________________________________________________
void AliFOCALClusterizerv2::AnalyzeEvent(TTree *tree, int type)
{
        // type = 0 -> execute the Digits2Cluster method
        // type = 1 -> execute the Hits2Cluster method
	switch(type)
	{
		case 0:
			SetInput(tree, "DIGITS");
			Digits2Clusters("sum");
			break;
		case 1:
			SetInput(tree, "HITS");
			Hits2Clusters("sum");
			break;
		default:
			cout << "Choose betwen digits (0) or hits (1)\n";
	}
}

//____________________________________________________________________________
AliFOCALCell * AliFOCALClusterizerv2::GetCell(Int_t seg, Int_t col, Int_t row) {
    
  //  if (!fDigits || !fDigitMap){
  //    AliDebug(1,Form("AliFOCALDigitizer::GetDigit: not initialized));
  //    return 0;
  //  }
    
    Int_t nCol, nRow, nSeg;
    nSeg = fGeom->GetVirtualNSegments();
    fGeom->GetVirtualNColRow(seg,nCol,nRow);
    
    if ( (col >= nCol) || (row >= nRow) || (seg >= nSeg) || (col < 0) || (row < 0) || (seg < 0) ) {
            //    AliDebug(1,Form("AliFOCALClusterizerv2::GetCell: indexes %i,%i,%i out of bounds",seg,col,row));
        return 0;
    }
    
    Long_t index = row*nCol + col;
    return fCellMap[seg].fCellMap[index];
}

//____________________________________________________________________________
Int_t AliFOCALClusterizerv2::GetMinRing(Int_t segment, Float_t /*energy*/) {
    
    if (fParameters && fParameters[segment])
        return (Int_t)fParameters[segment][parLocMinRad];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Int_t AliFOCALClusterizerv2::GetMaxRing(Int_t segment, Float_t energy) {
    
    Int_t maxRing, minRing;
    
    if (fParameters && fParameters[segment])
      maxRing = Int_t(fParameters[segment][parLocMaxRad]/fGeom->GetVirtualPadSize(segment) + 0.5);
    else {
      AliFatal("Parameters not initialised!");
      maxRing = 0;
    }
    minRing = GetMinRing(segment,energy);
    if (maxRing < minRing)
        return minRing;
    else
        return maxRing;
}

//____________________________________________________________________________
void AliFOCALClusterizerv2::AssignAmplitude(AliFOCALCell * c) {
    Int_t x,y;
    Float_t totalE = c->E();
    for (Int_t i = 1 ; i <= GetMinRing(c->Segment(),c->E()) ; i++) {
        fRinger->SetRing(i);
        while (fRinger->GetNext(x,y)) {
            AliFOCALCell * neighbor = GetCell(c->Segment(),c->Col()+x,c->Row()+y);
            if (neighbor) {
                totalE += neighbor->E();
            }  
        }
    }
    c->SetSeedEnergy(GetAmplitude(c->Segment(),GetMinRing(c->Segment(),c->E()),totalE));
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetAmplitude(Int_t /*segment*/, Int_t /*ring*/, Float_t totalE) {
    
    return totalE;// / (2*TMath::Pi()*fWeightingPar1*TMath::ATan(x/fWeightingPar1));
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetWeight(Int_t segment, Int_t ring, Float_t energy) {
    
  Float_t f, r0, r1;
    
    if (fParameters && fParameters[segment]) {
        f = fParameters[segment][parLocWeigh1];
        r0 = fParameters[segment][parLocWeigh2];
        r1 = fParameters[segment][parLocWeigh3];
    }
    else {
      AliFatal("Parameters not initialised!");
      return 0; // Dummy to silence compiler warning
    }
    
    Float_t x = ring*fGeom->GetVirtualPadSize(segment); 
    return energy *((1-f)*TMath::Exp(-x/r0) + f*TMath::Exp(-x/r1));
}

//____________________________________________________________________________

Float_t AliFOCALClusterizerv2::GetWeightCorrected(Int_t segment, Float_t dist, Float_t energy) {
    
  Float_t f, r0, r1;
    
    if (fParameters && fParameters[segment]) {
        f = fParameters[segment][parLocWeigh1];
        r0 = fParameters[segment][parLocWeigh2];
        r1 = fParameters[segment][parLocWeigh3];
    }
    else {
      AliFatal("Parameters not initialised!");
      return 0; // Dummy to silence compiler warning
    } 
    Float_t x = dist*fGeom->GetVirtualPadSize(segment); 
    return energy *((1-f)*TMath::Exp(-x/r0) + f*TMath::Exp(-x/r1));
}


//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetRejectionRatio(Int_t segment) const {
    
        // Returns apropriate value from fParameters
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocRejec1];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetRejectionRatio2(Int_t segment) const {
    
        // Returns apropriate value from fParameters
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocRejec2];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Bool_t AliFOCALClusterizerv2::IsOverEnergyThreshold(Int_t segment, Float_t energy) {
    
    if (fParameters && fParameters[segment])
        return (energy>fParameters[segment][parLocSeedTh]);
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
void AliFOCALClusterizerv2::FillClusterTree() {
    
    if (!fTreeR) {
        AliError("AliFOCALClusterizerv2::FillClusterTree(): fTreeR == 0 => no tree to fill!");
        return;
    }
    
    fTreeR->Fill();
}

//____________________________________________________________________________
void AliFOCALClusterizerv2::SortCells() {
    
    if (!fCell) {
        return;
    }
    
    fCell->Sort();
}

//____________________________________________________________________________
Float_t * AliFOCALClusterizerv2::GetCalibrationPars(Int_t segment) const {
    
    Float_t * p = new Float_t[3];
    p[0] = fParameters[segment][parLocCaliSt];
    p[1] = fParameters[segment][parLocCaliSt+1];
    p[2] = fParameters[segment][parLocCaliSt+2];
    return p;
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetWeightingPar1(Int_t segment) const {
    
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocWeigh1];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetWeightingPar2(Int_t segment) const {
    
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocWeigh2];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetWeightingPar3(Int_t segment) const {
    
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocWeigh3];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetSeedEnergyTreshold(Int_t segment) const {
    
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocSeedTh];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetClusterEnergyTreshold(Int_t segment) const {
    
    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocClusTh];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetNCellsThreshold(Int_t segment) const {

    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocCelNTh];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

//____________________________________________________________________________
Float_t AliFOCALClusterizerv2::GetShapeThreshold(Int_t segment) const {

    if (fParameters && fParameters[segment])
        return fParameters[segment][parLocShapTh];
    else {
      AliFatal("Parameters not initialised!");
      return 0;
    }
}

