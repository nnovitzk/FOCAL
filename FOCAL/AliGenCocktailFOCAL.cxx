#include "AliGenCocktailFOCAL.h"
#include <TParticle.h>
#include <AliLog.h>
#include <AliStack.h>
#include <AliRun.h>
#include <AliMC.h>

ClassImp(AliGenCocktailFOCAL)

AliGenCocktailFOCAL::AliGenCocktailFOCAL() : AliGenCocktail(), 
  fPartId(22), fMinPt(2), fMinEta(3.5), fMaxEta(5.8), fPrint(0)
{
}

void AliGenCocktailFOCAL::Generate() 
{
  Bool_t has_triggered=0;
  Int_t nGenEvt = 0;

  while (has_triggered==0) {
    if (fStack)
      fStack->Clean(); // This is probably never used
    else {
      AliRunLoader * runloader = AliRunLoader::Instance();
      if (runloader && runloader->Stack())
          runloader->Stack()->Clean();
    }
    AliGenCocktail::Generate();
    nGenEvt++;
    if (fPartId<0) 
      return;

    Int_t nparts = 0;
    if (fStack)
      nparts = fStack->GetNtrack();
    else
      nparts = gAlice->GetMCApp()->GetNtrack();
    for (Int_t j=0; j<nparts; ++j) {
      TParticle *part = 0;
      if (fStack)
        part = fStack->Particle(j);
      else
        part = gAlice->GetMCApp()->Particle(j);
      if (fPartId>0) {
	Int_t pdg = part->GetPdgCode();
	if (pdg!=fPartId)
	  continue;
      }
      Double_t pt = part->Pt();
      if (pt<fMinPt)
	continue;
      Double_t eta = part->Eta();
      if (eta<fMinEta||eta>fMaxEta)
	continue;
      has_triggered=1;
      if (fPrint)
	part->Print();
      break;
    }
  }
}
