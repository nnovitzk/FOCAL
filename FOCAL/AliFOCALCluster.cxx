/***************************************************************************
* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
*                                                                        *
* Author: The ALICE Off-line Project.                                    *
* Contributors are mentioned in the code where appropriate.              *
*                                                                        *
* Permission to use, copy, modify and distribute this software and its   *
* documentation strictly for non-commercial purposes is hereby granted   *
* without fee, provided that the above copyright notice appears in all   *
* copies and that both the copyright notice and this permission notice   *
* appear in the supporting documentation. The authors make no claims     *
* about the suitability of this software for any purpose. It is          *
* provided "as is" without express or implied warranty.                  *
**************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 30   2009                            //
//                                                     //
//  Cluster class for ALICE-FOCAL                          //
//                                                     //
//-----------------------------------------------------//


#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

#include "AliLog.h"

#include "AliFOCALCluster.h"


ClassImp(AliFOCALCluster)

using namespace std;

AliFOCALCluster::AliFOCALCluster():
	AliCluster(),
	fSegment(0),
	fEnergy(0),
        fVariable(true),
	fWeight(0),
        fHCALEnergy(0),
	fEisoR2(0),
	fEisoR4(0)
{
  //Default constructor
  for (Int_t i = 0; i < N_DEPTH_FIELDS; i++) {
    fSegIndex[i] = -1;
    fWidth1[i] = 0;
    fWidth2[i] = 0;
    fPhi[i] = 4.0;
    fSegmentEnergy[i] = 0;
    fSeedEnergy[i] = 0;
    fNcells[i] = 0;
  }
}

AliFOCALCluster::AliFOCALCluster(Double_t x, Double_t y, Double_t z, Double_t e, Int_t segment, Float_t seedE, Int_t nCells):
	AliCluster(), 
	fSegment(segment),
	fEnergy(e),
	fVariable(true),
	fWeight(0),
	fHCALEnergy(0),
	fEisoR2(0),
	fEisoR4(0)
{
	for (Int_t i = 0; i < N_DEPTH_FIELDS; i++) {
	  fSegIndex[i] = -1;
          fWidth1[i] = 0;
          fWidth2[i] = 0;
	  fPhi[i] = 4.0;		
	  fSegmentEnergy[i] = 0;
	  fSeedEnergy[i] = 0;
	  fNcells[i] = 0;
	}
	fSegIndex[0] = segment;
	fSeedEnergy[0] = seedE;
	fNcells[0] = nCells;
        // Set inherit fields for position
        SetX(x);
        SetY(y);
        SetZ(z);
}

AliFOCALCluster::AliFOCALCluster(const AliFOCALCluster &clst):
	AliCluster(clst),
	fSegment(clst.fSegment),
	fEnergy(clst.fEnergy),
	fVariable(clst.fVariable),
	fWeight(clst.fWeight),
	fHCALEnergy(clst.fHCALEnergy),
	fEisoR2(clst.fEisoR2),
	fEisoR4(clst.fEisoR4)
{
	for (Int_t i = 0; i < N_DEPTH_FIELDS; i++) {
	  fSegIndex[i] = clst.fSegIndex[i];
	  fWidth1[i] = clst.fWidth1[i];
	  fWidth2[i] = clst.fWidth2[i];
	  fPhi[i] = clst.fPhi[i];
	  fSegmentEnergy[i] = clst.fSegmentEnergy[i];
	  fSeedEnergy[i] = clst.fSeedEnergy[i];
	  fNcells[i] = clst.fNcells[i];
	}

}

AliFOCALCluster& AliFOCALCluster::operator= (const AliFOCALCluster &clst)
{
	if(&clst == this)
	{ 
		return *this;
	}
	
	fEnergy = clst.fEnergy;
	fSegment = clst.fSegment;
	fVariable = clst.fVariable;
	fWeight = clst.fWeight;
	fHCALEnergy = clst.fHCALEnergy;
	fEisoR2 = clst.fEisoR2;
	fEisoR4 = clst.fEisoR4;
	
	for (Int_t i = 0; i < N_DEPTH_FIELDS; i++) {
	  fSegIndex[i] = clst.fSegIndex[i];
	  fWidth1[i] = clst.fWidth1[i];
	  fWidth2[i] = clst.fWidth2[i];
	  fPhi[i] = clst.fPhi[i];
	  fSegmentEnergy[i] = clst.fSegmentEnergy[i];
	  fSeedEnergy[i] = clst.fSeedEnergy[i];
	  fNcells[i] = clst.fNcells[i];
	}
	
	return *this;

}

AliFOCALCluster::~AliFOCALCluster()
{
	// Default destructor
}

void AliFOCALCluster::Add(AliFOCALCluster const *clust) {
  if (clust->Segment() >= 0) {
    Int_t iSeg = clust->Segment();
    if (fSegmentEnergy[iSeg] != 0) {
      AliWarning(Form("Already have energy in segment %d; result will be inaccurate",iSeg));
    }
    Int_t segIdx = iSeg;
    if (clust->GetSegIndex(iSeg) == iSeg) 
      segIdx = segIdx;
    else if (clust->GetSegIndex(0) == iSeg) 
      segIdx = 0;
    else
      AliError("Expecting info in seg index slot 0 or iSeg");
    fSegIndex[iSeg] = iSeg;
    fSegmentEnergy[iSeg] += clust->E();
    fWidth1[iSeg] = clust->GetWidth1(segIdx);
    fWidth2[iSeg] = clust->GetWidth2(segIdx);
    fPhi[iSeg] = clust->GetPhi(segIdx);
    fSeedEnergy[iSeg] = clust->GetSeedEnergy(segIdx);
    fNcells[iSeg] = clust->GetNcells(segIdx);

    Float_t newE = fEnergy + clust->E();
    Float_t XatRef = clust->X()*Z()/clust->Z();
    Float_t YatRef = clust->Y()*Z()/clust->Z();
    SetX((fEnergy*X() + clust->E()*XatRef)/newE);
    SetY((fEnergy*Y() + clust->E()*YatRef)/newE);
    fEnergy = newE;
  }
  else
    AliError(Form("This function does not work for multi-segment clusters yet (seg %d)",clust->Segment()));
}

void AliFOCALCluster::MoveToZ(Float_t Z) {
  if (GetZ() == 0)
    AliError("Moving cluster with Z position = 0");
  else {
    SetX(X()*Z/GetZ());
    SetY(Y()*Z/GetZ());
    SetZ(Z);
  }
}

void AliFOCALCluster::SetSegIndex(Int_t idx, Int_t val) {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::SetSegIndex: Out of array bounds!" << endl;
    return;
  }
  
  fSegIndex[idx] = val;
}

void AliFOCALCluster::SetWidth1(Int_t idx, Float_t val) {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::SetWidth1: Out of array bounds!" << endl;
    return;
  }
  
  fWidth1[idx] = val;
}

void AliFOCALCluster::SetWidth2(Int_t idx, Float_t val) {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::SetWidth2: Out of array bounds!" << endl;
    return;
  }
  
  fWidth2[idx] = val;
}

void AliFOCALCluster::SetPhi(Int_t idx, Float_t val) {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::SetPhi: Out of array bounds!" << endl;
    return;
  }
  
  fPhi[idx] = val;
}

void AliFOCALCluster::SetSegmentEnergy(Int_t idx, Float_t val) {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::SetSegmentEnergy: Out of array bounds!" << endl;
    return;
  }
  
  fSegmentEnergy[idx] = val;
}

Int_t AliFOCALCluster::GetSegIndex(Int_t idx) const {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::GetSegIndex: Out of array bounds!" << endl;
    return -1;
  }
  
  return fSegIndex[idx];
}

Float_t AliFOCALCluster::GetWidth1(Int_t idx) const {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::GetWidth1: Out of array bounds!" << endl;
    return -1;
  }
  
  return fWidth1[idx];
}

Float_t AliFOCALCluster::GetWidth2(Int_t idx) const {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::GetWidth2: Out of array bounds!" << endl;
    return -1;
  }
  
  return fWidth2[idx];
}

Float_t AliFOCALCluster::GetPhi(Int_t idx) const {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::GetPhi: Out of array bounds!" << endl;
    return -1;
  }
  
  return fPhi[idx];
}

Float_t AliFOCALCluster::GetSegmentEnergy(Int_t idx) const {

  if ((idx < 0) || (idx >= N_DEPTH_FIELDS)) {
    cout << "AliFOCALCluster::GetSegmentEnergy: Out of array bounds!" << endl;
    return -1;
  }
  
  return fSegmentEnergy[idx];
}

