#ifndef AliFOCALCellMap_H
#define AliFOCALCellMap_H

#include "AliFOCALCell.h"

class AliFOCALCellMap : public TObject {

 public:
 
  AliFOCALCellMap();
  AliFOCALCellMap(Int_t segment, Int_t cols, Int_t rows);
  AliFOCALCellMap& operator=(const AliFOCALCellMap &fMap);
  ~AliFOCALCellMap();
  
  void CreateMap(Int_t segment, Int_t nCol, Int_t nRow);
  void SetCell(Int_t iCol, Int_t iRow, AliFOCALCell *cell) { fCellMap[iRow*fCols + iCol] = cell; }
  AliFOCALCell *GetCell(Int_t iCol, Int_t iRow) { if (iCol >= 0 && iCol < fCols && iRow > 0 && iRow < fRows) return fCellMap[iRow*fCols + iCol]; return 0;}


  void ResetMap();
 
  Int_t fRows;
  Int_t fCols;
  Int_t fSegment;
  AliFOCALCell * * fCellMap;
  
  ClassDef(AliFOCALCellMap,3);  // Map of Cells for clusterizer use

};

#endif // AliFOCALCellMap_H
