/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//
//
#include "TNamed.h"
#include "AliCDBEntry.h"
#include "AliFOCAL.h"
#include "AliFOCALPedestal.h"


ClassImp(AliFOCALPedestal)

AliFOCALPedestal::AliFOCALPedestal()
{
  // Default constructor
  Reset();
}
// ----------------------------------------------------------------- //
AliFOCALPedestal::AliFOCALPedestal(const char* name)
{
  //constructor
  TString namst = "Pedestal_";
  namst += name;
  SetName(namst.Data());
  SetTitle(namst.Data());
  Reset();
  
}
// ----------------------------------------------------------------- //
AliFOCALPedestal::AliFOCALPedestal(const AliFOCALPedestal &pedestal) :
  TNamed(pedestal)
{
  // copy constructor
  SetName(pedestal.GetName());
  SetTitle(pedestal.GetName());
  Reset();
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  for(Int_t icell = 0; icell < kCell; icell++)
		    {
		      fPedMeanRms[isec][iseg][irow][icol][icell] =
			pedestal.GetPedMeanRms(isec, iseg, irow, icol, icell);
		    }
		}
	    }
	  }
      }
  }
}
// ----------------------------------------------------------------- //
AliFOCALPedestal &AliFOCALPedestal::operator =(const AliFOCALPedestal &pedestal)
{
  //asignment operator
  SetName(pedestal.GetName());
  SetTitle(pedestal.GetName());
  Reset();
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  for(Int_t icell = 0; icell < kCell; icell++)
		    {
		      fPedMeanRms[isec][iseg][irow][icol][icell] =
			pedestal.GetPedMeanRms(isec, iseg, irow, icol, icell);
		    }
		}
	    }
	  }
      }
  }
  return *this;
}
// ----------------------------------------------------------------- //
AliFOCALPedestal::~AliFOCALPedestal()
{
  //destructor
}
// ----------------------------------------------------------------- //
void AliFOCALPedestal::Reset()
{
  //memset(fgainfact ,1,2*24*96*96*sizeof(Float_t));
    Float_t mean = 0.0;
    Float_t rms  = 0.0;
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  for(Int_t icell = 0; icell < kCell; icell++)
		    {
		      Int_t mean1 = (Int_t) (mean*10.);
		      Int_t rms1  = (Int_t) (rms*10.);
		      fPedMeanRms[isec][iseg][irow][icol][icell] =
			mean1*100 + rms1;
		    }
		}
	    }
	  }
      }
  }
}
// ----------------------------------------------------------------- //
Int_t AliFOCALPedestal:: GetPedMeanRms(Int_t sec, Int_t seg, Int_t row, Int_t col, Int_t cell) const
{
  return fPedMeanRms[sec][seg][row][col][cell];
}
// ----------------------------------------------------------------- //

void AliFOCALPedestal::SetPedMeanRms(Int_t sec, Int_t seg, Int_t row, Int_t col, Int_t cell, 
				     Float_t pedmean, Float_t pedrms)
{
    Int_t mean1 = (Int_t) (pedmean*10.);
    Int_t rms1  = (Int_t) (pedrms*10.); 
    fPedMeanRms[sec][seg][row][col][cell] = mean1*100 + rms1;
}
// ----------------------------------------------------------------- //
