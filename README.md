FOCAL software
==============

This is the simulation software for the ALICE FOCAL upgrade project. Instructions for use can be found at the Twiki page:

https://twiki.cern.ch/twiki/bin/view/ALICE/FOCALSoftware

Installation
------------

Requirements: ROOT, AliRoot
Optional: AliEn-Runtime

Steps for standalone installation

* cd aliroot

* mkdir obj

* cd obj

* cmake .. -DCMAKE\_INSTALL\_PREFIX="../install" -DROOTSYS="/pathToLocalDistribution/ROOT/latest" -DALIROOT="/pathToLocalDistribution/AliRoot/latest" -DALIEN="/pathToLocalDistribution/AliEn-Runtime/latest"

* make install

The results of the compilation: libraries, rootmap and pcm files will be stored in ../insall/lib, the include files in ../install/include, etc.
